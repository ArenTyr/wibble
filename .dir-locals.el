;; Default settings for all except cpan/
((nil . ((indent-tabs-mode . nil)))	; all modes
 (perl-mode . ((perl-indent-level . 4)))
 (c-mode . ((c-indentation-style . bsd)
            (c-basic-offset . 4))))
