# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)

package ScribbleWibble;
#use base 'App::Cmd';
use App::Cmd::Setup -app;
use ScribbleWibble::rg_search qw ( rg_init );

# For switch/given command suport
use strict;
no warnings qw(experimental::smartmatch);

use feature qw(switch);

use FindBin;
use Data::Dumper;
use Term::ANSIColor;

our $config_file = "$ENV{HOME}/.config/wibble/wibble.conf";
our $skel_dir = "$FindBin::Bin/skel";

our $archived_db_location = undef;
our $archive_manager = undef;
our $backup_location = undef;
our $bookmarks = undef;
our $db_location = undef;
our $editor = undef;
our $file_manager = undef;
our $gui_editor = undef;
our $jrn_dir = undef;
our $jrn_file = undef;
our $qn_dir = undef;
our $qn_file = undef;
our $task_file = undef;
our $tmp_dir = undef;
our $op_mode = undef;
our $wibble_store = undef;

my $fail_state = 0;
my $verbose = 0;

sub description
{
    return "Please set your options via the default configuration file"
	. " at $config_file. Required:\n\n"
    . "---\n"
    . "archive_manager=<program>\n"
    . "backup_location=<location>\n"
    . "bookmarks=<location>\n"
    . "db_archived=<location>\n"
    . "db_location=<location>\n"
    . "editor=<program>\n"
    . "file_manager=<program>\n"
    . "gui_edit=<program>\n"
    . "jrn_dir=<location>\n"
    . "jrn_file=<location>\n"
    . "op_mode=recutils  [ OR ]  op_mode=ripgrep\n"
    . "qn_dir=<location>\n"
    . "qn_file=<location>\n"
    . "task_file=<location>\n"
    . "wibble_store=<location>\n"
    . "---\n\n"
    . "<program> : command/program to execute\n"
    . "<location>: file system directory (no trailing slash)\n"
    . "<file>    : the file to use/save into\n"
    . "\nUse 'wibble init' to setup a default configuration file which you can then edit as needed.\n"
	. "\nSee README/documentation for further information.\n";
}

sub check_verbose
{
    my ($self) = @_;
    $verbose = $self->global_options->{verbose};
    if (length $verbose) {$verbose = 1};
}

sub global_opt_spec {
    return (
	["config|C" => "Override default configuration file"],
	["verbose|V" => "Show verbose output/debugging"],);
}

sub no_config_file
{
    print colored("No configuration file found at $config_file.\n\n", 'bright_red', 'bold');
    print colored("Please create a default configuration file.\n\n", 'bright_red', 'bold');
    print "Issue the command 'wibble init' to help generate one for you.\n";
    exit 0;
}

sub config_error
{
    my ($setting) = @_;
    print colored("\nRequired setting: '$setting' has not been defined in the configuration file. Please set.", 'bright_red'), "\n\n";
    print description;
    $fail_state = 1;
}

sub read_config_file
{
    if($verbose) { print "Reading config file: $config_file...\n"; print "---\n";}
    if(! -e $config_file) { no_config_file; }
    open (my $file_handle, '<', $config_file) or die "$!: No configuration file defined at: $config_file";
    chomp(my @file = <$file_handle>);
    close($file_handle);

    for my $line (@file)
    {

        given ($line)
        {
            when (/^#/) { next; }
            when (/archive_manager/)
            {
                if ($verbose) { say STDERR "* archive manager line: $line"; }
                $archive_manager = (split "=", $line, 2)[1];
                $archive_manager =~ s/~/$ENV{HOME}/g;
            }
            when (/backup_location/)
            {
                if ($verbose) { say STDERR "* $backup_location line: $line"; }
                $backup_location = (split "=", $line, 2)[1];
                $backup_location =~ s/~/$ENV{HOME}/g;
            }
            when (/bookmarks/)
            {
                if ($verbose) { say STDERR "* editor line: $line"; }
                $bookmarks = (split "=", $line, 2)[1];
                $bookmarks =~ s/~/$ENV{HOME}/g;
            }
            when (/db_archived/)
            {
                if ($verbose)
                {
                    say STDERR "* archived db location line: $line";
                }
                $archived_db_location = (split "=", $line, 2)[1];
                $archived_db_location =~ s/~/$ENV{HOME}/g;
            }
            when (/db_location/)
            {
                if ($verbose) { say STDERR "* db location line: $line"; }
                $db_location = (split "=", $line, 2)[1];
                $db_location =~ s/~/$ENV{HOME}/g;
            }
            when (/editor/)
            {
                if ($verbose) { say STDERR "* editor line: $line"; }
                $editor = (split "=", $line, 2)[1];
                $editor =~ s/~/$ENV{HOME}/g;
            }
            when (/file_manager/)
            {
                if ($verbose) { say STDERR "* file manager store: $line"; }
                $file_manager = (split "=", $line, 2)[1];
                $file_manager =~ s/~/$ENV{HOME}/g;
            }
            when (/gui_edit/)
            {
                if ($verbose) { say STDERR "* GUI editor line: $line"; }
                $gui_editor = (split "=", $line, 2)[1];
                $gui_editor =~ s/~/$ENV{HOME}/g;
            }
            when (/jrn_dir/)
            {
                if ($verbose) { say STDERR "* journal dir: $line"; }
                $jrn_dir = (split "=", $line, 2)[1];
                $jrn_dir =~ s/~/$ENV{HOME}/g;
            }
            when (/jrn_file/)
            {
                if ($verbose) { say STDERR "* default journal file: $line"; }
                $jrn_file = (split "=", $line, 2)[1];
                $jrn_file =~ s/~/$ENV{HOME}/g;
            }
            when (/op_mode/)
            {
                if ($verbose) { say STDERR "* task file: $line"; }
                $op_mode = (split "=", $line, 2)[1];
            }
            when (/qn_dir/)
            {
                if ($verbose) { say STDERR "* quicknote dir: $line"; }
                $qn_dir = (split "=", $line, 2)[1];
                $qn_dir =~ s/~/$ENV{HOME}/g;
            }
            when (/qn_file/)
            {
                if ($verbose) { say STDERR "* default quicknote file: $line"; }
                $qn_file = (split "=", $line, 2)[1];
                $qn_file =~ s/~/$ENV{HOME}/g;
            }
            when (/task_file/)
            {
                if ($verbose) { say STDERR "* task file: $line"; }
                $task_file = (split "=", $line, 2)[1];
                $task_file =~ s/~/$ENV{HOME}/g;
            }
            when (/tmp_dir/)
            {
                if ($verbose) { say STDERR "* tmp dir: $line"; }
                $tmp_dir = (split "=", $line, 2)[1];
                $tmp_dir =~ s/~/$ENV{HOME}/g;
            }
            when (/wibble_store/)
            {
                if ($verbose) { say STDERR "* wibble store: $line"; }
                $wibble_store = (split "=", $line, 2)[1];
                $wibble_store =~ s/~/$ENV{HOME}/g;
            }
        }
    }

    if($verbose)
    {
        print "---\n";
        print "Using editor      : $editor\n";
        print "Using GUI editor  : $gui_editor\n";
        print "Using file manager: $file_manager\n";
        print "Using main DB     : $db_location\n";
        print "Using archived DB : $archived_db_location\n";
        print "Using wibble_store: $wibble_store\n";
        print "===\n"
    }

    # Set fail state if anything not defined
    if(! defined $archived_db_location) { config_error("db_archived");     }
    if(! defined $archive_manager)      { config_error("archive_manager"); }
    if(! defined $backup_location)      { config_error("backup_location"); }
    if(! defined $bookmarks)            { config_error("bookmarks");       }
    if(! defined $db_location)          { config_error("db_location");     }
    if(! defined $editor)               { config_error("editor");          }
    if(! defined $file_manager)         { config_error("file_manager");    }
    if(! defined $gui_editor)           { config_error("gui_edit");        }
    if(! defined $jrn_dir)              { config_error("jrn_dir");         }
    if(! defined $jrn_file)             { config_error("jrn_file");        }
    if(! defined $op_mode)              { config_error("op_mode");         }
    if(! defined $qn_dir)               { config_error("qn_dir");          }
    if(! defined $qn_file)              { config_error("qn_file");         }
    if(! defined $task_file)            { config_error("task_file");       }
    if(! defined $tmp_dir)              { config_error("tmp_dir");         }
    if(! defined $wibble_store)         { config_error("wibble_store");    }

    if($fail_state) { exit(1); }

    rg_init($tmp_dir);
}

1;
