# ABSTRACT: Library wrapper/interface
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::lib_wrapper;
#use base 'App::Cmd::Command';
use ScribbleWibble::Command::search qw ( check_verbose check_config_override
                                         set_config_file extern_multiple_field_search
                                         extern_search_by_field return_all_notes );
use ScribbleWibble::Command::tool qw ( render_stats );
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy );
use Data::Dumper;
use Exporter 'import';
use File::Basename;
use File::Copy;
use File::Path;
use JSON::XS;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use Term::ReadLine;  # Do not "use Term::ReadLine::Gnu;"
use strict;
use warnings;
use utf8;

no warnings qw(experimental::try);
use feature 'try';

our @EXPORT_OK = qw ( wrapper_test execute_search serialize_wibble_records srch_serialize_wibble_records );

my $backup = 0;
my $full_backup = 0;
my $ls_backup = 0;
my $prune = 0;
my $verbose;


sub read_data_dir
{
    my ($archived, $note_id, $filetype, $proj) = @_;
    my $notepath_prefix = substr $note_id, 0, 2;
    my $note_datadir = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";
    if(! -e $note_datadir) { return "___NOT_FOUND___"; }
    my $result = `ls -QRA1 $note_datadir`;
    return $result;
}

sub read_data_dir_tree
{
    my ($archived, $note_id, $filetype, $proj) = @_;
    my $notepath_prefix = substr $note_id, 0, 2;
    my $note_datadir = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";
    if(! -e $note_datadir) { return "___NOT_FOUND___"; }
    my $result = `tree -Flshn --du --charset=ascii $note_datadir`;
    return $result;
}

sub read_linked_files
{
    my ($archived, $note_id, $filetype, $proj) = @_;
    my $notepath_prefix = substr $note_id, 0, 2;
    my $ln_flist = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id/.linked_files.wib";
    if(! -e $ln_flist) { return "___NOT_FOUND___"; }
    open (my $file_handle, '<', $ln_flist) or die "$!: $ln_flist";
    chomp(my @file = <$file_handle>);
    close($file_handle);
    return (join("\n", @file));
}

sub read_file_content
{
    my ($archived, $note_id, $filetype, $proj) = @_;
    my $notepath_prefix = substr $note_id, 0, 2;
    my $note_file;
    if(! $archived) { $note_file = "$ScribbleWibble::wibble_store/notes/$filetype/$proj/$notepath_prefix/$note_id.$filetype"; }
    else            { $note_file = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$proj/$notepath_prefix/$note_id.$filetype"; }
    if(! -e $note_file) { return "___NOT_FOUND___"; }
    open (my $file_handle, '<', $note_file) or die "$!: $note_file";
    chomp(my @file = <$file_handle>);
    close($file_handle);
    return (join("\n", @file));
}

sub get_linkids_full
{
    my ($link_ids_list) = @_;
    my @linked_results = ();
    if($link_ids_list eq "___NULL___") { return \@linked_results; }
    my @linked_notes = split / /, $link_ids_list;
    foreach(@linked_notes)
    {
        #print "Processing $_\n";
        # FIXME: Archive search...
        my $id_rel = substr $_, 0, 1;
        my $link_id = substr $_, 3, (length $_);
        my $relationship = "";
        #print "Got $link_id\n";
        my $results = extern_search_by_field(0, "NoteId", $link_id, "RIPGREP");
        #print Dumper $results;
        my $note_title = ( $results =~ /^Title:\s*(.*)$/gm )[0];
        my $note_type  = ( $results =~ /^Filetype:\s*(.*)$/gm )[0];
        my $link_result =
        {
            'id'    => $link_id,
            'title' => $note_title,
            'type'  => $note_type,
            'rel'   => $id_rel
        };
        push @linked_results, $link_result;
    }

    return \@linked_results;
}

# Wrapper to deal with strange namespace clash...
#sub serialize_wibble_records
#{
#    my ($passed_archive_search, $passed_results, $passed_everything_mode) = @_;
#    return serialize_wibble_records($passed_archive_search, $passed_results, $passed_everything_mode);
#}

sub count_results
{
    my ($results) = @_;
    my @note_id    = ( $results =~ /^NoteId:\s*(.*)$/gm );
    return scalar @note_id;
}

sub serialize_wibble_records
{
    my ($archive_search, $results, $metadata_only_mode) = @_;
    #print Dumper($results);

    #my $name = (caller(0))[3];
    #print colored("NAME IS: $name\n", 'bright_red', 'bold');

    my @dates      = ( $results =~ /^Date:\s*(.*)$/gm );
    my @note_title = ( $results =~ /^Title:\s*(.*)$/gm );
    my @note_id    = ( $results =~ /^NoteId:\s*(.*)$/gm );
    my @note_type  = ( $results =~ /^Filetype:\s*(.*)$/gm );
    my @note_desc  = ( $results =~ /^Description:\s*(.*)$/gm );
    my @note_proj  = ( $results =~ /^Project:\s*(.*)$/gm );
    my @note_tags  = ( $results =~ /^Tags:\s*(.*)$/gm );
    my @note_class = ( $results =~ /^Class:\s*(.*)$/gm );
    my @note_dirs  = ( $results =~ /^DataDirs:\s*(.*)$/gm );
    my @key_pairs  = ( $results =~ /^KeyPairs:\s*(.*)$/gm );
    my @note_cust  = ( $results =~ /^Custom:\s*(.*)$/gm );
    my @note_links = ( $results =~ /^LinkedIds:\s*(.*)$/gm );

    my @note_results;

    my $note_fp_dir = "";
    if(! $archive_search ) { $note_fp_dir = "notes"; }
    else                   { $note_fp_dir = "archived_notes"; }

    my $x = 0;
    for my $entry (@dates)
    {
        my $notepath_prefix = substr $note_id[$x], 0, 2;
        my $note_entry =
        {
            'metadata' =>
            {
                'date'          => $entry,
                'title'         => $note_title[$x],
                'note_id'       => $note_id[$x],
                'desc'          => $note_desc[$x],
                'type'          => $note_type[$x],
                'proj'          => $note_proj[$x],
                'tags'          => $note_tags[$x],
                'class'         => $note_class[$x],
                'kpairs'        => $key_pairs[$x],
                'custom'        => $note_cust[$x],
                'linkids'       => $note_links[$x],
                'ddirs'         => $note_dirs[$x],
                'linkids_full_map' =>  ""
            },
                'content'       => "",
                'data_dir_tree' => "",
                'linked_files'  => "",
                'data_dir_path' => "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id[$x]",
                'data_dir'      => "",
                'note_fp'       => "$ScribbleWibble::wibble_store/$note_fp_dir/$note_type[$x]/$note_proj[$x]/$notepath_prefix/$note_id[$x].$note_type[$x]"
        };

        # If a request is made for every note, we don't need to do the (potentially expensive) search/generate
        # linkids hash, since it will be possible to natively obtain the information in
        # the client-side React/GUI application by iterating over the JSON result list
        # Hence flag for "everything_mode"
        if(! $metadata_only_mode)
        {
            $note_entry->{'content'}       = read_file_content($archive_search, $note_id[$x], $note_type[$x], $note_proj[$x]);
            $note_entry->{'data_dir'}      = read_data_dir($archive_search, $note_id[$x], $note_type[$x], $note_proj[$x]),
            $note_entry->{'data_dir_tree'} = read_data_dir_tree($archive_search, $note_id[$x], $note_type[$x], $note_proj[$x]);
            $note_entry->{'linked_files'}  = read_linked_files($archive_search, $note_id[$x], $note_type[$x], $note_proj[$x]),
            $note_entry->{'metadata'}->{'linkids_full_map'} = get_linkids_full($note_links[$x]);
        }
        push @note_results, $note_entry;
        ++$x;

    }

    my $note = { 'matched_notes' => \@note_results };
    #print "Returning note data...\n";
    return $note;
}

sub execute_search
{
    my($input_json, $op_mode) = @_;
    try
    {
        chomp($input_json);
        #print colored("DEBUG: Executing search", 'bright_green', 'bold'), "\n";
        print colored("[  REQ ] Received request:\n--------------------------\n$input_json\n--------------------------",
                      'bright_yellow', 'bold'), "\n";
        my $search_cmd = decode_json $input_json;
        #print Dumper($search_cmd);

        my $results = "";
        if(exists $search_cmd->{mode} && $search_cmd->{mode} eq "metadata_only")
        {
            print colored("[ INFO ] Metadata only mode ENABLED. All metadata will be returned, no content.", 'bright_green', 'bold'), "\n";
            $results = return_all_notes(1);
            print colored("[ DATA ] Returning metadata for ALL [" . count_results($results) . "] note(s)", 'bright_green', 'bold'), "\n";
            return serialize_wibble_records(0, $results, 1);
        }
        elsif(exists $search_cmd->{mode} && $search_cmd->{mode} eq "statistics")
        {
            print colored("[ INFO ] Note statistics request. Building statistics data.", 'bright_green', 'bold'), "\n";
            my $stats_data = render_stats(1, "$ScribbleWibble::db_location", $op_mode);
            print colored("[  OK  ] Note statistics built. Returning statistics data.", 'bright_green', 'bold'), "\n";
            return $stats_data;
        }
        else
        {
            # params: archive search, field, search term
            #my $foo = extern_search_by_field(0, "Title", "Foo", "RIPGREP");
            $results = extern_multiple_field_search(0, $search_cmd->{date},
                                                       $search_cmd->{title},
                                                       $search_cmd->{note_id},
                                                       $search_cmd->{description},
                                                       $search_cmd->{filetype},
                                                       $search_cmd->{project},
                                                       $search_cmd->{tags},
                                                       $search_cmd->{class},
                                                       "",
                                                       $search_cmd->{keypairs},
                                                       $search_cmd->{custom},
                                                       "",
                                                       $op_mode);
            print colored("[ DATA ] Returning [" . count_results($results) . "] matched note(s)", 'bright_green', 'bold'), "\n";
            return serialize_wibble_records(0, $results, 0);
        }
    }
    catch($e)
    {
        print colored("[ ERR  ] Check that input JSON is valid [?]", 'bright_red', 'bold'), "\n";
        print colored("[ ERR  ] Trapped error: $e.\n"
                    . "[ ERR  ] Input JSON:\n"
                    . "--------------------\n"
                    . "$input_json\n"
                    . "--------------------\n", 'bright_red', 'bold');
        print colored("[  OK  ] Returning empty result set", 'bright_green', 'bold'), "\n";
        return '{}';
    }
}

1;
