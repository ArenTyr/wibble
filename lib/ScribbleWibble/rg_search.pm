# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::rg_search;
use ScribbleWibble::common_funcs qw ( parameter_check make_tmp_db_backup );
#use base 'App::Cmd::Command';

use Data::Dumper;
use Exporter 'import';
use File::Copy;
use File::Path;
use Term::ANSIColor;
use strict;
use warnings;

my $TMP_RG_FILE = undef;
my $TMP_KP_FILE = undef;

our @EXPORT_OK = qw ( rg_init rg_search_by_field rg_multiple_field_search rg_get_field_from_id_match
                      rg_update_record_field rg_delete_entry rg_update_entire_record );

# So we don't keep copying-on-value a potentially huge string
my $rg_result = "";
my $search_started = 0;

# https://stackoverflow.com/questions/5166662/perl-what-is-the-easiest-way-to-flatten-a-multidimensional-array#5166855
sub flat {  # no prototype for this one to avoid warnings
    return map { ref eq 'ARRAY' ? flat(@$_) : $_ } @_;
}

sub rg_init
{
    my ($tmp_dir) = @_;
    parameter_check(\@_, 1, "rg_init");
    $TMP_KP_FILE = "$tmp_dir/.wibble_rg_kp_tmp_search";
    $TMP_RG_FILE = "$tmp_dir/.wibble_rg_tmp_search";
    if(! -d $tmp_dir) { mkpath("$tmp_dir") or die "Unable to create temporary working directory $tmp_dir!\n"; }
}

# (Overwrite) latest intermediate rg results to temporary file
# Used for filtering search; similar to using pipe in command line chain
# Means after 1st pass (or 2nd pass) search results will already be
# extremely small
sub write_tmp_result_to_file
{
    my ($file_content) = @_;
    open (my $file_handle, '>', "$TMP_RG_FILE") or die "$!: $TMP_RG_FILE";
    print $file_handle $file_content;
    close($file_handle);
}

sub extract_record
{
    my ($rec_entry) = @_;
    # Split the input multiline string into an array of lines
    my @tmp_array = split /\n/, $rec_entry;

    # calculate start and end indices
    # delimiting start/end of record
    # first field is 'Date'
    my $start = 1;
    my $end = 0;
    my $find_start = 1;
    my $find_date = 1;
    my $index = 0;

    foreach (@tmp_array)
    {

        # Find "Date" == start of record
        if($find_date && $_ =~ /^Date:(.*)+$/)
        {
          $find_date = 0;
          $start = $index;
        }

        # Find "LinkedIds" == end of record
        if(! $find_date && $_ =~ /^LinkedIds:(.*)+$/)
        {
            $end = $index;
            # Ensure that we haven't found end snippet of previous record
            # If true record end, will be greater value than start
            # and we can immediately exit loop
            if($end > $start) { last; }
        }
        ++$index;
    }

    # Return spliced array segment
    my @part = @tmp_array[$start..$end];
    return \@part;

}

sub rg_process_keypairs
{
    my($search_term, $line_num_mode, $search_db) = @_;
    parameter_check(\@_, 3, "rg_process_keypairs");

    if(! length $search_term) { return ""; }
    my $cmd;
    my @search_pairs = split / /, $search_term;
    my $kp_tmp_file = $TMP_KP_FILE;
    my $x = 0;
    foreach(@search_pairs)
    {
        print "Searching: $_\n";
        if($x == 0)
        {
            print "searching main file, dump to tmp\n";
            if(! $line_num_mode) { $cmd = `rg  -i -N -C 11 "^KeyPairs:(.*)+$_#(.*)\$" $search_db`; }
            else                 { $cmd = `rg  -i -n -C 11 "^KeyPairs:(.*)+$_#(.*)\$" $search_db`; }
            write_tmp_result_to_file($cmd);
        }
        # if last keypair, no need to dump to temporary file
        elsif($x == ((scalar @search_pairs) - 1))
        {
            print "FINAL filtering tmp file, final read\n";
            if(! $line_num_mode) { $cmd = `cat $kp_tmp_file | rg  -i -N -C 11 "^KeyPairs:(.*)+$_#(.*)\$\"`; }
            else                 { $cmd = `cat $kp_tmp_file | rg  -i -n -C 11 "^KeyPairs:(.*)+$_#(.*)\$\"`; }
            # cleanup
            #if(-f $kp_tmp_file)
            #{
            #    system("rm $kp_tmp_file");
            #}
        }
        else
        {
            print "filtering tmp file, re-dump\n";
            if(! $line_num_mode) { $cmd = `cat $kp_tmp_file | rg  -i -N -C 11 "^KeyPairs:(.*)+$_#(.*)\$"`; }
            else                 { $cmd = `cat $kp_tmp_file | rg  -i -n -C 11 "^KeyPairs:(.*)+$_#(.*)\$"`; }
            write_tmp_result_to_file($cmd);
        }
        ++$x;
    }

    return $cmd;
}

sub rg_search_by_field
{
    my ($search_field, $exact_search, $search_term, $ark_search, $verbose, $tmp_file, $line_num_mode) = @_;
    parameter_check(\@_, 7, "rg_search_by_field");

    my $cmd;
    my $search_db;

    #if(! length $tmp_file) { print "DEBUG: Direct search, no tmp file\n"; }

    if(length $tmp_file)
    {
        #print colored("DEBUG: Using TEMPORARY file: $tmp_file", 'bright_red'), "\n";
        $search_db = $tmp_file;
    }
    elsif($ark_search)
    {
        $search_db = $ScribbleWibble::archived_db_location;
    }
    else
    {
        $search_db = $ScribbleWibble::db_location;
    }

    my $result = "";
    # Exact boundary of 11 lines context is critical - each record is 12 rows followed
    # by a blank line separating it from the next record
    if($exact_search)
    {
        if(! $line_num_mode) { $cmd = "rg \"^${search_field}: $search_term\" -N -C 11 $search_db"; }
        else                 { $cmd = "rg \"^${search_field}: $search_term\" -n -C 11 $search_db"; }
        #if($verbose) { print colored("RG: Performing EXACT search with cmd:\n$cmd", 'bright_green'), "\n"; }
        $result = `$cmd`;
    }
    elsif($search_field eq "KeyPairs")
    {
        $result = rg_process_keypairs($search_term, $line_num_mode, $search_db);
    }
    else
    {
        if(! $line_num_mode) { $cmd = "rg \"^${search_field}(.*)+$search_term\" -i -N -C 11 $search_db"; }
        else                 { $cmd = "rg \"^${search_field}(.*)+$search_term\" -i -n -C 11 $search_db"; }
        #if($verbose) { print colored("RG: Performing REGEXP search with cmd:\n$cmd", 'bright_green'), "\n"; }
        $result = `$cmd`;
    }

    # Return at this point if all we need is/are line number(s)
    if ($line_num_mode) { return $result; }

    # Remove confusion of ripgrep "--" result separator in addition to blank lines
    $result =~ s/^\-\-/ /gm;

    # (Potential) record matches are separated by blank lines
    my @matches = split /^\s*$/m, $result;
    my @cleaned_array = [];

    # Inspect each record fragment for a complete record == a match
    foreach(@matches)
    {
        my @record_entry = extract_record($_);
        push @cleaned_array, \@record_entry;
        # Critical - ensure newly extracted record has a blank line to
        # delimit it from other matches
        push @cleaned_array, " ";
    }

    # Flatten resultant array of array matches to one flat list of lines
    # i.e. restore back to normal text listing
    my @new_results = flat @cleaned_array;
    return join("\n", @new_results);
}

sub rg_get_field_from_id_match
{
    my ($note_id, $ark_search, $field_to_get) = @_;
    parameter_check(\@_, 3, "rg_get_field_from_id_match");

    my $results = rg_search_by_field("NoteId", 1, $note_id, $ark_search, 0, "", 0);

    my $field = ( $results =~ /^$field_to_get:(.*)$/m )[0];
    $field =~ s/^\s+|\s+$//g ;
    return $field;

}

sub find_line_number_to_update
{
    my ($record, $field_to_update, $index_only_mode) = @_;
    my @tmp_array = split /\n/, $record;
    my $start_index = 0;
    my $index = 0;
    my $linematch = -1;
    my $line_num = -1;
    my $record_start = 0;

    foreach (@tmp_array)
    {
        # Find start of record section (has colon)
        if ($_ =~ /^(\d+):/gm)
        {
          $record_start = 1;
          $start_index = $index;

          # for use with rg_update_entire_record
          if($index_only_mode)
          {
              return (split /:/, $tmp_array[$index])[0];
          }
        }

        # Find field match
        if($_ =~ /^(\d+)\-$field_to_update(.*)+$/)
        {
            # Ensure that we haven't found end snippet of previous record
            if($record_start && $index > $start_index)
            {
                $linematch = $index;
                last;
            }
        }
        ++$index;
    }

    if($linematch < 0)
    {
        print colored("❌ Critical error updating field. Aborting.", 'bright_red'), "\n";
        exit(1);
    }

    $line_num = (split /-/, $tmp_array[$linematch])[0];

    return $line_num;
}


sub rg_update_entire_record
{
    my ($note_id, $ark_search, $replacement_record) = @_;
    parameter_check(\@_, 3, "rg_update_entire_record");

    my $record = rg_search_by_field("NoteId", 1, $note_id, $ark_search, 1, "", 1);
    my $line_num = find_line_number_to_update($record, "NoteId", 1);

    # NoteId is third field/line; subtract two plus another for 0 index
    # $line_num now holds zero-indexed start of record from "Date:"
    $line_num = $line_num - 3;
    # Make backup of database file for safety
    my $database_file = make_tmp_db_backup($ark_search);

    open (my $file_handle, '<', "$database_file") or die "$!: $database_file";
    my @db_file = <$file_handle>;
    close($file_handle);

    foreach(@{$replacement_record})
    {
        $db_file[$line_num] = $_;
        ++$line_num;
    }

    # Clobber old database file with new one with updated lines
    open ($file_handle, '>', "$database_file") or die "$!: $database_file";
    foreach(@db_file)
    {
        print $file_handle $_;
    }
    close($file_handle);

    print colored("✓ Field successfully updated.", 'bright_green'), "\n";
    return 1;

}

sub rg_update_record_field
{
    my ($note_id, $ark_search, $field_to_update, $new_field_value) = @_;
    parameter_check(\@_, 4, "rg_update_record_field");

    my $record = rg_search_by_field("NoteId", 1, $note_id, $ark_search, 1, "", 1);
    my $line_num = find_line_number_to_update($record, $field_to_update, 0);
    
    # Make backup of database file for safety
    my $database_file = make_tmp_db_backup($ark_search);

    open (my $file_handle, '<', "$database_file") or die "$!: $database_file";
    my @db_file = <$file_handle>;
    close($file_handle);

    $db_file[$line_num - 1] = $field_to_update . ": $new_field_value\n";

    # Clobber old database file with new one with updated line
    open ($file_handle, '>', "$database_file") or die "$!: $database_file";
    foreach(@db_file)
    {
        print $file_handle $_;
    }
    close($file_handle);

    print colored("✓ Field successfully updated.", 'bright_green'), "\n";
    return 1;
}

sub rg_delete_entry
{
    my ($uuid, $ark, $verbose) = @_;
    parameter_check(\@_, 3, "rg_delete_entry");

    my $start_line_num = -1;
    my $end_line_num = 1;

    my $results = rg_search_by_field("NoteId", 1, $uuid, $ark, $verbose, "", 1);

    # Find the line number corresponding to the rg match
    if ($results =~ /^(\d+):/gm)
    {
        $start_line_num = $1 - 2;
        $end_line_num = $start_line_num + 12;
    }

    if($start_line_num < 0 || $end_line_num < 0)
    {
        print colored("No matches. Please specify a unique UUID or leading UUID substring for note to delete.", 'bright_red'), "\n";
        exit(1);
    }

    my $database_file = make_tmp_db_backup($ark);
    open (my $file_handle, '<', "$database_file") or die "$!: $database_file";
    my @db_file = <$file_handle>;
    # Array indices start at 0, and we want to stop on the line BEFORE -> hence - 2
    my @db_region_before = @db_file[0..($start_line_num - 3)];
    # shift onto start of next record, already capturing blank line from above
    my @db_region_after = @db_file[($end_line_num - 1)..(scalar @db_file - 1)];
    my @new_db_file = (@db_region_before, @db_region_after);
    close($file_handle);

    print "Checking integrity of new database...\n";

    my $integrity_check = ((scalar @db_file) - (scalar @new_db_file));
    if($integrity_check == 13)
    {
        print colored("✓ Integrity check passed.", 'bright_green'), "\n";
    }
    else
    {
        print colored("❌ Integrity check failed. Aborting deletion operation. Existing database file has not been altered.\n",
                      'bright_red', 'bold'), "\n";
        exit(1);
    }

    my $replacement_db = "$ScribbleWibble::tmp_dir/newdb.rec";
    open ($file_handle, '>', "$replacement_db") or die "$!: Unable to open $replacement_db for writing.";
    foreach(@new_db_file)
    {
        print $file_handle $_;
    }
    close($file_handle);

    my $replace_db = move($replacement_db, $database_file);
    if ($replace_db)
    {
        if(! $ark) { print colored("✓ Main database successfully updated.", 'bright_green'), "\n"; }
        else       { print colored("✓ Archived database successfully updated.", 'bright_green'), "\n"; }
    }
    else
    {
        print colored("❌ Error updating metadata.", 'bright_green'), "\n";
        exit(1);
    }
}


sub rg_do_search_iteration
{
    my ($search_field, $search_term, $ark_search, $verbose, $tmp_file) = @_;
    parameter_check(\@_, 5, "rg_do_search_iteration");

    # print "DEBUG. In new: '$search_started', '$search_field', '$search_term', '$ark_search', '$verbose', '$tmp_file'\n";

    # Exact search makes no sense for incremental menu search
    my $exact_search = 0;

    if($search_started && length $rg_result)
    {
        if(length $search_term)
        {
            $rg_result = rg_search_by_field($search_field, $exact_search, $search_term, $ark_search, $verbose, $tmp_file, 0);
            write_tmp_result_to_file($rg_result);
        }
    }
    elsif(! $search_started && length $search_term)
    {
        $rg_result = rg_search_by_field($search_field, $exact_search, $search_term, $ark_search, $verbose, "", 0);
        write_tmp_result_to_file($rg_result);
        $search_started = 1;
    }
    elsif($search_started && ! length $rg_result)
    {
        # Search has commenced, and already no length/result
        #print colored("DEBUG: Exiting early, stage: $search_field.", 'bright_green'), "\n";
        # no-op
        1;
    }
}

sub rg_multiple_field_search
{
    my ( $date_search, $title_search, $id_search,   $desc_search,
         $ft_search,   $proj_search,  $tags_search, $class_search,
         $data_search, $keys_search,  $cust_search, $link_ids,
         $ark_search, $verbose  ) = @_;

    parameter_check(\@_, 14, "rg_multiple_field_search");

    #print "DEBUG: Got values:\n";
    #print "'$date_search'\n";
    #print "'$title_search'\n";
    #print "'$id_search'\n";
    #print "'$desc_search'\n";
    #print "'$ft_search'\n";
    #print "'$proj_search'\n";
    #print "'$tags_search'\n";
    #print "'$class_search'\n";
    #print "'$data_search'\n";
    #print "'$keys_search'\n";
    #print "'$cust_search'\n";
    #print "'$link_ids'\n";
    #print "'$ark_search'\n";

    my $chain_command = "";
    my $tmp_file = $TMP_RG_FILE;

    # ====================================================================================
    # Ensure search starts with empty/no temporary file search
    # 1. Date         ====================================================================
    rg_do_search_iteration("Date",        $date_search,  $ark_search, $verbose, ""       );
    # 2. Title        ====================================================================
    rg_do_search_iteration("Title",       $title_search, $ark_search, $verbose, $tmp_file);
    # 3.  NoteId      ====================================================================
    rg_do_search_iteration("NoteId",      $id_search,    $ark_search, $verbose, $tmp_file);
    # 4.  Description ====================================================================
    rg_do_search_iteration("Description", $desc_search,  $ark_search, $verbose, $tmp_file);
    # 5.  Filetype    ====================================================================
    rg_do_search_iteration("Filetype",    $ft_search,    $ark_search, $verbose, $tmp_file);
    # 6.  Project     ====================================================================
    rg_do_search_iteration("Project",     $proj_search,  $ark_search, $verbose, $tmp_file);
    # 7.  Tags        ====================================================================
    rg_do_search_iteration("Tags",        $tags_search,  $ark_search, $verbose, $tmp_file);
    # 8.  Class       ====================================================================
    rg_do_search_iteration("Class",       $class_search, $ark_search, $verbose, $tmp_file);
    # 9.  DataDir     ====================================================================
    rg_do_search_iteration("DataDirs",    $data_search,  $ark_search, $verbose, $tmp_file);
    # 10. KeyPairs    ====================================================================
    rg_do_search_iteration("KeyPairs",    $keys_search,  $ark_search, $verbose, $tmp_file);
    # 11. Custom      ====================================================================
    rg_do_search_iteration("Custom",      $cust_search,  $ark_search, $verbose, $tmp_file);
    # 12. LinkedIds   ====================================================================
    rg_do_search_iteration("LinkedIds",   $link_ids,     $ark_search, $verbose, $tmp_file);
    # ====================================================================================

    if( -f $tmp_file ) { system("rm $tmp_file"); }

    $search_started = 0;
    return $rg_result;
}

1;
