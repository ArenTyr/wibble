package ScribbleWibble::ui_elems;
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)

use Exporter 'import';
use strict;
use warnings;

use constant HL_WIDTH => 54;

our @EXPORT_OK = qw ( print_hl print_hl_heavy padded_number );

sub print_hl
{
    print "\n" . "-" x HL_WIDTH . "\n";
}

sub print_hl_heavy
{
    print "\n" . "=" x HL_WIDTH . "\n";
}

sub padded_number
{
    my ($total_num, $num_to_pad) = @_;

    my    $zeropad = 0;
    if   ($total_num < 10 )     { $zeropad = 1; }
    elsif($total_num < 100)     { $zeropad = 2; }
    elsif($total_num < 1000)    { $zeropad = 3; }
    elsif($total_num < 10000)   { $zeropad = 4; }
    elsif($total_num < 100000)  { $zeropad = 5; }
    elsif($total_num < 1000000) { $zeropad = 6; }

    return sprintf("%0${zeropad}d", $num_to_pad);
}
