# ABSTRACT: Setup / initialise a brand new wibble configuration
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::init;
use ScribbleWibble -command;
#use base 'App::Cmd::Command';
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use ScribbleWibble::common_funcs qw ( open_file_handle_CLOBBER );
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy );
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Path;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use Term::ReadLine;  # Do not "use Term::ReadLine::Gnu;"
use strict;
use warnings;

my $rg_installed = 0;
my $rec_installed = 0;

sub description
{
    return "Use this command to initialise a new Wibble installation\n";
}

sub check_dependencies
{
    my $check_rg = `which rg || echo \$?`;
    if(looks_like_number($check_rg) && $check_rg == 1)
    {
        print colored("* Ripgrep is not installed/not found. Only recutils mode will be available\n"
                      . "and some functionality will be missing. It is strongly recommended to install\n"
                      ."ripgrep. Ensure it is visible on your \$PATH.", 'bright_red'), "\n\n";
    }
    else
    {
        print colored("* Ripgrep FOUND. Native mode (\"RIPGREP\") can be enabled (RECOMMENDED),\n"
                    . "and will be the default suggestion.\n", 'bright_green'), "\n\n";
        $rg_installed = 1;
    }
    my $check_rec = `which recsel || echo \$?`;
    if(looks_like_number($check_rec) && $check_rec == 1)
    {
        print colored("* GNU Recutils is not installed/not found. Only ripgrep mode will be available.\n"
                     . "Recutils is NOT required as ripgrep is the recommended mode. However, some advanced\n"
                     . "querying is only available via recutils (recsel). For 99.99% of users, this querying\n"
                     . "will not matter/be of no interest, and this warning can be safely ignored."
                     , 'bright_yellow'), "\n\n";
    }
    else
    {
        print colored("* Recutils FOUND. Advanced searching via recsel is available. Native mode (\"RIPGREP\")\n"
                    . "is still the recommend mode to enable, and will not affect availablity of 'recsel'\n"
                    . "based advanced queries.\n", 'bright_green'), "\n\n";
        $rec_installed = 1;
    }

    # NOTE: Remember system commands return "0" on success
    if($rg_installed) { print colored("[ RIPGREP MODE  ]: AVAILABLE", 'bright_green', 'on_white', 'bold'), "\n"; }
    else            { print colored("[ RIPGREP MODE  ]: NOT AVAILABLE", 'bright_red', 'on_white', 'bold'), "\n"; }

    if($rec_installed) { print colored("[ RECUTILS MODE ]: AVAILABLE", 'bright_green', 'on_white', 'bold'), "\n"; }
    else             { print colored("[ RECUTILS MODE ]: NOT AVAILABLE", 'bright_red', 'on_white', 'bold'), "\n"; }

    if($rg_installed == 0 && $rec_installed == 0)
    {
        print colored("\nNeither ripgrep nor recutils was found. Please install at least one of these\n"
                      . "to proceed. Ripgrep is the recommended option. Installing both is optional.\n\n", 'bright_red', 'bold');
        print colored("Critical dependency (ripgrep or recutils) missing. Aborting.", 'bright_red', 'bold'), "\n";
        exit 1;
    }
    elsif ($rg_installed && $rec_installed)
    {
        print colored("\nBoth ripgrep and recutils FOUND. Maximum functionality is available.", 'bright_green', 'bold'), "\n";
        print colored("Ripgrep mode is recommended choice.", 'bright_green', 'bold'), "\n";
    }


}

sub execute
{
    print colored("This command will setup a fresh/new Wibble installation. Proceed? Input 'y'.", 'bright_green', 'bold'), "\n";
    print "> ";
    my $response = <STDIN>;
    chomp($response);
    if($response eq "y")
    {
        print_hl_heavy; print "\n";
        check_dependencies;
        if(-e "$ENV{HOME}/.config/wibble/wibble.conf")
        {
            print colored("An existing configuration file already exists at '~/.config/wibble/wibble.conf'. Aborting.",
                          'bright_red', 'bold'), "\n";
            print"If you wish to start over, please first either delete or move the existing configuration file.\n";
        }
        else
        {
            print_hl_heavy;
            my $term = Term::ReadLine->new('User selection', \*STDIN, \*STDOUT);
            print "\nProceeding. For each question, type in the command or path for your given choice.\n\n";
            print "NOTES:\n";
            print "- A default suggestion is supplied, press CTRL-U or repeatedly backspace to remove this\n"
                . "  option to then input your own custom choice.\n";
            print "- If you need to enter additional command line arguments for a command, do so.\n";
            print "- Do not enter a final trailing slash when specifying a directory path.\n";
            print "- Do not escape spaces or use quotation marks around directory paths - just input it.\n";
            print "\n";

            print colored("[1/8] Input main/root Wibble directory (e.g. '~/Documents/wibble')", 'bright_green'), "\n";
            my $prompt = colored("Main Wibble directory: ", 'bright_yellow', 'bold');
            my $main_wibble_dir = $term->readline($prompt, "~/Documents/wibble");
            print colored("[2/8] Input backup Wibble directory (should be OUTSIDE root Wibble directory)\n(e.g. '~/Documents/backup/wibble')", 'bright_green'), "\n";
            $prompt = colored("Backup Wibble directory: ", 'bright_yellow', 'bold');
            my $backup_location = $term->readline($prompt, "~/Documents/backup/wibble");
            print colored("[3/8] Editor (terminal) (e.g. 'vim, micro, emacsclient -t')", 'bright_green'), "\n";
            $prompt = colored("Input terminal editor command: ", 'bright_yellow', 'bold');
            my $term_editor = $term->readline($prompt, "vim");
            print colored("[4/8] Editor (GUI) (e.g. 'emacs, mousepad, codium')", 'bright_green'), "\n";
            $prompt = colored("Input GUI editor command: ", 'bright_yellow', 'bold');
            my $gui_editor = $term->readline($prompt, "emacs");
            print colored("[5/8] Filemanager (GUI) (e.g. 'thunar, pcmanfm, xterm -e mc')", 'bright_green'), "\n";
            $prompt = colored("Input file manager command: ", 'bright_yellow', 'bold');
            my $fm_prog = $term->readline($prompt, "thunar");
            print colored("[6/8] Archive tool (GUI) (e.g. 'xarchiver, fileroller')", 'bright_green'), "\n";
            $prompt = colored("Input file manager command: ", 'bright_yellow', 'bold');
            my $archive_prog = $term->readline($prompt, "xarchiver");
            print colored("[7/8] Temporary file/directory location (e.g. '/tmp/wibble-tmp, ~/Documents/tmp')", 'bright_green'), "\n";
            $prompt = colored("Input temporary directory location: ", 'bright_yellow', 'bold');
            my $tmp_wibble_dir = $term->readline($prompt, "/tmp/wibble-tmp");

            my $op_input_mode = "";
            if($rg_installed && $rec_installed == 0)
            {
                print colored("[8/8] Ripgrep installed, recutils NOT installed. Using 'ripgrep' operation mode.", 'bright_green'), "\n";
                $op_input_mode = "ripgrep";
            }
            elsif($rg_installed == 0 && $rec_installed)
            {
                print colored("[8/8] Recutils installed, ripgrep NOT installed. Using 'recutils' operation mode.", 'bright_green'), "\n";
                $op_input_mode = "recutils";
            }
            elsif($rg_installed && $rec_installed)
            {
                print colored("[8/8] Selection OPERATION mode. Enter '1' for 'ripgrep' (default), '2' for 'recutils')", 'bright_green'), "\n";
                $prompt = colored("Input temporary directory location: ", 'bright_yellow', 'bold');
                my $op_input_mode_choice = $term->readline($prompt, "1");
                chomp($op_input_mode_choice);
                if($op_input_mode_choice == 1) { $op_input_mode = "ripgrep"; }
                elsif($op_input_mode_choice == 2) { $op_input_mode = "recutils"; }
                else
                {
                    print colored("(Invalid input - expecting input of '1' or '2'. Defaulting choice to 'ripgrep').\n", 'bright_red'), "\n";
                    $op_input_mode = "ripgrep";
                }

            }

            $main_wibble_dir =~ s/^\s+|\s+$//g ;
            $backup_location =~ s/^\s+|\s+$//g ;
            $term_editor     =~ s/^\s+|\s+$//g ;
            $gui_editor      =~ s/^\s+|\s+$//g ;
            $fm_prog         =~ s/^\s+|\s+$//g ;

            print_hl_heavy; print "\n";
            print "archive_manager=$archive_prog\n";
            print "backup_location=$backup_location\n";
            print "bookmarks=$main_wibble_dir/bookmarks/bookmarks\n";
            print "db_archived=$main_wibble_dir/wibble_db_archived.rec\n";
            print "db_location=$main_wibble_dir/wibble_db.rec\n";
            print "editor=$term_editor\n";
            print "gui_edit=$gui_editor\n";
            print "file_manager=$fm_prog\n";
            print "jrn_dir=$main_wibble_dir/journal\n";
            print "jrn_file=$main_wibble_dir/journal/journal.txt\n";
            print "qn_dir=$main_wibble_dir/quicknotes\n";
            print "qn_file=$main_wibble_dir/quicknotes/INBOX.txt\n";
            print "task_file=$main_wibble_dir/tasks/tasks\n";
            print "tmp_dir=$tmp_wibble_dir\n";
            print "op_mode=$op_input_mode\n";
            print "wibble_store=$main_wibble_dir\n";
            print_hl_heavy;

            print colored("\nWrite out the above settings to ~/.config/wibble/wibble.conf and setup directories? (y/n)",
                          'bright_green', 'bold'), "\n";
            print "> ";
            $response = <STDIN>;
            chomp($response);
            if($response eq "y")
            {
                mkpath("$ENV{HOME}/.config/wibble");
                my $CONFIG_FILE = open_file_handle_CLOBBER("~/.config/wibble/wibble.conf");
                print $CONFIG_FILE "# Generated Wibble configuration file\n";
                print $CONFIG_FILE "# ===========================================\n";
                print $CONFIG_FILE "# ============= Archive manager =============\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "archive_manager=$archive_prog\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ============= Backup location =============\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "backup_location=$backup_location\n";
                print $CONFIG_FILE "bookmarks=$main_wibble_dir/bookmarks/bookmarks\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# =========== Database locations ============\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "db_archived=$main_wibble_dir/wibble_db_archived.rec\n";
                print $CONFIG_FILE "db_location=$main_wibble_dir/wibble_db.rec\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ============= Editor settings =============\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "editor=$term_editor\n";
                print $CONFIG_FILE "gui_edit=$gui_editor\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# =============== File manager ==============\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "file_manager=$fm_prog\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ======= Journal/Quicknotes settings =======\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "jrn_dir=$main_wibble_dir/journal\n";
                print $CONFIG_FILE "jrn_file=$main_wibble_dir/journal/journal.txt\n";
                print $CONFIG_FILE "qn_dir=$main_wibble_dir/quicknotes\n";
                print $CONFIG_FILE "qn_file=$main_wibble_dir/quicknotes/INBOX.txt\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ================= Task file ===============\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "task_file=$main_wibble_dir/tasks/tasks\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ======= Temporary/working directory =======\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "tmp_dir=$tmp_wibble_dir\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ==== Operation: 'ripgrep' / 'recutils' ====\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "op_mode=$op_input_mode\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ======= Main/root storage directory =======\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "wibble_store=$main_wibble_dir\n";
                print $CONFIG_FILE "#\n";
                print $CONFIG_FILE "# ================= END CONFIG ==============\n";
                close($CONFIG_FILE);

                print colored("\nSetting up directories (wibble_store) under '$main_wibble_dir'.\n", 'bright_green', 'bold'), "\n";

                $main_wibble_dir =~ s/~/$ENV{HOME}/g;
                $backup_location =~ s/~/$ENV{HOME}/g;

                mkpath("$main_wibble_dir/bookmarks");
                mkpath("$main_wibble_dir/journal");
                mkpath("$main_wibble_dir/quicknotes");
                mkpath("$main_wibble_dir/tasks");
                mkpath("$backup_location");
                

                print colored("\nConfiguration file written out and setup complete.\n"
                            . "You can now create your first note with 'wibble create'.", 'bright_green', 'bold'), "\n";
            }

        }
    }
}

1;
