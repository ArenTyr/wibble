# ABSTRACT: Add / work with journal entry / entries
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)

package ScribbleWibble::Command::journal;
use ScribbleWibble -command;
use ScribbleWibble::Command::quicknote qw ( get_special_wrapper_start get_special_wrapper_end inbox_section_header
                                            dump_or_edit_inbox add_inbox_section view_inbox_section check_inbox_init
                                            extern_qn_set_state                                                      );
use ScribbleWibble::Command::search    qw ( check_verbose check_config_override set_config_file                      );
use ScribbleWibble::ui_elems           qw ( print_hl print_hl_heavy padded_number                                    );

use strict;
use warnings;

use Data::Dumper;
use File::Basename;
use File::Path;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;

my $add_journal = 0;
my $alt_file = 0;
my $custom_file;
my $dump_journal = 0;
my $edit_journal = 0;
my $ls_files = 0;
my $ls_journal = 0;
my $view_journal = 0;
my $use_gui = 0;
my $verbose;

# NOTE: No "delete" functionality, deliberate/by-design; only for "quicknotes"
sub opt_spec
{
    return (
        ["add|A"     => "Add/append to default journal file"],
        ["dir|R"     => "List available/initialised journal files"],
        ["dump|D"    => "Dump entire journal file"],
        ["edit|E"    => "Edit journal file"],
        ["file|F"    => "Use alternate journal file instead of default file"],
        ["guiedit|G" => "Add/edit to journal file with GUI editor instead of regular editor"],
        ["ls|L"      => "List all journal entries"],
        ["view|W"    => "View journal entry"],
    );
}

sub description
{
    return "Journal files.\n";
}

sub ls_jrn_files
{
    print "Available/initialised journal files:\n\n";
    system("ls -1 ${ScribbleWibble::jrn_dir}");
    exit(0);
}

# Imported functions
#sub get_special_wrapper_start
#sub get_special_wrapper_end
#sub inbox_section_header
#sub dump_or_edit_inbox
#sub add_inbox_section
#sub view_inbox_section
#sub check_inbox_init

sub add_jrn_entry    { add_inbox_section;  }
sub dump_or_edit_jrn { dump_or_edit_inbox; }
sub ls_jrn_entry     { view_inbox_section; }
sub view_jrn_section { view_inbox_section; }


sub dispatch_journal
{
    if    ($add_journal)  { add_jrn_entry;    }
    elsif ($dump_journal) { dump_or_edit_jrn; }
    elsif ($edit_journal) { dump_or_edit_jrn; }
    elsif ($ls_files)     { ls_jrn_files;     }
    elsif ($ls_journal)   { ls_jrn_entry;     }
    elsif ($view_journal) { view_jrn_section; }
    else
    {
        print "Unknown option specified. Please select one of '--add', '--edit', '--dir', '--dump', "
              . "'--ls', or '--view'.\n";
    }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $enabled_modes = 0;

    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;

    if($opt->guiedit)   { $use_gui = 1;  }
    if($opt->file)
    {
        $alt_file = 1;
        if(scalar @$args == 0)
        {
            print "No custom journal filename supplied. Exiting.\n";
            exit(1);
        }
        else
        {
            $custom_file = "@$args";
            $custom_file =~ tr/ /_/;
            chomp($custom_file);
        }

    }

    # mode options
    if($opt->add)       { $add_journal = 1;  ++$enabled_modes; }
    if($opt->dir)       { $ls_files = 1;     ++$enabled_modes; }
    if($opt->dump)      { $dump_journal = 1; ++$enabled_modes; }
    if($opt->edit)      { $edit_journal = 1; ++$enabled_modes; }
    if($opt->ls)        { $ls_journal = 1;   ++$enabled_modes; }
    if($opt->view)      { $view_journal = 1; ++$enabled_modes; }


    extern_qn_set_state($ScribbleWibble::jrn_dir, $ScribbleWibble::jrn_file, "journal",
                        "Journal",                $alt_file,                 $custom_file,
                        $use_gui,                 $ls_journal,               $verbose,
                        $edit_journal,            $dump_journal,             $view_journal);
    check_inbox_init;

    if($enabled_modes gt 1 || $verbose)
    {

        if(! $alt_file) { print colored ("Using journal file: $ScribbleWibble::jrn_file", 'bright_green'), "\n";                    }
        else            { print colored ("Using CUSTOM journal file: ${ScribbleWibble::jrn_dir}/$custom_file", 'bright_red'), "\n"; }

        if($enabled_modes gt 1)
        {
            print "Please choose only one option out of [ --add | --ls | --view | --dump ]\n";
            print "❌ Error: Multiple conflicting options.\n";
            exit(0);
        }

        if($add_journal eq 1)  { print colored ("--add    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--add    : DISABLED", 'bright_red'), "\n"; }
        if($dump_journal eq 1) { print colored ("--dump   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--dump   : DISABLED", 'bright_red'), "\n"; }
        if($edit_journal eq 1) { print colored ("--edit   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--edit   : DISABLED", 'bright_red'), "\n"; }
        if($ls_files eq 1)   { print colored ("--dir    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--dir    : DISABLED", 'bright_red'), "\n"; }
        if($ls_journal eq 1)   { print colored ("--ls     : ENABLED", 'bright_green'), "\n"; } else { print colored ("--ls     : DISABLED", 'bright_red'), "\n"; }
        if($view_journal eq 1) { print colored ("--view   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--view   : DISABLED", 'bright_red'), "\n"; }
    }

    dispatch_journal;
}

1;
