# ABSTRACT: Non-interactively (script) add data directly to notes
# Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::add;
use ScribbleWibble -command;
use ScribbleWibble::common_funcs qw ( open_file_ro parameter_check set_operation_mode );
use ScribbleWibble::Command::search qw ( check_verbose check_config_override
                                         set_config_file extern_search_by_field
                                         extern_update_data_dir_metadata );
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Copy::Recursive qw( rcopy rmove );
use File::Path;
use Term::ANSIColor;
use strict;
use warnings;

use utf8;

no warnings 'utf8';
no warnings qw(experimental::try);
use feature 'try';

my $enabled_modes = 0;
my $filename = "";
my $copy_ebld = 0;
my $move_ebld = 0;
my $link_ebld = 0;
my $note_id = undef;
my $op_mode;
my $verbose;

sub description
{
    my ($self, $opt, $args) = @_;
    my $desc = "Automatically add data (no prompt) to existing note.\n"
             . "Usage: wibble add [ --copy-file <file/directory> ]\n"
             . "                  [ --move-file <file/directory> ]\n"
             . "                  [ --add-link <file/directory> ]\n"
             . "                  <note id>\n\n"
             . "Example invocation:\n\n"
             . "       wibble add --copy-file \"file.jpg\" f85a88r1-783\n\n\n"
             . "The specified note ID must contain either the full ID or \n"
             . "sufficient substring to uniquely identify the single candidate note.\n";
        return $desc;
}

sub check_args
{
    my ($input) = @_;
    my @arr = @{$input};
    my $block_arg = 0;

    my $copy_file = 0; # 1
    my $move_file = 0; # 2
    my $add_link  = 0; # 3
    my $valid_arg = 0;
    my $set_note_id = 0;

    while(scalar @arr > 0)
    {
        my $arg = shift @arr;

        ##################### DETECTION #####################
        # ========== 1. Copy file mode ===========
        if(! $block_arg && $arg eq "--copy-file")
        {
            $enabled_modes++;
            $block_arg = 1;
            $copy_file = 1;
            $valid_arg = 1;
            next;
        }

        # ========== 2. Move data mode ===========
        if(! $block_arg && $arg eq "--move-file")
        {
            $enabled_modes++;
            $block_arg = 1;
            $move_file = 1;
            $valid_arg = 1;
            next;
        }

        # ========== 3. Add linked file mode ===========
        if(! $block_arg && $arg eq "--add-link")
        {
            $enabled_modes++;
            $block_arg = 1;
            $add_link = 1;
            $valid_arg = 1;
            next;
        }

        if($enabled_modes > 1 || ($filename eq "--copy-file" ||
                                  $filename eq "--move-file" ||
                                  $filename eq "add-link"))
        {
            print colored("A maximum of only one option can be selected:\n"
                        . "'--copy-file', '--move-file', '--add-link'. Aborting.",
                          'bright_red', 'bold'), "\n";
            exit 1;

        }

        if($enabled_modes == 1 && $set_note_id)
        {
            $note_id = $arg;
            $set_note_id = 0;
            next;
        }

        if(! $block_arg && ! $valid_arg)
        {
            print colored("Invalid/unknown argument: '$arg'. Aborting.", 'bright_red', 'bold'), "\n";
            exit 1;
        }
        else
        {
            $valid_arg = 0;
        }

        ##################### ASSIGNMENT #####################

        # ========== 1. Copy file ===========
        if($copy_file)
        {
            $copy_file = 0;
            $filename = $arg;
            $block_arg = 0;
            $copy_ebld = 1;
            $set_note_id = 1;
            next;
        }

        # ========== 2. Move file ===========
        if($move_file)
        {
            $move_file = 0;
            $filename = $arg;
            $block_arg = 0;
            $move_ebld = 1;
            $set_note_id = 1;
            next;
        }

        # ========== 3. Add linked file ===========
        if($add_link)
        {
            $add_link = 0;
            $filename = $arg;
            $block_arg = 0;
            $link_ebld = 1;
            $set_note_id = 1;
            next;
        }

    }

    my $error_case = 0;
    my $show_values = 0;

    if((! defined($note_id)) || (! length $note_id))
    { print colored("❌ Error processing destination note ID. The note ID (or partial unique substring of) must be supplied.",
            'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; $note_id = "NOT FOUND"; }

    if($enabled_modes != 1)
    { print colored("❌ At least one action out of '--copy-data', '--move-file', and '--add-link' must be supplied.",
            'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; $note_id = "NOT FOUND"; }


    # Check that the key/value pair is set
    if($copy_ebld && ! length $filename)
    { print colored("❌ Error processing filename for '--copy-file' operation. A valid file or directory must be be supplied.",
            'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; }
    if($move_ebld && ! length $filename)
    { print colored("❌ Error processing filename for '--move-file' operation. A valid file or directory must be be supplied.",
            'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; }
    if($link_ebld && ! length $filename)
    { print colored("❌ Error processing filename for '--add-link' operation. A valid file or directory must be be supplied.",
            'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; }

    if($show_values || $verbose)
    {
        print("\nDetected input values:\n");
        print("=====================\n");
        print("--copy-file: $copy_ebld\n");
        print("--move-file: $move_ebld\n");
        print(" --add-link: $link_ebld\n");
        print("note ID: $note_id\n");
        print("=====================\n\n");

        if($error_case) { exit 1; }
    }

}

sub dispatch_data_action
{
    try
    {
        my $results = extern_search_by_field(0, "NoteId", $note_id, $op_mode);
        my @note_match = ( $results =~ /^NoteId:\s*(.*)$/gm );
        my @note_dirs  = ( $results =~ /^DataDirs:\s*(.*)$/gm );
        if(scalar @note_match == 0 || scalar @note_dirs == 0)
        {
            print colored("❌ No matches for supplied ID or substring of ID '$note_id'. Please enter a matching note ID.",
                'bright_red', 'bold'), "\n";
            exit 1;
        }
        elsif(scalar @note_match > 1 || scalar @note_dirs > 1)
        {
            print colored("❌ Multiple matches for supplied ID or substring of ID '$note_id'.\n"
                        . "Please enter either a full unique note ID, or substring that uniquely matches only ONE ID.",
                'bright_red', 'bold'), "\n";
            print "Matched IDs:\n";
            print "@note_match";
            exit 1;
        }

        my $full_note_id = $note_match[0];
        my $notepath_prefix = substr $full_note_id, 0, 2;
        my $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$full_note_id";
        mkpath("$output_path");

        $filename =~ s/^\s+|\s+$//g;
        # replace with Cwd abs_path https://stackoverflow.com/questions/19001957/perl-command-for-same-behaviour-as-readlink
        $filename = `readlink -f "$filename"`;
        chomp($filename);
        my $fl_basename = basename($filename);

        my $file_success = 0;

        if($copy_ebld)
        {
            print colored("* Copying file: \"$filename\" -> \"$output_path\"", 'bright_green'), "\n";
            my $copy_result;
            if(-d $filename) { mkpath("$output_path/$fl_basename"); $copy_result = rcopy("$filename", "$output_path/$fl_basename"); }
            else             { $copy_result = rcopy("$filename", "$output_path"); }
            if($copy_result)
            {
                print colored("✓ File successfully copied.", 'bright_green'), "\n";
                $file_success = 1;
            }
            else
            {
                print colored("❌ Error copying file. Check that '$filename' exists and is readable.", 'bright_red', 'bold'), "\n";
            }
        }
        elsif($move_ebld)
        {
            print colored("* Moving file: \"$filename\" -> \"$output_path\"", 'bright_green'), "\n";
            my $move_result;
            if(-d $filename) { mkpath("$output_path/$fl_basename"); $move_result = rmove("$filename", "$output_path/$fl_basename"); }
            else             { $move_result = rmove("$filename", "$output_path"); }
            if($move_result)
            {
                print colored("✓ File successfully moved.", 'bright_green'), "\n";
                $file_success = 1;
            }
            else
            {
                print colored("❌ Error moving file. Check that '$filename' exists and is readable.", 'bright_red', 'bold'), "\n";
                print "DEBUG:\n$!";
            }
        }
        elsif($link_ebld)
        {
            if(! -f $filename)
            {
                print colored("❌ Uknown file '$filename'. Check that it exists and is readable.", 'bright_red', 'bold'), "\n";
                exit 1;
            }

            print "* Adding link to file\n system(\"echo \"$filename\" >> $output_path/.linked_files.wib\")";
            my $link_result = `echo '$filename' >> $output_path/.linked_files.wib || echo 'ERROR'`;
            chomp($link_result);
            if($link_result ne "ERROR")
            {
                print colored("✓ File successfully added to linked files list.", 'bright_green'), "\n";
                $file_success = 1;
            }
            else
            {
                print colored("❌ Error adding file linkage. Check that '$filename' exists and is readable.", 'bright_red', 'bold'), "\n";
            }
        }

        if($file_success)
        {
            extern_update_data_dir_metadata($full_note_id, $note_dirs[0], $output_path, $op_mode);
        }
    }
    catch($err)
    {
        print colored("❌ Error performing data action: $err.", 'bright_red', 'bold'), "\n";
        exit 1;
    }
}


sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $op_mode = set_operation_mode($verbose);

    if(scalar @{$args} == 0)
    {
        print description, "\n";
        exit 0;
    }
    else
    {
        #print scalar @{$args};
        #print "\n$args\n";
    }

    check_args($args);
    dispatch_data_action;

}

1;
