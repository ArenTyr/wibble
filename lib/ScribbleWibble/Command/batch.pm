# ABSTRACT: Batch/script create new note(s)
# Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::batch;
use ScribbleWibble -command;
use ScribbleWibble::common_funcs qw ( open_file_ro parameter_check set_operation_mode update_record_metadata );
use ScribbleWibble::Command::create qw ( extern_batch_insert_new_note );
use ScribbleWibble::rg_search qw ( rg_search_by_field );
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use Data::Dumper;
use File::Copy;
use File::Path;
use Term::ANSIColor;
use strict;
use warnings;

no warnings qw(experimental::smartmatch);

use feature qw(switch);

my $auto_mode = 0;

my $class = "";
my $custom = "";
my $datadirs = "";
my $description = "";
my $filetype = "";
my $keypairs = "";
my $linkids = "";
my $op_mode = "";
my $project = "";
my $tags = "";
my $schema = "";
my $template = "";
my $title = "";
my $verbose;

my $from_file = "";
my $update_id = "";

# default to creation of new files via batch mode (vs updating existing)
my $enable_create_mode = 1;

sub description
{
    my ($self, $opt, $args) = @_;
    #$self->app->read_config_file;
    my $desc = "Batch mode. Create a new note programmatically/script-wise by specifying.\n"
             . "field metadata values, and file content from an existing file.\n\n"
             . " --- Creating new notes: ---\n\n"
             . "Usage: wibble batch --title <title_value> --filetype <filetype_value> \n"
             . "                  [ --description <description_value> ] [ --project <project_value> ]\n"
             . "                  [ --project <project_value> ] [ --tags <tags_value> ]\n"
             . "                  [ --class <class_value> ] [ --keypairs <keypairs_value> ]\n"
             . "                  [ --custom <custom_value> ] [ --schema <schema> ]\n"
             . "                  [ --from-file <path_to_file> ] [ --template <schema> ]\n"
             . "                  [ --auto ]\n\n"
             . "Example invocation using all definable fields:\n\n"
             . "       wibble batch \\ \n"
             . "       --title \"My amazing note\" \\ \n"
             . "       --description \"A tremendous description\"  \\ \n"
             . "       --filetype \"md\"  \\ \n"
             . "       --project \"My Personal Project\"  \\ \n"
             . "       --tags \"#tag1 #tag2\"  \\ \n"
             . "       --class \"Reference_Material\"  \\ \n"
             . "       --custom \"URGENT - Waiting For\"  \\ \n"
             . "       --keypairs \"priority: A#users: Mike Sandy#deadline: 2028-01-01_16:00#\"  \\ \n"
             . "       --schema \"personal_schm\"  \\ \n"
             . "       --from-file \"~/doc/proj/some_file.txt\"  \\ \n"
             . "       --auto\n\n\n"
             . "The above command would immediately create the note with no further input, the \n"
             . "contents of 'some_file.txt', and with the field values as defined.\n\n"
             . "  NOTES:\n\n"
             . "* Only --title and --filetype are REQUIRED. They constitute the bare minimum input.\n"
             . "* 'Date', 'DataDirs', 'NoteId', and 'LinkedIds' are all automatically managed,\n"
             . "  so cannot be specified.\n"
             . "* All other fields are optional.\n"
             . "* If a field is not specified, the field will be blank/have NULL value.\n"
             . "* Field values should always be quoted unless they're a single value with no special\n"
             . "  characters present that the shell will interpret.\n"
             . "* If specifying keypairs ensure you follow the correct keypair format with '#'\n"
             . "  termination block symbol after every defined keypair of 'key: value' format.\n"
             . "* If --from-file value is not specified, the note will be created blank/empty\n"
             . "  or using default template, or using --template value if that field is specified.\n"
             . "* Using both --template and --from-file together makes no sense, --from-file\n"
             . "  will override any defined --template value.\n"
             . "* By default the command will prompt to confirm note generation. To entirely SKIP\n"
             . "  this prompt, pass in the '--auto' flag. Use --auto only once you have familiarised\n"
             . "  yourself with usage of the command and verified your script is working as intended.\n"
             . "\n\n --- Updating existing notes: ---\n\n"
             . "Usage: wibble batch [<as per above>] --update-existing <note_id>\n\n"
             . "Operation is the same but pass in --update-existing <note_id> plus your fields.\n"
             . "--title and --filetype are no longer required, since they are already defined.\n"
             . "Note ID must be a sufficient stub to match ONE singular candidate for update to proceed.\n"
             . "The --auto flag once again bypasses any prompts.\n"
             . "Note that passing in --from-file or --template option will OVERWRITE ANY\n"
             . "EXISTING NOTE CONTENT. You have been warned :-) \n";
        return $desc;
}

sub check_args
{
    my ($input) = @_;
    my @arr = @{$input};
    my $block_arg = 0;

    my $title_ebld   = 0; # 1
    my $desc_ebld    = 0; # 2
    my $ft_ebld      = 0; # 3
    my $proj_ebld    = 0; # 4
    my $tags_ebld    = 0; # 5
    my $class_ebld   = 0; # 6
    #my $dd_ebld     = 0; # 7
    my $kp_ebld      = 0; # 8
    my $cust_ebld    = 0; # 9
    my $linkids_ebld = 0; # 10
    my $tem_ebld     = 0; # 11
    my $sch_ebld     = 0; # 12
    my $ff_ebld      = 0; # 13

    my $update_ebld = 0; # 14

    my $title_flag   = 0; # 1
    my $desc_flag    = 0; # 2
    my $ft_flag      = 0; # 3
    my $proj_flag    = 0; # 4
    my $tags_flag    = 0; # 5
    my $class_flag   = 0; # 6
    #my $dd_flag     = 0; # 7
    my $kp_flag      = 0; # 8
    my $cust_flag    = 0; # 9
    my $linkids_flag = 0; # 10
    my $tem_flag     = 0; # 11
    my $sch_flag     = 0; # 12
    my $ff_flag      = 0; # 13

    my $update_flag = 0; # 14

    my $title_set   = 0; # 1 Mandatory
    my $desc_set    = 0; # 2
    my $ft_set      = 0; # 3 Mandatory
    my $proj_set    = 0; # 4
    my $tags_set    = 0; # 5
    my $class_set   = 0; # 6
    #my $dd_set     = 0; # 7
    my $kp_set      = 0; # 8
    my $cust_set    = 0; # 9
    my $linkids_set = 0; # 10
    my $tem_set     = 0; # 11
    my $sch_set     = 0; # 12
    my $ff_set      = 0; # 13

    my $update_set = 0; # 14


    my $valid_arg = 0;

    while(scalar @arr > 0)
    {
        my $arg = shift @arr;

        ##################### DETECTION #####################

        if(! $block_arg && $arg eq "--auto")
        {
            $auto_mode = 1;
            next;
        }

        # ========== 1. Title ===========
        if(! $block_arg && $arg eq "--title")
        {
            $title_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $title_flag = 1;
            next;
        }

        # ========== 2. Description ===========
        if(! $block_arg && $arg eq "--description")
        {
            $desc_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $desc_flag = 1;
            next;
        }

        # ========== 3. Filetype ===========
        if(! $block_arg && $arg eq "--filetype")
        {
            $ft_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $ft_flag = 1;
            next;
        }

        # ========== 4. Project ===========
        if(! $block_arg && $arg eq "--project")
        {
            $proj_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $proj_flag = 1;
            next;
        }

        # ========== 5. Tags ===========
        if(! $block_arg && $arg eq "--tags")
        {
            $tags_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $tags_flag = 1;
            next;
        }

        # ========== 6. Class ===========
        if(! $block_arg && $arg eq "--class")
        {
            $class_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $class_flag = 1;
            next;
        }

        # FIXME: Handle adding files/data at note creation in batch
        # ========== 7. DataDirs ===========
        #if(! $block_arg && ($arg eq "--template" || $arg eq "-T"))
        #{
        #    $tem_ebld = 1;
        #    $block_arg = 1;
        #    next;
        #}

        # ========== 8. KeyPairs ===========
        if(! $block_arg && $arg eq "--keypairs")
        {
            $kp_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $kp_flag = 1;
            next;
        }

        # ========== 9. Custom ===========
        if(! $block_arg && $arg eq "--custom")
        {
            $cust_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $cust_flag = 1;
            next;
        }

        # ========== 10. LinkIds ===========
        if(! $block_arg && $arg eq "--linkids")
        {
            $linkids_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $linkids_flag = 1;
            next;
        }

        # ========== 11. Template ===========
        if(! $block_arg && $arg eq "--template")
        {
            $tem_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $tem_flag = 1;
            next;
        }

        # ========== 12. Schema ===========
        if(! $block_arg && $arg eq "--schema")
        {
            $sch_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $sch_flag = 1;
            next;
        }

        # ========== 13. From File ===========
        if(! $block_arg && $arg eq "--from-file")
        {
            $ff_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $ff_flag = 1;
            next;
        }

        # ========== 14. Update Existing ===========
        if(! $block_arg && $arg eq "--update-existing")
        {
            $update_ebld = 1;
            $block_arg = 1;
            $valid_arg = 1;
            $update_flag = 1;
            next;
        }

        if(! $block_arg && ! $valid_arg)
        {
            print colored("Invalid/unknown argument: '$arg'. Aborting.", 'bright_red', 'bold'), "\n";
            exit 1;
        }
        else
        {
            $valid_arg = 0;
        }

        ##################### ASSIGNMENT #####################

        # ========== 1. Title ===========
        if($title_ebld)
        {
            $title = $arg;
            $block_arg = 0;
            $title_ebld = 0;
            $title_set = 1;
            next;
        }

        # ========== 2. Description ===========
        if($desc_ebld)
        {
            $description = $arg;
            $block_arg = 0;
            $desc_ebld = 0;
            $desc_set = 1;
            next;
        }

        # ========== 3. Filetype ===========
        if($ft_ebld)
        {
            $filetype = $arg;
            $block_arg = 0;
            $ft_ebld = 0;
            $ft_set = 1;
            next;
        }

        # ========== 4. Project ===========
        if($proj_ebld)
        {
            $project = $arg;
            $block_arg = 0;
            $proj_ebld = 0;
            $proj_set = 1;
            next;
        }

        # ========== 5. Tags ===========
        if($tags_ebld)
        {
            $tags = $arg;
            $block_arg = 0;
            $tags_ebld = 0;
            $tags_set = 1;
            next;
        }

        # ========== 6. Class ===========
        if($class_ebld)
        {
            $class = $arg;
            $block_arg = 0;
            $class_ebld = 0;
            $class_set = 1;
            next;
        }

        # FIXME: See above
        # ========== 7. DataDirs ===========
        #if($sch_ebld)
        #{
        #    $schema = $arg;
        #    $use_schema = 1;
        #    $block_arg = 0;
        #    $sch_ebld = 0;
        #    $sch_set = 1;
        #    next;
        #}

        # ========== 8. KeyPairs ===========
        if($kp_ebld)
        {
            $keypairs = $arg;
            $block_arg = 0;
            $kp_ebld = 0;
            $kp_set = 1;
            next;
        }

        # ========== 9. Custom ===========
        if($cust_ebld)
        {
            $custom = $arg;
            $block_arg = 0;
            $cust_ebld = 0;
            $cust_set = 1;
            next;
        }

        # ========== 10. LinkIds ===========
        if($linkids_ebld)
        {
            $linkids = $arg;
            $block_arg = 0;
            $linkids_ebld = 0;
            $linkids_set = 1;
            next;
        }

        # ========== 11. Template ===========
        if($tem_ebld)
        {
            $template = $arg;
            #$use_template = 1;
            $block_arg = 0;
            $tem_ebld = 0;
            $tem_set = 1;
            next;
        }

        # ========== 12. Schema ===========
        if($sch_ebld)
        {
            $schema = $arg;
            #$use_schema = 1;
            $block_arg = 0;
            $sch_ebld = 0;
            $sch_set = 1;
            next;
        }

        # ========== 13. From File ===========
        if($ff_ebld)
        {
            $from_file = $arg;
            $block_arg = 0;
            $ff_ebld = 0;
            $ff_set = 1;
            next;
        }

        # ========== 14. Update existing ===========
        if($update_ebld)
        {
            $update_id = $arg;
            $block_arg = 0;
            $update_ebld = 0;
            $update_set = 1;
            next;
        }


    }

    # Testing
    # DEBUG
    #print("=====================\n");
    #print("FLAG?\n");
    #print("---------------------\n");
    #print("TITLE:   $title_flag  \n");
    #print("DESC:    $desc_flag   \n");
    #print("FT:      $ft_flag     \n");
    #print("PROJ:    $proj_flag   \n");
    #print("TAGS:    $tags_flag   \n");
    #print("CLASS:   $class_flag  \n");
    ##print(" $dd_flag    \n");
    #print("KP:      $kp_flag     \n");
    #print("CUST:    $cust_flag   \n");
    #print("LINKIDS: $linkids_flag\n");
    #print("TEM:     $tem_flag    \n");
    #print("SCH:     $sch_flag    \n");
    #print("FF:      $ff_flag     \n");
    #print("=====================\n");
    #print("SET?\n");
    #print("---------------------\n");
    #print("TITLE:   $title_set  \n");
    #print("DESC:    $desc_set   \n");
    #print("FT:      $ft_set     \n");
    #print("PROJ:    $proj_set   \n");
    #print("TAGS:    $tags_set   \n");
    #print("CLASS:   $class_set  \n");
    ##print(" $dd_set    \n");
    #print("KP:      $kp_set     \n");
    #print("CUST:    $cust_set   \n");
    #print("LINKIDS: $linkids_set\n");
    #print("TEM:     $tem_set    \n");
    #print("SCH:     $sch_set    \n");
    #print("FF:      $ff_set     \n");
    #print("=====================\n");
    #print("VALUES?\n");
    #print("---------------------\n");
    #print("TITLE:   $title\n");
    #print("DESC:    $description\n");
    #print("FT:      $filetype\n");
    #print("PROJ:    $project\n");
    #print("TAGS:    $tags\n");
    #print("CLASS:   $class\n");
    ##print(" $dd    \n");
    #print("KP:      $keypairs\n");
    #print("CUST:    $custom\n");
    #print("LINKIDS: $linkids\n");
    #print("TEM:     $template\n");
    #print("SCH:     $schema\n");
    #print("FF:      $from_file\n");
    #print("=====================\n");

    my $error_case = 0;
    my $show_values = 0;

    # Check that the key/value pair is set
    if($title_flag && ! length $title)      { print colored("❌ Error processing 'Title' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";       $error_case = 1; $show_values = 1; }
    if($desc_flag && ! length $description) { print colored("❌ Error processing 'Description' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; }
    if($ft_flag && ! length $filetype)      { print colored("❌ Error processing 'Filetype' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";    $error_case = 1; $show_values = 1; }
    if($proj_flag && ! length $project)     { print colored("❌ Error processing 'Project' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";     $error_case = 1; $show_values = 1; }
    if($tags_flag && ! length $tags)        { print colored("❌ Error processing 'Tags' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";        $error_case = 1; $show_values = 1; }
    if($class_flag && ! length $class)      { print colored("❌ Error processing 'Class' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";       $error_case = 1; $show_values = 1; }
    #if($dd_flag && ! length $datadirs)     { print colored("❌ Error processing schema value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";       $error_case = 1; $show_values = 1; }
    if($kp_flag && ! length $keypairs)      { print colored("❌ Error processing 'Keypairs' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";    $error_case = 1; $show_values = 1; }
    if($cust_flag && ! length $custom)      { print colored("❌ Error processing 'Custom' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";      $error_case = 1; $show_values = 1; }
    if($linkids_flag && ! length $linkids)  { print colored("❌ Error processing 'Linkids' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";     $error_case = 1; $show_values = 1; }
    if($tem_flag && ! length $template)     { print colored("❌ Error processing 'Template' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";    $error_case = 1; $show_values = 1; }
    if($sch_flag && ! length $schema)       { print colored("❌ Error processing 'Schema' value. A value must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";      $error_case = 1; $show_values = 1; }
    if($ff_flag && ! length $from_file)     { print colored("❌ Error processing 'from-file' value. A filename must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";   $error_case = 1; $show_values = 1; }
    if($update_flag && ! length $update_id)     { print colored("❌ Error processing 'update-existing' value. A valid note ID must be supplied if the field is specified.", 'bright_red', 'bold'), "\n";   $error_case = 1; $show_values = 1; }


    # This set of checks should be redundant, but will leave in for completeness
    if($title_set && ! defined $title)      { print colored("❌ Error processing arguments/title definition.", 'bright_red', 'bold'), "\n";       $error_case = 1; $show_values = 1; }
    if($desc_set && ! defined $description) { print colored("❌ Error processing arguments/description definition.", 'bright_red', 'bold'), "\n"; $error_case = 1; $show_values = 1; }
    if($ft_set && ! defined $filetype)      { print colored("❌ Error processing arguments/filetype definition.", 'bright_red', 'bold'), "\n";    $error_case = 1; $show_values = 1; }
    if($proj_set && ! defined $project)     { print colored("❌ Error processing arguments/project definition.", 'bright_red', 'bold'), "\n";     $error_case = 1; $show_values = 1; }
    if($tags_set && ! defined $tags)        { print colored("❌ Error processing arguments/tags definition.", 'bright_red', 'bold'), "\n";        $error_case = 1; $show_values = 1; }
    if($class_set && ! defined $class)      { print colored("❌ Error processing arguments/class definition.", 'bright_red', 'bold'), "\n";       $error_case = 1; $show_values = 1; }
    #if($dd_set && ! defined $datadirs)     { print colored("❌ Error processing arguments/schema definition.", 'bright_red', 'bold'), "\n";       $error_case = 1; $show_values = 1; }
    if($kp_set && ! defined $keypairs)      { print colored("❌ Error processing arguments/keypairs definition.", 'bright_red', 'bold'), "\n";    $error_case = 1; $show_values = 1; }
    if($cust_set && ! defined $custom)      { print colored("❌ Error processing arguments/custom definition.", 'bright_red', 'bold'), "\n";      $error_case = 1; $show_values = 1; }
    if($linkids_set && ! defined $linkids)  { print colored("❌ Error processing arguments/linkids definition.", 'bright_red', 'bold'), "\n";     $error_case = 1; $show_values = 1; }
    if($tem_set && ! defined $template)     { print colored("❌ Error processing arguments/template definition.", 'bright_red', 'bold'), "\n";    $error_case = 1; $show_values = 1; }
    if($sch_set && ! defined $schema)       { print colored("❌ Error processing arguments/schema definition.", 'bright_red', 'bold'), "\n";      $error_case = 1; $show_values = 1; }
    if($ff_set && ! defined $from_file)     { print colored("❌ Error processing arguments/from-file definition.", 'bright_red', 'bold'), "\n";   $error_case = 1; $show_values = 1; }
    if($update_set && ! defined $update_id) { print colored("❌ Error processing arguments/update-existing definition.", 'bright_red', 'bold'), "\n";   $error_case = 1; $show_values = 1; }

    # flag to determine whether we create a new record or update an existing record
    if($update_set) { $enable_create_mode = 0; }

    if($show_values || $verbose || ! $auto_mode)
    {
        print("\nDetected input values:\n");
        print("=====================\n");
        print("Title:           $title\n");
        print("Description:     $description\n");
        print("Filetype:        $filetype\n");
        print("Project:         $project\n");
        print("Tags:            $tags\n");
        print("Class:           $class\n");
        #print(" $dd    \n");
        print("KeyPairs:        $keypairs\n");
        print("Custom:          $custom\n");
        print("LinkedIds:       $linkids\n");
        print("Template:        $template\n");
        print("Schema:          $schema\n");
        print("From-file:       $from_file\n");
        print("Update-existing: $update_id\n");
        print("=====================\n\n");

        if($error_case) { exit 1; }

        if($enable_create_mode && ! length $from_file)
        {
            print colored("WARNING. No value set for '--from-file', this will\nresult in the automatic creation of a blank/empty note.\n"
                          . "This may not be what you intended (?).\n\nEnsure to specify a valid file if you want to\n"
                          . "automatically populate/copy across information into the new note.\n\n", 'bright_yellow', 'bold');
        }

        if($enable_create_mode)
        {
            print colored("(NOTE: Use --auto flag to skip this confirmation/display entirely)", 'bright_green'), "\n\n";
            print colored("Create note with above settings? (Input 'y' to proceed)\n> ", 'bright_green', 'bold');
            my $create_note = <STDIN>;
            chomp($create_note);
            if($create_note ne "y") { exit 0; }
        }
    }

    if($enable_create_mode && (! length $title || ! length $filetype))
    {
        print colored("❌ Error. At minimum both title and filetype must be supplied in order to create a note.",
                      'bright_red', 'bold'), "\n";
        exit (1);
    }

}

sub batch_update_existing_record
{
    if(!length $update_id || length $update_id < 3)
    {
        print "Please enter at least three digits/characters for UUID match.\n";
        print "Aborting\n";
        exit 1;
    }

    my $results = "";

    my $search_cmd_main = "$ScribbleWibble::db_location";
    my $search_cmd = "recsel -t ScribbleWibble_Note -e \"NoteId ~ '$update_id'\" ";
    my $search_exec_main = $search_cmd . $search_cmd_main;
    my $start_line_num = -1;
    my $end_line_num = -1;

    if ($op_mode eq "RIPGREP")
    {
        $results = rg_search_by_field("NoteId", 0, $update_id, 0, 1, "", 0);
    }
    elsif ($op_mode eq "RECUTILS")
    {
        $results = `$search_exec_main`;
    }
    else
    { print "Unknown mode '$op_mode'. Aborting.\n"; exit(1); }

    my @note_ids = ( $results =~ /^NoteId:\s*(.*)$/gm );

    if((scalar @note_ids) == 0)
    {
        print colored("No matches. Please specify a unique UUID or leading UUID substring for note to update.", 'bright_red'), "\n";
        exit(1);
    }
    if((scalar @note_ids) > 1)
    {
        print colored("More than one note matches that UUID substring. " .
                      "Please specify more characters to provide a singular unambiguous match.", 'bright_red'), "\n";
        exit(1);
    }

    if((scalar @note_ids) != 1) { print "Exiting. No singular valid match."; exit(0); }

    my @date       = ( $results =~ /^Date:\s*(.*)$/m );
    my @note_title = ( $results =~ /^Title:\s*(.*)$/m );
    my @note_id    = ( $results =~ /^NoteId:\s*(.*)$/m );
    my @note_type  = ( $results =~ /^Filetype:\s*(.*)$/m );
    my @note_desc  = ( $results =~ /^Description:\s*(.*)$/m );
    my @note_proj  = ( $results =~ /^Project:\s*(.*)$/m );
    my @note_tags  = ( $results =~ /^Tags:\s*(.*)$/m );
    my @note_class = ( $results =~ /^Class:\s*(.*)$/m );
    my @note_dirs  = ( $results =~ /^DataDirs:\s*(.*)$/m );
    my @key_pairs  = ( $results =~ /^KeyPairs:\s*(.*)$/m );
    my @note_cust  = ( $results =~ /^Custom:\s*(.*)$/m );
    my @note_links = ( $results =~ /^LinkedIds:\s*(.*)$/m );

    my $upd_date = $date[0];
    my $upd_title = "";

    my $upd_desc = "";
    my $upd_type = "";
    my $upd_proj = "";

    my $upd_tags = "";
    my $upd_class = "";
    my $upd_pairs = "";
    my $upd_cust = "";
    my $upd_linkids = "";

    # Set fields which have been specified for update
    if(length $title)       { $upd_title = $title;      } else { $upd_title = $note_title[0];   }
    if(length $filetype)    { $upd_type = $filetype;    } else { $upd_type = $note_type[0];     }
    if(length $description) { $upd_desc = $description; } else { $upd_desc = $note_desc[0];     }
    if(length $project)     { $upd_proj = $project;     } else { $upd_proj = $note_proj[0];     }
    if(length $tags)        { $upd_tags = $tags;        } else { $upd_tags = $note_tags[0];     }
    if(length $class)       { $upd_class = $class;      } else { $upd_class = $note_class[0];   }
    if(length $keypairs)    { $upd_pairs = $keypairs;   } else { $upd_pairs = $key_pairs[0];    }
    if(length $custom)      { $upd_cust = $custom;      } else { $upd_cust = $note_cust[0];     }
    if(length $linkids)     { $upd_linkids = $linkids;  } else { $upd_linkids = $note_links[0]; }

    if(! $auto_mode)
    {
        print colored("\n=================== [ BATCH UPDATE ] ======================", 'bright_yellow', 'bold');
        print colored("\nTitle      :", 'bright_yellow', 'bold'); print " $upd_title";
        print colored("\nFiletype   :", 'bright_yellow', 'bold'); print " $upd_type";
        print colored("\nDescription:", 'bright_yellow', 'bold'); print " $upd_desc";
        print colored("\nProject    :", 'bright_yellow', 'bold'); print " $upd_proj";
        print colored("\nTags       :", 'bright_yellow', 'bold'); print " $upd_tags";
        print colored("\nClass      :", 'bright_yellow', 'bold'); print " $upd_class";
        print colored("\nDataDirs   :", 'bright_yellow', 'bold'); print " $note_dirs[0]";
        print colored("\nKeyPairs   :", 'bright_yellow', 'bold'); print " $upd_pairs";
        print colored("\nCustom     :", 'bright_yellow', 'bold'); print " $upd_cust";
        print colored("\nLinkedIds  :", 'bright_yellow', 'bold'); print " $upd_linkids";
        print colored("\n===========================================================\n\n", 'bright_yellow', 'bold');

        print "Proceed? (y/n) > ";
        my $confirm = <STDIN>;
        chomp $confirm;

        if($confirm ne "y")
        {
            exit 0;
        }
    }

    # archive search is 0 for batch
    update_record_metadata($note_id[0],      $upd_type,    $upd_date,
                           $upd_title,      $upd_desc,     $upd_proj,
                           $upd_tags,       $upd_class,    $note_dirs[0],
                           $upd_pairs,      $upd_cust,     $upd_linkids,
                           0,               $verbose,      $from_file,
                           $note_type[0],   $note_proj[0], $op_mode);
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $op_mode = set_operation_mode($verbose);

    if(scalar @{$args} == 0)
    {
        print description, "\n";
        exit 0;
    }

    check_args($args);

    if($enable_create_mode)
    {
        extern_batch_insert_new_note($title,    $description, $filetype,
                                     $project,  $tags,        $class,
                                     $datadirs, $keypairs,    $custom,
                                     $linkids,  $template,    $schema,
                                     $from_file, $update_id,  $op_mode);
     }
     else
     {
        batch_update_existing_record;
     }
}

1;
