# ABSTRACT: Delete an existing note (and any attached data)
# Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::delete;
use ScribbleWibble -command;
use ScribbleWibble::common_funcs qw ( parameter_check set_operation_mode make_tmp_db_backup );
use ScribbleWibble::rg_search qw ( rg_search_by_field );
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use File::Copy;
use File::Path;
use Fcntl qw(SEEK_SET SEEK_CUR SEEK_END); # more readable than 0, 1, 2
use Term::ANSIColor;
use UUID 'uuid';
use strict;
use warnings;
use 5.010;

my $ark = 0;
my $op_mode = undef;
my $uuid;
my $verbose = 0;

sub description
{
    my $desc  = "Use this command to DELETE an existing note by specifying its UUID.\n";
    $desc .= "Usage: wibble delete [--archived] <uuid>\n\n";
    $desc .= "If only part of the UUID is specified, attempt a substring match.\n";
    return $desc;
}

sub opt_spec
{
    ["archived|K" => "Delete a note from the archived notes database instead."],
}

sub perform_delete
{

    if(!length $uuid || length $uuid < 3)
    {
        print "Please enter at least three digits/characters for UUID match:\n";
        print "> ";
        $uuid = <STDIN>;
        chomp($uuid);
    }

    my $results;
    my $search_cmd_archived = "$ScribbleWibble::archived_db_location";
    my $search_cmd_main = "$ScribbleWibble::db_location";

    my $search_cmd = "recsel -t ScribbleWibble_Note -e \"NoteId ~ '$uuid'\" ";
    my $search_exec_main = $search_cmd . $search_cmd_main;
    my $search_exec_archived = $search_cmd . $search_cmd_archived;

    my $start_line_num = -1;
    my $end_line_num = -1;

    if ($op_mode eq "RIPGREP")
    {
        $results = rg_search_by_field("NoteId", 1, $uuid, $ark, $verbose, "", 1);

        # Find the line number corresponding to the rg match
        if ($results =~ /^(\d+):/gm)
        {
            $start_line_num = $1 - 2;
            $end_line_num = $start_line_num + 12;
        }

        if($start_line_num < 0 || $end_line_num < 0)
        {
            print colored("No matches. Please specify a unique UUID or leading UUID substring for note to delete.", 'bright_red'), "\n";
            exit(1);
        }
        else
        {
            $results = rg_search_by_field("NoteId", 1, $uuid, $ark, $verbose, "", 0);
        }
    }
    elsif ($op_mode eq "RECUTILS")
    {
        if   (!$ark) { $results = `$search_exec_main`; }
        else         { $results = `$search_exec_archived`; }
    }
    else
    { print "Unknown mode '$op_mode'. Aborting.\n"; exit(1); }

    my @note_ids    = ( $results =~ /^NoteId:\s*(.*)$/gm );

    if((scalar @note_ids) == 0)
    {
        print colored("No matches. Please specify a unique UUID or leading UUID substring for note to delete.", 'bright_red'), "\n";
        exit(1);
    }
    if((scalar @note_ids) > 1)
    {
        print colored("More than one note matches that UUID substring. " .
                      "Please specify more characters to provide a singular unambiguous match.", 'bright_red'), "\n";
        exit(1);
    }

    if((scalar @note_ids) != 1) { print "Exiting. No singular valid match."; exit(0); }

    my @date       = ( $results =~ /^Date:\s*(.*)$/m );
    my @note_title = ( $results =~ /^Title:\s*(.*)$/m );
    my @note_id    = ( $results =~ /^NoteId:\s*(.*)$/m );
    my @note_type  = ( $results =~ /^Filetype:\s*(.*)$/m );
    my @note_desc  = ( $results =~ /^Description:\s*(.*)$/m );
    my @note_proj  = ( $results =~ /^Project:\s*(.*)$/m );
    my @note_tags  = ( $results =~ /^Tags:\s*(.*)$/m );
    my @note_class = ( $results =~ /^Class:\s*(.*)$/m );
    my @note_dirs  = ( $results =~ /^DataDirs:\s*(.*)$/m );
    my @key_pairs  = ( $results =~ /^KeyPairs:\s*(.*)$/m );
    my @note_cust  = ( $results =~ /^Custom:\s*(.*)$/m );
    my @note_links = ( $results =~ /^LinkedIds:\s*(.*)$/m );

    if(! length $date[0])        { $date[0]         = "___FIELD_MISSING___"; }
    if(! length $note_title[0])  { $note_title[0]   = "___FIELD_MISSING___"; }
    if(! length $note_id[0])     { $note_id[0]      = "___FIELD_MISSING___"; }
    if(! length $note_type[0])   { $note_type[0]    = "___FIELD_MISSING___"; }
    if(! length $note_desc[0])   { $note_desc[0]    = "___FIELD_MISSING___"; }
    if(! length $note_proj[0])   { $note_proj[0]    = "___FIELD_MISSING___"; }
    if(! length $note_tags[0])   { $note_tags[0]    = "___FIELD_MISSING___"; }
    if(! length $note_class[0])  { $note_class[0]   = "___FIELD_MISSING___"; }
    if(! length $note_dirs[0])   { $note_dirs[0]    = "___FIELD_MISSING___"; }
    if(! length $key_pairs[0])   { $key_pairs[0]    = "___FIELD_MISSING___"; }
    if(! length $note_cust[0])   { $note_cust[0]    = "___FIELD_MISSING___"; }
    if(! length $note_links[0])  { $note_links[0]   = "___FIELD_MISSING___"; }

    if($ark == 0)
    {
        print "\nNOTE. Deletion is permanent. Consider archiving the note instead if the information/data attachments may still be of future value.\n\n";
        print "Deletion action performs the following actions:\n\n";
        print "1. Deletes the note file.\n";
        print "2. Removes the note metadata from its associated database (main or archived).\n";
        print "3. Deletes the associated note data directory and all files within, if it has one.\n\n";
    }

    print "DELETE the following note?\n\n";
    print "===============================================\n";
    print "Date       : $date[0]\n";
    print "Title      : $note_title[0]\n";
    print "NoteId     : $note_id[0]\n";
    print "Description: $note_desc[0]\n";
    print "Filetype   : $note_type[0]\n";
    print "Project    : $note_proj[0]\n";
    print "Tags       : $note_tags[0]\n";
    print "Class      : $note_class[0]\n";
    print "DataDirs   : $note_dirs[0]\n";
    print "KeyPairs   : $key_pairs[0]\n";
    print "Custom     : $note_cust[0]\n";
    print "LinkedIds  : $note_links[0]\n";
    print "===============================================\n";

    my $note_dd = 0;
    my $note_dir_to_delete; my $note_dd_prefix;
    my $att_delete;

    my $note_prefix = substr $note_id[0], 0, 2;
    my $note_file_base_path;

    if(! $ark)
    {
        $note_file_base_path = "$ScribbleWibble::wibble_store/notes/$note_type[0]/$note_proj[0]";
    }
    else
    {
        $note_file_base_path = "$ScribbleWibble::wibble_store/archived_notes/$note_type[0]/$note_proj[0]";
    }

    my $note_file_path = $note_file_base_path . "/$note_prefix";
    my $note_file_to_delete = $note_file_path . "/$note_id[0].$note_type[0]";

    if($note_dirs[0] ne "___NULL___")
    {
        $note_dd = 1;
        $note_dir_to_delete = (split / /, $note_dirs[0])[0];
        $note_dd_prefix = substr $note_dir_to_delete, 0, 2;
        $att_delete = "$ScribbleWibble::wibble_store/note_data/$note_dd_prefix/$note_dir_to_delete";
    }

    print colored("\nNote file pending deletion:\n\n$note_file_to_delete", 'bright_red', 'bold'), "\n\n";
    if($note_dd)
    {
        print colored("WARNING: Note has an associated data directory.\n" .
                      "Deletion of the note will also DELETE the data directory and ALL files/data contained within.", 'bright_red', 'bold'), "\n\n";
        print colored("Directory to be removed is:\n\n$att_delete \n", 'bright_red', 'bold'), "\n";
        my $data_storage = `du -hs $att_delete | cut -f 1`;
        chomp($data_storage);
        print colored("Above directory contains " . $data_storage . " of data.", 'bright_red'), "\n\n";
        print colored("CONFIRM: Really delete the above data directory AND note? THIS ACTION CANNOT BE UNDONE.", 'bright_red', 'bold'), "\n\n";

    }
    else { print colored("CONFIRM: Really delete the above note? THIS ACTION CANNOT BE UNDONE.", 'bright_red', 'bold'), "\n\n"; }

    print colored("Type 'delete' (without quotes) to proceed with deletion, any other input to cancel.", 'bright_red', 'bold'), "\n";
    print "> ";
    my $firstcheck = <STDIN>;
    chomp($firstcheck);
    if($firstcheck eq "delete")
    {
        my $recdel = 0;
        print colored("Final confirmation. Type 'YES' (all capitals) to permanently delete note data as listed above.", 'bright_red', 'bold'), "\n";
        print "> ";
        my $finalcheck = <STDIN>;
        chomp($finalcheck);
        if($finalcheck eq "YES")
        {
            print "Deletion in progress, please wait...\n";
            # Assume commands are successful, flag will change if anything fails
            # Update: now aborts rest of operation if metadata update fails
            my $success = 1;

            if($op_mode eq "RIPGREP")
            {
                my $database_file = make_tmp_db_backup($ark);

                open (my $file_handle, '<', "$database_file") or die "$!: $database_file";
                my @db_file = <$file_handle>;
                # Array indices start at 0, and we want to stop on the line BEFORE -> hence - 2
                my @db_region_before = @db_file[0..($start_line_num - 3)];
                # shift onto start of next record, already capturing blank line from above
                my @db_region_after = @db_file[($end_line_num - 1)..(scalar @db_file - 1)];
                my @new_db_file = (@db_region_before, @db_region_after);
                close($file_handle);

                print "Checking integrity of new database...\n";

                my $integrity_check = ((scalar @db_file) - (scalar @new_db_file));
                if($integrity_check == 13)
                {
                    print colored("✓ Integrity check passed.", 'bright_green'), "\n";
                }
                else
                {
                    print colored("❌ Integrity check failed. Aborting deletion operation. Existing database file has not been altered.\n",
                                  'bright_red', 'bold'), "\n";
                    exit(1);
                }

                my $replacement_db = "$ScribbleWibble::tmp_dir/newdb.rec";
                open ($file_handle, '>', "$replacement_db") or die "$!: Unable to open $replacement_db for writing.";
                foreach(@new_db_file)
                {
                    print $file_handle $_;
                }
                close($file_handle);

                my $replace_db = move($replacement_db, $database_file);
                if ($replace_db)
                {
                    $success = 1;
                    if(! $ark) { print colored("✓ Main database successfully updated.", 'bright_green'), "\n"; }
                    else       { print colored("✓ Archived database successfully updated.", 'bright_green'), "\n"; }
                }
                else
                {
                    print colored("❌ Error updating metadata.", 'bright_green'), "\n";
                    $success = 0;
                    exit(1);
                }
            }
            elsif($op_mode eq "RECUTILS")
            {
                # here we do the actual deletion...
                my $del_cmd = "recdel -t ScribbleWibble_Note -e \"NoteId = '$note_id[0]'\" ";
                my $del_exec_main = $del_cmd . $search_cmd_main . " --force";
                my $del_exec_archived = $del_cmd . $search_cmd_archived . "--force";

                if(! $ark) { $recdel = system($del_exec_main); }
                else       { $recdel = system($del_exec_archived); }

                if($recdel == 1)
                {
                    print colored("❌ Error updating metadata.", 'bright_green'), "\n";
                    $success = 0;
                    exit(1);
                }
            }
            else
            {
                print colored ("Invalid mode '$op_mode' specified. Aborting.", 'bright_red'), "\n";
                exit(1);
            }

            if($success) { print colored("✓ Note metadata successfully deleted.", 'bright_green', 'bold'), "\n"; }
            else         { print colored("❌ Problem removing note metadata.", 'bright_red', 'bold'), "\n";       }

            my $delete_note_file = system("rm $note_file_to_delete");
            if($delete_note_file == 1) { $success = 0; }

            # remove note prefix directory if empty; if not, no harm
            system("rmdir --ignore-fail-on-non-empty $note_file_path");
            # remove note project directory if empty; if not, no harm
            system("rmdir --ignore-fail-on-non-empty $note_file_base_path");

            if($note_dd)
            {
                #print "DEBUG: find $att_delete -type f -print";
                print colored("Deleting data directory and all files within...", 'bright_green'), "\n";
                my $delete_all_files = system("find $att_delete -print -delete");
                if($delete_all_files == 1) { $success = 0; }

                #print "DEBUG: rmdir $ScribbleWibble::wibble_store/note_data/$note_dd_prefix";
                my $remove_dir = system("rmdir $ScribbleWibble::wibble_store/note_data/$note_dd_prefix");
                if($remove_dir == 1) { $success = 0; }

                if($success) { print colored("✓ Note data directory and associated files successfully deleted.", 'bright_green', 'bold'), "\n"; }
                else         { print colored("❌ Problem deleting note data directory/and or files. Check manually.", 'bright_red', 'bold'), "\n"; }
            }

            if($success) { print colored("✓ Note and all/any associated note data deleted as requested. Note metadata removed from database.", 'bright_green', 'bold'), "\n"; }
            else         { print colored("❌ Issue deleting either metadata and/or attached files. Please check manually.", 'bright_red', 'bold'), "\n"; }
        }
    }
}

sub execute
{

    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $config_override = check_config_override(@_);

    if($opt->{archived}) { $ark = 1; }

    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $uuid = "@{$args}";

    $op_mode = set_operation_mode($verbose);

    if($verbose)
    {
        if($ark == 1)  { print colored ("--archived: ENABLED", 'bright_green'), "\n"; }
        else           { print colored ("--archived: DISABLED", 'bright_red'), "\n"; }
    }

    if(length $uuid) { perform_delete; }
    else             { print description;    }

}

1;
