# ABSTRACT: Bookmark an existing note
# Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::bookmark;
use ScribbleWibble -command;
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use ScribbleWibble::common_funcs qw ( set_operation_mode );
use ScribbleWibble::rg_search qw ( rg_search_by_field );
use ScribbleWibble::ui_elems qw ( print_hl padded_number );
use File::Basename;
use File::Copy;
use File::Path;
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use UUID 'uuid';
use strict;
use warnings;

my $ark = 0;
my $add_bm = 0;
my $bm;
my $del_bm = 0;
my $ls_bm = 0;
my $open_bm = 0;
my $op_mode = "NULL";
my $uuid;
my $use_gui = 0;
my $verbose;

sub description
{
    my $desc  = "Use this command to bookmark a note by specifying its UUID.\n";
    $desc .= "Usage: wibble bookmark [--archived | --add | --delete | --ls | --open] <bookmark num>\n\n";
    $desc .= "If only part of the UUID is specified, attempt a substring match.\n";
    $desc .= "If no switch is specified, assume --ls.\n";
    $desc .= "If a numeric argument is supplied, assume --open and attempt --open for the specified bookmark.\n";
    
    return $desc;
}

sub opt_spec
{
    ["archived|K" => "Bookmark a note by searching the archived notes database instead."],
    ["add|A"      => "Add a new bookmark for a note."],
    ["delete|D"   => "Delete an existing note bookmark."],
    ["ls|L"       => "List defined note bookmarks."],
    ["open|O"     => "Open a bookmarked note."],
}

sub find_note
{
    my ($note_uuid) = @_;
    chomp($note_uuid);
    my $results;

    if($op_mode eq "RIPGREP")
    {
        $results = rg_search_by_field("NoteId", 1, $note_uuid, 0, 0, "", "");
    }
    elsif ($op_mode eq "RECUTILS")
    {
        my $search_cmd_archived = "$ScribbleWibble::archived_db_location";
        my $search_cmd_main     = " $ScribbleWibble::db_location";

        my $search_cmd =
          "recsel -t ScribbleWibble_Note -e \"NoteId ~ '$note_uuid'\" ";
        my $search_exec_main     = $search_cmd . $search_cmd_main;
        my $search_exec_archived = $search_cmd . $search_cmd_archived;

        #print "EXECUTING: $search_exec_main\n";
        if   (!$ark) { $results = `$search_exec_main`; }
        else         { $results = `$search_exec_archived`; }

    }
    else
    {
        print "Unknown mode '$op_mode'. Aborting.\n";
        exit(1);
    }
    return $results;
}

sub display_bookmarks
{
    my ($quick_open) = @_;
    
    open (my $file_handle, '<', $ScribbleWibble::bookmarks) or die "$!: $ScribbleWibble::bookmarks";
    my @bookmarks = <$file_handle>;
    close($file_handle);

    if($quick_open) { return @bookmarks; }
    else
    {
        my $total_results = scalar @bookmarks;
        my $padded_x;
        my $x = 0;
        for my $line(@bookmarks)
        {
            my $note_id       =  (split (/\|/, $line))[0];
            my $note_id_short =  substr $note_id, 0, 8;
            my $note_desc     = (split /\|/, $line)[1];

            $padded_x = padded_number($total_results, $x + 1);
            print colored("[$padded_x] - [$note_id_short...] ", 'bright_yellow'); print "- $note_desc";
            ++$x;
        }

        return @bookmarks;
    }
}

sub delete_bookmark
{
    print "\nDELETE bookmark\n";
    print_hl; print "\n";

    my @bookmarks = display_bookmarks(0);
    my $total_results = scalar @bookmarks;

    print_hl;
    print "\nEnter "; print colored("bookmark", 'bright_yellow'); print " to "; print colored("DELETE ", 'bright_red'); print "> ";
    my $res_num = <STDIN>;
    chomp($res_num);
    
    if(looks_like_number($res_num) && $res_num >= 1 && ($res_num <= $total_results))
    {
        print colored("Confirm removal of bookmark [$res_num] (y/n)? > ", 'bright_red');
        my $confirm = <STDIN>;
        chomp($confirm);
        if($confirm eq "y")
        {
            delete $bookmarks[$res_num - 1];
            open (my $file_handle, '>', $ScribbleWibble::bookmarks) or die "$!: $ScribbleWibble::bookmarks";
            foreach my $entry (@bookmarks)
            {
                if(! defined $entry) { next; }
                print $file_handle $entry;
            }
            close($file_handle);
            print "Bookmark successfully removed\n";
        }
    }
}

sub open_bookmark
{
    my ($bookmark_num, $ls_only) = @_;
    my $quick_open = 0;
    if(defined $bookmark_num && looks_like_number($bookmark_num))
    {
        $quick_open = 1;
    }
    elsif(defined $bookmark_num && length $bookmark_num)
    {
        print colored("Invalid bookmark number '$bookmark_num'. Please enter a valid input.", 'bright_red'), "\n";
        print "\nBookmarks\n";
        print_hl; print "\n";
        $ls_only = 1;
    }
    elsif($ls_only)
    {
        print "\nBookmarks\n";
        print_hl; print "\n";
    }
    else
    {
        print "\nOpen bookmark\n";
        print_hl; print "\n";
    }
    
    my @bookmarks = display_bookmarks($quick_open);
    if($ls_only) { print_hl; exit(0); }
    my $total_results = scalar @bookmarks;

    if($total_results == 0)
    { print colored("No bookmarks defined. Please add one using --add. Exiting.", 'bright_red'), "\n"; exit(0); }

    my $res_num;
    if(! $quick_open)
    {
        print_hl;
        print "\nEnter "; print colored("bookmark", 'bright_yellow'); print " to open > ";

        $res_num = <STDIN>;
        chomp($res_num);
    }
    else
    { $res_num = $bookmark_num; }
    
    if(looks_like_number($res_num) && $res_num >= 1 && ($res_num <= $total_results))
    {
        my $note_details = find_note($bookmarks[$res_num - 1]); 

        my $note_id   = ( $note_details =~ /^NoteId:\s*(.*)$/m )[0];
        my $filetype  = ( $note_details =~ /^Filetype:\s*(.*)$/m )[0];
        my $project  =  ( $note_details =~ /^Project:\s*(.*)$/m  )[0];

        my $notepath_prefix = substr $note_id, 0, 2;

        my $output_path_main = "$ScribbleWibble::wibble_store/notes/$filetype/$project/$notepath_prefix/$note_id.$filetype";
        my $output_path_ark  = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$project/$notepath_prefix/$note_id.$filetype";
        my $output_path;
        if    (-f $output_path_main) { $output_path = $output_path_main; }
        elsif (-f $output_path_ark ) { $output_path = $output_path_ark;  }
        else
        {
            print "Unable to find note '$note_id' in either main store or archived.\n";
            print "Remove bookmark?\n";
            # implement removal
            exit(0);
        }

        if($use_gui) { print "Opening $output_path.\n"; system("$ScribbleWibble::gui_editor $output_path"); }
        else         { print "Opening $output_path.\n"; system("$ScribbleWibble::editor $output_path");     }
    }
    else
    {
        print colored("Unknown/invalid input: '$res_num' does not match a listed bookmark. Aborting.", 'bright_red'), "\n";
        print "\nListing defined bookmarks:\n\n";
        display_bookmarks(0);
    }
    
}

sub add_bookmark
{

    if(!length $uuid || length $uuid < 3)
    {
        print "Please enter at least three digits/characters for UUID match:\n";
        print "> ";
        $uuid = <STDIN>;
        chomp($uuid);
    }

    my $results = find_note($uuid, $op_mode);
    
    my @note_ids    = ( $results =~ /^NoteId:\s*(.*)$/gm );

    if((scalar @note_ids) == 0)
    {
        print colored("No matches. Please specify a unique UUID or leading UUID substring for note to delete.", 'bright_red'), "\n"; 
        exit(1);
    }
    if((scalar @note_ids) > 1)
    {
        print colored("More than one note matches that UUID substring. " .
                      "Please specify more characters to provide a singular unambiguous match.", 'bright_red'), "\n"; 
        exit(1);
    }


    if((scalar @note_ids) != 1) { print "Exiting. No singular valid match."; exit(0); }
        
    my @date       = ( $results =~ /^Date:\s*(.*)$/m );
    my @note_title = ( $results =~ /^Title:\s*(.*)$/m );
    my @note_id    = ( $results =~ /^NoteId:\s*(.*)$/m );
    my @note_type  = ( $results =~ /^Filetype:\s*(.*)$/m );
    my @note_desc  = ( $results =~ /^Description:\s*(.*)$/m );
    my @note_proj  = ( $results =~ /^Project:\s*(.*)$/m );
    my @note_tags  = ( $results =~ /^Tags:\s*(.*)$/m );
    my @note_class = ( $results =~ /^Class:\s*(.*)$/m );
    my @note_dirs  = ( $results =~ /^DataDirs:\s*(.*)$/m );
    my @key_pairs  = ( $results =~ /^KeyPairs:\s*(.*)$/m );
    my @note_cust  = ( $results =~ /^Custom:\s*(.*)$/m );
    my @note_links = ( $results =~ /^LinkedIds:\s*(.*)$/m );

    if(! length $date[0])        { $date[0]         = "___FIELD_MISSING___"; }
    if(! length $note_title[0])  { $note_title[0]   = "___FIELD_MISSING___"; }
    if(! length $note_id[0])     { $note_id[0]      = "___FIELD_MISSING___"; }
    if(! length $note_type[0])   { $note_type[0]    = "___FIELD_MISSING___"; }
    if(! length $note_desc[0])   { $note_desc[0]    = "___FIELD_MISSING___"; }
    if(! length $note_proj[0])   { $note_proj[0]    = "___FIELD_MISSING___"; }
    if(! length $note_tags[0])   { $note_tags[0]    = "___FIELD_MISSING___"; }
    if(! length $note_class[0])  { $note_class[0]   = "___FIELD_MISSING___"; }
    if(! length $note_dirs[0])   { $note_dirs[0]    = "___FIELD_MISSING___"; }
    if(! length $key_pairs[0])   { $key_pairs[0]    = "___FIELD_MISSING___"; }
    if(! length $note_cust[0])   { $note_cust[0]    = "___FIELD_MISSING___"; }
    if(! length $note_links[0])  { $note_links[0]   = "___FIELD_MISSING___"; }

    print "Bookmark the following note?\n\n";
    print "===============================================\n";
    print "Date       : $date[0]\n";
    print "Title      : $note_title[0]\n";
    print "NoteId     : $note_id[0]\n";
    print "Description: $note_desc[0]\n";
    print "Filetype   : $note_type[0]\n";
    print "Project    : $note_proj[0]\n";
    print "Tags       : $note_tags[0]\n";
    print "Class      : $note_class[0]\n";
    print "DataDirs   : $note_dirs[0]\n";
    print "KeyPairs   : $key_pairs[0]\n";
    print "Custom     : $note_cust[0]\n";
    print "LinkedIds  : $note_links[0]\n";
    print "===============================================\n";

    print colored("Create bookmark for the above note? (y/n)", 'bright_green', 'bold'), "\n"; 
    print "> ";
    my $response = <STDIN>;
    chomp($response);
    if($response eq "y")
    {
        open (my $file_handle, '>>', "$ScribbleWibble::bookmarks") or die "$!: $ScribbleWibble::bookmarks";
        print $file_handle "$note_id[0]|" . (substr $note_title[0], 0, 70) . "\n";
        close($file_handle);
        print colored("Bookmark for note '$note_id[0]' added.", 'bright_green'), "\n";
    }
}

sub setup_new_bookmarks_file
{
    print "No bookmark file at $ScribbleWibble::bookmarks. Initialising.\n";
    my $base_path = dirname("$ScribbleWibble::bookmarks");
    my $create_bookmark_dir = mkpath("$base_path");
    if(! -e $base_path) { print "Error creating destination bookmark directory: $base_path\n"; exit(1); }
    my $init_bm_file = system("touch $ScribbleWibble::bookmarks");
    if($init_bm_file eq 0) { print "Fresh bookmarks file at $ScribbleWibble::bookmarks initialised.\n" }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $config_override = check_config_override(@_);

    if($opt->{archived}) { $ark = 1;     }
    if($opt->{add}     ) { $add_bm = 1;  }
    if($opt->{delete}  ) { $del_bm = 1;  }
    if($opt->{ls}    )   { $ls_bm = 1;   }
    if($opt->{open}    ) { $open_bm = 1; } 
        
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $op_mode = set_operation_mode($verbose);
    $bm = "@{$args}";


    if(! -e $ScribbleWibble::bookmarks) { setup_new_bookmarks_file; }
    
    if(defined $bm)
    {
        chomp($bm);
        if(length $bm)
        {
            $open_bm = 1;
            print "Bookmark argument supplied, attempting direct opening of bookmark '[$bm]'.\n";
        }
    }

    if($verbose)
    {
        if($ark == 1)  { print colored ("--archived: ENABLED", 'bright_green'), "\n"; }
        else           { print colored ("--archived: DISABLED", 'bright_red'), "\n"; }
    }

    if($open_bm)   { open_bookmark($bm,0);                                                      }
    elsif($ls_bm)  { open_bookmark($bm,1);                                                      }
    elsif($add_bm) { add_bookmark;                                                            }
    elsif($del_bm) { delete_bookmark;                                                         }
    else           { print "No action specified, defaulting to '--open'.\n"; open_bookmark; } 

}

1;
