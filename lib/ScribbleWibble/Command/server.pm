# ABSTRACT: Start up a web server/socket for web UI
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::server;
use ScribbleWibble -command;
#use base 'App::Cmd::Command';
use ScribbleWibble::common_funcs qw ( parameter_check set_operation_mode );
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy );
use ScribbleWibble::lib_wrapper qw ( execute_search serialize_wibble_records );
use Data::Dumper;
use HTTP::Daemon;
use HTTP::Response;
use HTTP::Status qw(:constants :is status_message);
use JSON::XS;
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use strict;
use warnings;

use utf8;

no warnings qw(experimental::try);
use feature 'try';

my $op_mode = undef;
my $server_start = 0;
my $verbose;
my $c = undef;
my $d = undef;
my $end_server = "START_SERVER";

$SIG{INT} = \&graceful_shutdown;
$SIG{PIPE} = \&sigpipe_warning;

sub opt_spec
{
    return (
        ["start|S" => "Start web-server"],
           );
}

sub description
{
    return "Use this command to start up a web server\n";
}

sub start_server
{

    $d = HTTP::Daemon->new(LocalPort => 22889, ReuseAddr => 1) || die;
    print "\n";
    print colored("[  OK  ] Wibble server started on " . $d->url, 'bright_green', 'bold'), "\n";
    print colored("[  OK  ] Access GUI with via web-browser at " .
                  "http://localhost:3000/wibble/", 'bright_green', 'bold'), "\n";

    while (($c = $d->accept) && ($end_server ne "TERM_SERVER")) 
    {
        while ((my $r = $c->get_request) && ($end_server ne "TERM_SERVER"))
        {
            if ($r->method eq 'GET' and $r->uri->path eq "/hello")
            {
                my $the_message = "<html><body><h1>Hello there!</h1></body></html>";
                my $res = HTTP::Response->new(HTTP_OK);
                $res->header('Content-Type' => 'text/html');
                $res->header('Content-Length' => length($the_message));
                $res->content($the_message);
                
                $c->send_response($res);
            }
            elsif($r->method eq 'OPTIONS')
            {
                #print "DEBUG: ";
                #print Dumper($r);
                    
                my $res = HTTP::Response->new(HTTP_OK);
                $res->header('Access-Control-Allow-Headers' => 'Authorization, Content-Type');
                $res->header('Access-Control-Allow-Methods' => 'OPTIONS, GET, POST');
                $res->header('Access-Control-Allow-Origin' => 'http://localhost:3000');
                $res->header('Access-Control-Max-Age' => '6000');
                $res->header('Allow' => 'OPTIONS, GET, POST');
                $c->send_response($res);
            }
            elsif($r->method eq 'POST')
            {
                try
                {
                    #print "DEBUG: POST request:\n";
                    #print $r->content , "\n";
                    my $raw_message = execute_search($r->content, $op_mode);
                    my $the_message = encode_json $raw_message;
                    #print "DEBUG: POST response:\n";
                    #print Dumper $the_message;
                    my $res = HTTP::Response->new(HTTP_OK);
                    $res->header('Access-Control-Allow-Origin' => 'http://localhost:3000');
                    $res->header('Content-Type' => 'text/json');
                    $res->header('Allow' => 'OPTIONS, GET, POST');
                    $res->header('Content-Length' => length($the_message));
                    $res->content($the_message);
                    $c->send_response($res);
                }
                catch($err)
                {
                    print colored("ERROR - server.pm. Trapped error: $err\n", 'bright_red', 'bold');
                }
            }
            else
            {
                $c->send_error(HTTP_FORBIDDEN);
            }
        }
        $c->close;
        undef($c);
    }

    #print "DEBUG: END_SERVER: $end_server\n";
}

sub dispatch_server
{
    if ($server_start) { start_server(0); }
}

sub graceful_shutdown
{
    # Clean shutdown on ctrl-C
    # https://stackoverflow.com/questions/13132438/httpdaemon-crashing-when-i-stop-the-loop
    print "\n";
    print colored("[ INFO ] Termination request received", 'bright_yellow', 'bold'), "\n";
    $end_server = "TERM_SERVER";
    if(defined $d)
    {
        $d->shutdown(2);
        undef($d);
    }
    kill(2, getppid());
    print colored("[  OK  ] Shutdown complete: Exiting", 'bright_green', 'bold'), "\n";
    exit(0);
}

sub sigpipe_warning
{
    print colored("[ INFO ] Received SIGPIPE (141): Ignoring", 'bright_yellow', 'bold'), "\n";
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $enabled_modes = 0;

    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $op_mode = set_operation_mode($verbose);

    if($opt->{start})
    {
        $server_start = 1;
        ++$enabled_modes;
    }

    if($enabled_modes > 1 || $verbose)
    {
        if($enabled_modes gt 1)
        {
            print "Please choose only one option out of [ --start ]\n";
            print "❌ Error: Multiple conflicting options.\n";
            exit(0);
        }

        if($server_start == 1) { print colored ("--start     : ENABLED", 'bright_green'), "\n"; }
        else                   { print colored ("--start     : DISABLED", 'bright_red'), "\n";  }
    }

    if($enabled_modes == 0) { print "No action specified. Pass in '--start' to enable the server.\n\n"; }

    dispatch_server();
}

1;
