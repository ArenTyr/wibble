# ABSTRACT: Create a new note
# Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::create;
use ScribbleWibble -command;
use ScribbleWibble::common_funcs qw ( open_file_ro parameter_check set_operation_mode open_file_handle_CLOBBER );
use ScribbleWibble::rg_search qw ( rg_search_by_field rg_delete_entry );
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use Data::Dumper;
use Exporter 'import';
use File::Copy;
use File::Path;
use POSIX qw(strftime);
use Term::ANSIColor;
use Term::ReadLine;
use UUID 'uuid';
use constant INPUT_ATTEMPTS => 3;
use strict;
use warnings;

no warnings qw(experimental::smartmatch);

use feature qw(switch);

our @EXPORT_OK = qw ( extern_batch_insert_new_note );

my $class = "";
my $create_note = "";
my $custom = "";
my $datadirs = "";
my $db_file = "wibble_db.rec";
#my $db_skel = "wibble_db_skel.rec";
my $description = "";
my $filetype = "";
my $ft = "";
my $keypairs = "";
my $linkids = "";
my $op_mode = undef;
my $project = "";
my $tags = "";
my $schema = undef;
my $template = undef;
my $template_string = "";
my $title = "";
my $use_schema = 0;
my $use_template = 0;
my $verbose;

my $from_file = undef;

sub setup_fresh_db_file
{
    my ($db_new_file) = @_;
    my $DB_FILE = open_file_handle_CLOBBER($db_new_file);
    print $DB_FILE "# -*- mode: rec -*-\n";
    print $DB_FILE "\n";
    print $DB_FILE "%rec: ScribbleWibble_Note\n";
    print $DB_FILE "%key: NoteId\n";
    print $DB_FILE "%type: NoteId uuid\n";
    print $DB_FILE "%type: Date date\n";
    print $DB_FILE "%auto: NoteId Date\n";
    print $DB_FILE "%sort: Date\n";
    print $DB_FILE "%mandatory: Title Filetype\n";
    print $DB_FILE "\n";

    close($DB_FILE);
}


sub db_check_init
{
    print "\n";
    if(-e "$ScribbleWibble::db_location")
    {
        print colored("✓ Found existing main database.\n", 'bright_green');
    }
    else
    {
        print colored("❌ No database exists under $ScribbleWibble::db_location, initialising.\n", 'bright_red');
        system("mkdir -p \$(dirname $ScribbleWibble::db_location)");
        setup_fresh_db_file($ScribbleWibble::db_location);
        #system("cp $ScribbleWibble::skel_dir/$db_skel \$(dirname $ScribbleWibble::db_location)/\$(basename $ScribbleWibble::db_location)");
    }

    if(-e "$ScribbleWibble::archived_db_location")
    {
        print colored("✓ Found existing archive database.\n", 'bright_green');
    }
    else
    {
        print colored("❌ No archive database exists under $ScribbleWibble::archived_db_location, initialising.\n", 'bright_red');
        system("mkdir -p \$(dirname $ScribbleWibble::archived_db_location)");
        setup_fresh_db_file($ScribbleWibble::archived_db_location);
        #system("cp $ScribbleWibble::skel_dir/$db_skel \$(dirname $ScribbleWibble::archived_db_location)/\$(basename $ScribbleWibble::archived_db_location)");
    }

    if(! -e "$ScribbleWibble::wibble_store/archived_notes" ||
       ! -e "$ScribbleWibble::wibble_store/notes" ||
       ! -e "$ScribbleWibble::wibble_store/note_data" ||
       ! -e "$ScribbleWibble::wibble_store/schemas" ||
       ! -e "$ScribbleWibble::wibble_store/templates")
    {
        print colored("❌ Storage directory structure incomplete under $ScribbleWibble::wibble_store, initialising.\n", 'bright_red');
        mkpath("$ScribbleWibble::wibble_store/archived_notes");
        mkpath("$ScribbleWibble::wibble_store/notes");
        mkpath("$ScribbleWibble::wibble_store/note_data");
        mkpath("$ScribbleWibble::wibble_store/schemas");
        mkpath("$ScribbleWibble::wibble_store/templates");
    }
    else
    {
        print colored("✓ Found existing storage directory structure.\n", 'bright_green');
    }
}

sub insert_new_note
{
    my ($batch_create, $from_file) = @_;
    my $batch_mode = 0;
    if(defined $batch_create && $batch_create == 1)
    {
        if($verbose) { print colored("Batch mode in operation.", 'bright_green', 'bold'), "\n"; }
        $batch_mode = 1;
    }

    my $note_id = uuid();
    # The mathematical probability of a UUID collision is so low the universe is likely to end
    # before a collision, but still, we'll check, just to be pedantic...
    # recutils (if used), already does an integrity check, but we'll check manually with ripgrep
    # since only the note UUID is critical as a "primary key"/essential to be unique...
    if($op_mode eq "RIPGREP")
    {
        while(1)
        {
            my $results = rg_search_by_field("NoteId", 1, $note_id, 0, 0, "", 0);
            my @matches_in_main = ( $results =~ /^NoteId:\s*(.*)$/gm );
            $results = rg_search_by_field("NoteId", 1, $note_id, 1, 0, "", 0);
            my @matches_in_ark = ( $results =~ /^NoteId:\s*(.*)$/gm );

            if((scalar @matches_in_main) > 0 || (scalar @matches_in_ark) > 0)
            {
                print colored ("UUID collision! THE UNIVERSE HAS SURELY ENDED! ... OK, generating a fresh one.", 'bright_red', 'bold'), "\n";
                print colored ("Joking aside, this is a remarkable mathematical event, so record this moment in your life"
                            ." and/or contact the author of this software to let them know!", 'bright_red', 'bold'), "\n";
                print colored("Generating a fresh UUID.", 'bright_green'), "\n";
                $note_id = uuid();
            }
            else { last; }
        }
    }

    my $output_path = "";
    my $notepath_prefix = "";

    my $mt_insert = 999;

    if ($op_mode eq "RIPGREP")
    {
        open (my $file_handle, '>>', "$ScribbleWibble::db_location") or die "$!: $ScribbleWibble::db_location";
        my $entry_datetime = strftime "%a, %d %b %Y %T %z", localtime;
        my $record_string  = "\n";
           $record_string .= "Date: " . $entry_datetime . "\n";
           $record_string .= "Title: " . $title . "\n";
           $record_string .= "NoteId: " . $note_id . "\n";
           $record_string .= "Description: " . $description . "\n";
           $record_string .= "Filetype: " . $filetype . "\n";
           $record_string .= "Project: " . $project . "\n";
           $record_string .= "Tags: " . $tags . "\n";
           $record_string .= "Class: " . $class . "\n";
           $record_string .= "DataDirs: " . $datadirs . "\n";
           $record_string .= "KeyPairs: " . $keypairs . "\n";
           $record_string .= "Custom: " . $custom . "\n";
           $record_string .= "LinkedIds: " . $linkids . "\n";

        print $file_handle $record_string;
        close($file_handle);
        $mt_insert = 0;
    }
    elsif($op_mode eq "RECUTILS")
    {
        my $rec_ins_cmd = "";

        $rec_ins_cmd  = "recins -t ScribbleWibble_Note -f Title -v '$title' ";
        $rec_ins_cmd .= "-f NoteId -v '$note_id' -f Description -v '$description' ";
        $rec_ins_cmd .= "-f Filetype -v '$filetype' ";
        $rec_ins_cmd .= "-f Project -v '$project' -f Tags -v '$tags' ";
        $rec_ins_cmd .= "-f Class -v '$class' -f DataDirs -v '$datadirs' ";
        $rec_ins_cmd .= "-f KeyPairs -v '$keypairs' -f Custom -v '$custom' ";
        $rec_ins_cmd .= "-f LinkedIds -v '$linkids' ";
        $rec_ins_cmd .= "$ScribbleWibble::db_location";

        $mt_insert = system("$rec_ins_cmd");
    }
    else
    {
        print "Unknown mode '$op_mode'. Aborting.\n"; exit(1);
    }


    if($mt_insert eq 0)
    {
        print colored("✓ Inserted note metadata for $note_id.\n", 'bright_green');
        $notepath_prefix = substr $note_id, 0, 2;
        $output_path = "$ScribbleWibble::wibble_store/notes/$filetype/$project/$notepath_prefix";
        my @created = mkpath("$output_path");

        my $cpy_template = 1;
        my $template_mode = 0;

        if((! $use_template) && (! length $from_file) && -f "$ScribbleWibble::wibble_store/templates/$filetype/default")
        {
            $cpy_template = copy("$ScribbleWibble::wibble_store/templates/$filetype/default", "$output_path/$note_id.$filetype");
            $template_mode = 1;
            print colored("✓ No template specified, but 'default' exists for filetype $filetype, using 'default'.\n", 'bright_green');
        }
        elsif((! length $from_file) && $use_template)
        {
            $cpy_template = copy("$ScribbleWibble::wibble_store/templates/$filetype/$template", "$output_path/$note_id.$filetype");
            $template_mode = 1;
            print colored("✓ Using $filetype template '$template'.\n", 'bright_green');
        }

        if (! $cpy_template)
        {
            print colored("❌ WARNING. Error copying template.", 'bright_red', 'bold'), "\n";
            print "1. Check that '$ScribbleWibble::wibble_store/templates/$filetype/$template' exists.\n";
            print "2. Check that this template and/or 'default' template are readable.\n";
            print colored("Using blank/no template.", 'bright_green'), "\n";
        }

        # Automatically insert org-mode id support org-roam if filetype is "org" and has relevant template
        # Don't use template if --from-file is enabled (pointless)
        if((! length $from_file) && $cpy_template && $template_mode && $filetype eq "org")
        {
            my @file;
            open (my $file_handle, '<', "$output_path/$note_id.$filetype") or die "$!: $output_path/$note_id.$filetype";
            @file = <$file_handle>;
            close($file_handle);

            my $org_id_check = -1;
            if( $org_id_check = grep { /###ORG_ID###/ } @file)
            {
                s/###ORG_ID###/$note_id/ for @file;

                open (my $file_handle, '>', "$output_path/$note_id.$filetype") or die "$!: $output_path/$note_id.$filetype";
                foreach(@file) { print $file_handle $_; }
                close($file_handle);

                print colored("✓ Org-mode file with ###ORG_ID### stub in template detected, inserted note ID as org-id.\n", 'bright_green');
            }
        }

        if(defined $from_file && length $from_file)
        {
            $from_file =~ s/~/$ENV{HOME}/g;
            my $gen_from_file = copy($from_file, "$output_path/$note_id.$filetype");
            if($gen_from_file)
            {
                print colored("✓ New note successfully created from file $from_file.\n", 'bright_green');
            }
            else
            {
                print colored("❌ Error creating new note from file $from_file. Check that '$from_file' exists and is readable.\n", 'bright_red', 'bold');
            }

        }
        else
        {
            system("$ScribbleWibble::editor $output_path/$note_id.$filetype");
        }
    }
    else
    {
        print colored("❌ Error creating note metadata. Aborting.\n", 'bright_red', 'bold');
        exit 1;
    }

    if(! -e "$output_path/$note_id.$filetype")
    {
        if(! $batch_mode) { print colored("❌ No note created by editor. User aborted?\n", 'bright_red'); }
        else              { print colored("❌ Unable to create new note from file.\n", 'bright_red'); }
        print colored("✓ Cleaning up database/removing entry.\n", 'bright_green');
        if($op_mode eq "RIPGREP")
        {
            rg_delete_entry($note_id, 0, $verbose);
        }
        elsif($op_mode eq "RECUTILS")
        {
            system("recdel -t ScribbleWibble_Note -e \"NoteId = '$note_id'\" $ScribbleWibble::db_location --verbose");
        }
        else
        {
            print colored("ERROR. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                        'bright_red'), "\n";
            exit(1);
        }
        my $delete_empty_dir = system("find $ScribbleWibble::wibble_store/notes/ -type d -empty -delete");

        if($delete_empty_dir eq 0) { print colored("✓ Original note directory removed.\n", 'bright_green'); }
        else { print colored("❌ Error removing original note directory. Run 'wibble tool --clean' to fix.\n", 'bright_red'); }

    }
    else { print colored("✓ Successfully stored note at '$output_path/$note_id.$filetype'.\n", 'bright_green'); }

}

sub extern_batch_insert_new_note
{
    my ($title_extern,     $description_extern, $filetype_extern,
        $project_extern,   $tags_extern,        $class_extern,
        $datadirs_extern,  $keypairs_extern,    $custom_extern,
        $linkids_extern,   $template_extern,    $schema_extern,
        $from_file_extern, $update_id,          $op_mode_extern) = @_;

    parameter_check(\@_, 15, "extern_batch_insert_new_note");

    $title       = $title_extern;
    $description = $description_extern;
    $filetype    = $filetype_extern;
    $project     = $project_extern;
    $tags        = $tags_extern;
    $class       = $class_extern;
    $datadirs    = $datadirs_extern;
    $keypairs    = $keypairs_extern;
    $custom      = $custom_extern;
    $linkids     = $linkids_extern;
    $template    = $template_extern;
    $schema      = $schema_extern;
    $from_file   = $from_file_extern;
    $op_mode     = $op_mode_extern;


    # remove leading/trailing extraneous whitespace/tabs etc
    $title       =~ s/^\s+|\s+$//g ;
    $filetype    =~ s/^\s+|\s+$//g ; $filetype =~ tr/ /_/;
    $description =~ s/^\s+|\s+$//g ;
    $project     =~ s/^\s+|\s+$//g ; $project =~ tr/ /_/;
    $tags        =~ s/^\s+|\s+$//g ;
    $class       =~ s/^\s+|\s+$//g ;
    $datadirs    =~ s/^\s+|\s+$//g ;
    $keypairs    =~ s/^\s+|\s+$//g ;
    $custom      =~ s/^\s+|\s+$//g ;
    $linkids     =~ s/^\s+|\s+$//g ;
    
    if(! length $description) { $description = "___NULL___"; }
    if(! length $project)     { $project     = "General";    }
    if(! length $tags)        { $tags        = "___NULL___"; }
    if(! length $class)       { $class       = "___NULL___"; }
    if(! length $datadirs)    { $datadirs    = "___NULL___"; }
    if(! length $keypairs)    { $keypairs    = "___NULL___"; }
    if(! length $custom)      { $custom      = "___NULL___"; }
    if(! length $linkids)     { $linkids     = "___NULL___"; }

    my $batch_create = 1;

    if(length $template) { $use_template = 1; }
    if(length $schema)   { $use_schema = 1;   }

    if(! length $update_id) { insert_new_note($batch_create, $from_file); }
    else { batch_update_existing_record($update_id, $from_file); }

    $use_template = 0;
    $use_schema = 0;
    $template = undef;
    $schema = undef;
}

sub get_keypairs_input
{
    my ($kp_string) = @_;
    if(defined $kp_string && length $kp_string)
    {
        print colored("KeyPairs detected from schema: $kp_string\n", 'bright_yellow', 'bold'), "\n";
        print colored("Add additional KeyPairs? (y/n)\n> ", 'bright_yellow');
    }
    else { print colored("Add KeyPairs? (y/n)\n> ", 'bright_yellow'); }

    my $answer = <STDIN>;
    chomp($answer);

    my $keypairs = $kp_string;
    my $input_key = "";
    my $input_val = "";
    if($answer eq "y")
    {
        print colored ("[ An empty 'Key' or 'Value' input will cancel/exit input loop ]", 'bright_green'), "\n";
        while(1)
        {
            print "Key        : ";
            $input_key = lc <STDIN>;
            chomp($input_key);
            if($input_key eq "") { last; }
            $input_key  =~ s/^\s+|\s+$//g ;
            print "Value      : ";
            $input_val = lc <STDIN>;
            chomp($input_val);
            if($input_val eq "") { last; }
            # important: remove any extraneous whitespace
            $input_key  =~ s/^\s+|\s+$//g ;
            $input_val  =~ s/^\s+|\s+$//g ;
            $keypairs .= $input_key . ":" . $input_val . "#";
            print colored("[ Key/value pair ( $input_key:$input_val ) registered ]", 'bright_green'), "\n";
        }

        print colored ("[ KeyPair input complete ]", 'bright_yellow'), "\n";
    }
    return $keypairs;
}

sub extract_schema_val
{
    my ($line) = @_;
    my $sch_val = (split "\\|", $line, 2)[1];
    if(! length $sch_val) { return ""; }
    else
    {
        chomp($sch_val);
        $sch_val =~ s/^\s+|\s+$//g;
        return $sch_val;
    }
}

sub parse_schema_file
{
    my $schema_def = open_file_ro("${ScribbleWibble::wibble_store}/schemas/$schema");
    my @schema_opt = @{$schema_def};
    my $title = ""; my $desc = ""; my $ft = ""; my $proj = ""; my $tags = ""; my $class = ""; my $kp = ""; my $cust = "";
    for my $line(@schema_opt)
    {
        given($line)
        {
            when (/^#/) { next; }
            when (/^TITLE(.*)+\|/)    { $title = extract_schema_val($line);  }
            when (/^DESC(.*)+\|/)     { $desc  = extract_schema_val($line);  }
            when (/^FILETYPE(.*)+\|/) { $ft    = extract_schema_val($line);  }
            when (/^PROJECT(.*)+\|/)  { $proj  = extract_schema_val($line);  }
            when (/^TAGS(.*)+\|/)     { $tags  = extract_schema_val($line);  }
            when (/^CLASS(.*)+\|/)    { $class = extract_schema_val($line)   }
            when (/^KEYPAIRS(.*)+\|/) { $kp    = extract_schema_val($line);  }
            when (/^CUSTOM(.*)+\|/)   { $cust  = extract_schema_val($line);  }
        }
    }

    my %schema_skel =
    (
        'title' => $title,
        'desc'  => $desc,
        'ft'    => $ft,
        'proj'  => $proj,
        'tags'  => $tags,
        'class' => $class,
        'kp'    => $kp,
        'cust'  => $cust,
    );

    return \%schema_skel;
}


sub use_blank_schema
{
    my %schema_def =
    (
        "title"   => "",
        "desc"    => "",
        "ft"      => "",
        "proj"    => "",
        "tags"    => "",
        "class"   => "",
        "kp"      => "",
        "cust"    => ""
    );

    return \%schema_def;
}

sub create_note_input_interactive
{
    my %schema_def = ();
    if($use_schema)
    {
        my $def_sch = parse_schema_file($schema);
        %schema_def = %{$def_sch};
        print colored("✓ Using defined schema file: '$schema'.", 'bright_green', 'bold'), "\n";
    }
    else
    {
        my $sch = use_blank_schema;
        %schema_def = %{$sch};
    }

    my $attempts = 0;
    #system("clear");

    if(defined $from_file && length $from_file)
    {
        print colored("✓ Note will be created with contents from existing file: '$from_file'.",
                      'bright_green', 'bold'), "\n";
    }

    my $term = Term::ReadLine->new('Edit metadata', \*STDIN, \*STDOUT);
    print colored("\n====================== [ INPUT ] =========================\n", 'bright_yellow', 'bold');
    while(! length $title || $title =~ /^\s*$/)
    {
        my $title_prompt = colored("Title     *: ", 'bright_yellow', 'bold');
        $title = $term->readline($title_prompt, $schema_def{'title'});
        ++$attempts;
        if($attempts >= INPUT_ATTEMPTS)
        {
            print colored("\n❌ Title is required. Maximum attempts reached. Aborting.\n", 'bright_red', 'bold');
            exit 1;
        }
    }

    $attempts = 0;

    if(! length $template)
    {
        while(! length $filetype || $filetype =~ /^\s*$/)
        {
            my $ft_prompt = colored("Filetype  *: ", 'bright_yellow', 'bold');
            $filetype = $term->readline($ft_prompt, $schema_def{'ft'});
            ++$attempts;

            if($attempts >= INPUT_ATTEMPTS)
            {
                print colored("\n❌ Filetype is required. Maximum attempts reached. Aborting.\n", 'bright_red', 'bold');
                exit 1;
            }
        }
    }
    else { $filetype = $ft; }

    my $desc_prompt = colored("Description: ", 'bright_yellow');
    $description = $term->readline($desc_prompt, $schema_def{'desc'});
    my $proj_prompt = colored("Project    : ", 'bright_yellow');
    $project = $term->readline($proj_prompt, $schema_def{'proj'});
    my $tags_prompt = colored("Tags       : ", 'bright_yellow');
    $tags = $term->readline($tags_prompt, $schema_def{'tags'});
    my $class_prompt = colored("Class      : ", 'bright_yellow');
    $class = $term->readline($class_prompt, $schema_def{'class'});
    $keypairs = get_keypairs_input($schema_def{'kp'});
    my $cust_prompt = colored("Custom     :  ", 'bright_yellow');
    $custom = $term->readline($cust_prompt, $schema_def{'cust'});

    # remove leading/trailing extraneous whitespace/tabs etc
    $title       =~ s/^\s+|\s+$//g ;
    $filetype    =~ s/^\s+|\s+$//g ;
    $description =~ s/^\s+|\s+$//g ;
    $project     =~ s/^\s+|\s+$//g ; $project =~ tr/ /_/;
    $tags        =~ s/^\s+|\s+$//g ;
    $class       =~ s/^\s+|\s+$//g ;
    $datadirs    =~ s/^\s+|\s+$//g ;
    $keypairs    =~ s/^\s+|\s+$//g ;
    $custom      =~ s/^\s+|\s+$//g ;
    $linkids     =~ s/^\s+|\s+$//g ;

    if(! length $description) { $description = "___NULL___"; }
    if(! length $project)     { $project     = "General"; }
    if(! length $tags)        { $tags        = "___NULL___"; }
    if(! length $class)       { $class       = "___NULL___"; }
    if(! length $datadirs)    { $datadirs    = "___NULL___"; }
    if(! length $keypairs)    { $keypairs    = "___NULL___"; }
    if(! length $custom)      { $custom      = "___NULL___"; }
    if(! length $linkids)     { $linkids     = "___NULL___"; }

    system("clear");

    print colored("\n====================== [ CREATE ] =========================", 'bright_yellow', 'bold');
    print colored("\nTitle      :", 'bright_yellow', 'bold'); print " $title";
    print colored("\nFiletype   :", 'bright_yellow', 'bold'); print " $filetype";
    print colored("\nDescription:", 'bright_yellow', 'bold'); print " $description";
    print colored("\nProject    :", 'bright_yellow', 'bold'); print " $project";
    print colored("\nTags       :", 'bright_yellow', 'bold'); print " $tags";
    print colored("\nClass      :", 'bright_yellow', 'bold'); print " $class";
    print colored("\nDataDirs   :", 'bright_yellow', 'bold'); print " $datadirs";
    print colored("\nKeyPairs   :", 'bright_yellow', 'bold'); print " $keypairs";
    print colored("\nCustom     :", 'bright_yellow', 'bold'); print " $custom";
    print colored("\nLinkedIds  :", 'bright_yellow', 'bold'); print " $linkids";
    print colored("\n===========================================================\n", 'bright_yellow', 'bold');

    if(defined $from_file && length $from_file)
    {
        print colored("\n✓ FILE CONTENTS from FILE: '$from_file'.",
                      'bright_yellow', 'bold'), "\n";
    }


    $attempts = 0;
    print "\n";
    do
    {
        print colored("Create new note?", 'bright_yellow', 'bold'); print " (y/n) > ";
        $create_note = <STDIN>;
        chomp ($create_note);
        ++$attempts;
    }
    while (! length $create_note && $attempts < (INPUT_ATTEMPTS + 2));

    if($create_note eq "y")
    {
        db_check_init;
        insert_new_note(0, $from_file);
    }
    else
    {
        print colored("❌ Note creation cancelled.\n", 'bright_red');
    }
}

sub description
{
    my ($self, $opt, $args) = @_;
    $self->app->read_config_file;
    my $desc = "Create a new note with optional schema and/or optional template.\n\n"
             . "Specify --from-file to use existing file as content rather than create an\n"
             . "empty note.\n\n"
             . "If no schema or template is specified, start from blank.\n\n"
             . "Usage: wibble create [--schema|-S <schema>] [--template|-T <template>]\n"
             . "                     [--from-file|-F <valid path to file> ]\n"
             . "                     [--list]\n\n"
             . "Where schema is of form <schema_path/file>\n"
             . "e.g. wibble create --schema work/meeting\n"
             . "(if a file 'meeting' exists under 'work/')\n\n"
             . "Where template is of form <type>:<template>\n"
             . "e.g. wibble create --template org:todo_list\n"
             . "(if a file 'todo_list' exists under 'org/')\n\n"
             . "Or both together, in a more complex definition:\n"
             . "e.g. wibble create --schema cs/notes/snippets --template md:code/code_notes\n"
             . "(here schema 'snippets' exists under 'cs/notes/' and a markdown template\n"
             . "'code_notes' exists under 'code/')"
             . "\n\nSchema/template definitions are always relative to the schema/template directory.\n"
             . "\n------------------------------\n\n"
             . "Schema location  : $ScribbleWibble::wibble_store/schemas\n"
             . "Template location: $ScribbleWibble::wibble_store/templates\n"
             . "\n------------------------------\n\n"
             . "Meanwhile, --from-file argument must point to a valid existing file:\n"
             . "e.g. wibble create --from-file \"~/Documents/somefile.txt\"\n\n"
             . "Note that using both --template and --from-file makes no sense, as --from-file\n"
             . "would simply overwrite/obliterate the template content in the new destination note.\n\n"
             . "Use the --list function to conveniently display all existing schema\n"
             . "and template files stored under schema/ and templates/ respectively.\n";
    return $desc;
}

sub validate_template_string
{
    my ($template_string) = @_;
    if(length $template_string)
    {
        ($ft, $template) = split /:/, $template_string;
        if(! length $ft || ! length $template)
        {
            print colored("Invalid template supplied. Specify as <filetype>:<template>\n.", 'bright_red', 'bold');
            print colored("e.g. org:todo-list, or txt:meeting_minutes\n", 'bright_red', 'bold');
            exit 0;
        }
        else
        {
            $use_template = 1;
            print colored("✓ Using defined template file: '$template'.", 'bright_green', 'bold'), "\n";
        }
    }
}

sub check_args
{
    my ($input) = @_;
    my @arr = @{$input};
    my $ff_ebld = 0;
    my $ff_set = undef;
    my $sch_ebld = 0;
    my $sch_set = undef;
    my $tem_ebld = 0;
    my $tem_set = undef;
    my $block_arg = 0;

    while(scalar @arr > 0)
    {
        my $arg = shift @arr;
        if($arg eq "--list")
        {
            print colored("✓ Listing all defined schemas.", 'bright_green', 'bold'), "\n";
            system("tree $ScribbleWibble::wibble_store/schemas");
            print colored("\n✓ Listing all defined templates.", 'bright_green', 'bold'), "\n";
            system("tree $ScribbleWibble::wibble_store/templates");
            exit(0);
        }

        if(! $block_arg && $arg eq "--from-file" || $arg eq "-F")
        {
            $ff_ebld = 1;
            $block_arg = 1;
            next;
        }

        if(! $block_arg && $arg eq "--schema" || $arg eq "-S")
        {
            $sch_ebld = 1;
            $block_arg = 1;
            next;
        }

        if(! $block_arg && $arg eq "--template" || $arg eq "-T")
        {
            $tem_ebld = 1;
            $block_arg = 1;
            next;
        }

        if($ff_ebld)
        {
            $from_file = $arg;
            $block_arg = 0;
            $ff_set = 1;
            next;
        }

        if($sch_ebld)
        {
            $schema = $arg;
            $use_schema = 1;
            $block_arg = 0;
            $sch_ebld = 0;
            $sch_set = 1;
            next;
        }

        if($tem_ebld)
        {
            $template = $arg;
            $use_template = 1;
            $block_arg = 0;
            $tem_ebld = 0;
            $tem_set = 1;
            next;
        }
    }

    if($ff_set && ! defined $from_file) { print colored("❌ Error processing arguments/from file value.", 'bright_red', 'bold'), "\n";     exit (1); }
    if($sch_set && ! defined $schema)   { print colored("❌ Error processing arguments/schema definition.", 'bright_red', 'bold'), "\n";   exit (1); }
    if($tem_set && ! defined $template) { print colored("❌ Error processing arguments/template definition.", 'bright_red', 'bold'), "\n"; exit (1); }

    my $malformed_args = 0;
    if(defined $from_file)
    {
        if($from_file eq "--template" || $from_file eq "-T" || $from_file eq "--schema" || $from_file eq "-S" || $from_file eq "--from-file" || $from_file eq "-F")
        {
            $malformed_args = 1;
        }
    }
    if(defined $schema)
    {
        if($schema eq "--template" || $schema eq "-T" || $schema eq "--schema" || $schema eq "-S" || $schema eq "--from-file" || $schema eq "-F")
        {
            $malformed_args = 1;
        }
    }
    if(defined $template)
    {
        if($template eq "--schema" || $template eq "-S" || $template eq "--template" || $template eq "-T" || $template eq "--from-file" || $template eq "-F")
        {
            $malformed_args = 1;
        }
    }

    if($malformed_args)
    {
        print colored("❌ Invalid argument list.\nPlease supply "
                      . "\"[ --schema|-S <schema> ] [ --template|-T <template> ] [ --from-file|-F <valid path to file> ]\"",
                      'bright_red', 'bold'), "\n";
            print colored("e.g. '--schema work/meeting --template org:meeting_notes'", 'bright_red'), "\n";
            print colored("(Schema and template are both optional arguments, but require their corresponding value).", 'bright_red'), "\n";
            exit 1;
    }

    if(defined $template && length $template) { validate_template_string($template); }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $op_mode = set_operation_mode($verbose);

    check_args($args);
    create_note_input_interactive;
}

1;
