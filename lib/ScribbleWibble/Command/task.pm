# ABSTRACT: Simple task management / TODO list handling
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::task;
use ScribbleWibble -command;
#use base 'App::Cmd::Command';
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy );
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Path;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use Term::ReadLine;  # Do not "use Term::ReadLine::Gnu;"
use strict;
use warnings;


my $add_task = 0;
my $ls_task = 0;
my $all_task = 0;
my $mig_task = 0;
my $mkc_task = 0;
my $verbose;

sub opt_spec
{
    return (
        ["add|A" => "Add new TASK (default action)"],
        ["ls|L"  => "List outstanding TASKs"],
        ["all|T"  => "List all TASKs (included completed)"],
        ["mark|D" => "Mark outstanding task as COMPLETE"],
        ["migrate|M" => "Migrate incomplete tasks to a fresh TASK file"],
    );
}

sub setup_new_task_file
{
    print "No task file at $ScribbleWibble::task_file exists. Initialising.\n";
    my $base_path = dirname("$ScribbleWibble::task_file");
    my $create_taskdir = mkpath("$base_path");
    if($create_taskdir eq 1 && ! -e $base_path) { print "Error creating destination task directory: $base_path\n"; exit(1); }
    my $init_task_file = system("touch $ScribbleWibble::task_file");
    if($init_task_file eq 0) { print "Fresh task file at $ScribbleWibble::task_file initialised.\n" }
}

sub read_task_file
{
    if($verbose) { print "Reading task file: $ScribbleWibble::task_file...\n"; print "---\n";}
    if(! -e $ScribbleWibble::task_file) { setup_new_task_file; }
    open (my $file_handle, '<', $ScribbleWibble::task_file) or die "$!: $ScribbleWibble::task_file";
    chomp(my @file = <$file_handle>);
    close($file_handle);
    return @file;
}

sub migrate_to_new_task_file
{
    print "\nMigrate all pending tasks into a fresh file, archiving old tasks?\n(y/n) > ";
    my $response = <STDIN>;
    if(! length $response) { print "\nAborting. Please enter 'y' to proceed, anything else to cancel.\n"; exit(1); }
    chomp($response);
    if($response eq "y")
    {
        my @tasks = read_task_file;
        my @completed;
        my @pending;

        for my $line (@tasks)
        {
            if ($line =~ /\Q[*] > (/) { push @completed, $line; }
            if ($line =~ /\Q[ ] > (/) { push @pending,   $line; }
        }

        my $tasks_src = "$ScribbleWibble::task_file";
        my $date_suffix = strftime "%Y-%m-%d_%H_%M_%S", localtime;
        my $tasks_archived = "$ScribbleWibble::task_file" . "_" . $date_suffix . "_completed.txt";

        # write out all of the completed tasks to the archive file
        my $file_handle;
        open ($file_handle, '>', $tasks_archived) or die "$!: $tasks_archived";
        for my $task (@completed)
        {
            if(length $task)
            {
                print $file_handle $task . "\n";
            }
        }
        close($file_handle);

        # clobber the task file with only the pending tasks
        open ($file_handle, '>', $tasks_src) or die "$!: $tasks_src";
        for my $task (@pending)
        {
            if(length $task)
            {
                print $file_handle $task . "\n";
            }
        }
        close($file_handle);

        print "Completed tasks successfully archived into $tasks_archived.\n";
    }
    else
    {
        print "Aborting.\n";
    }
}

sub add_new_task
{
    print "Add a new task.\n\n";
    my ($task_string) = @_;
    my $term = new Term::ReadLine 'Taskentry';
    my $task_entry = $term->readline("Enter task: ", $task_string);
    $task_entry =~ s/^\[.\]/\[\*\]/;
    if(! length $task_entry)
    {
        print "No task defined. Aborting.\n";
        exit(0);
    }
    my $current_date = strftime "%Y-%m-%d", localtime;
    $task_entry = "[ ] > (" . $current_date .  ") " . $task_entry . "\n";
    open (my $file_handle, '>>', $ScribbleWibble::task_file) or die "$!: $ScribbleWibble::task_file";
    print $file_handle $task_entry;
    close($file_handle);
    print "\nNew task added.\n\n";
}

sub complete_task
{
    print "\nMark task as complete.\n\n";
    my ($pending, $completed) = @_;
    my $task_size = scalar @${pending};

    if($task_size eq 0)
    {
        print "No pending tasks to complete. Add new tasks with --add.\n";
        exit(0);
    }

    print "Enter task NUM to mark as COMPLETE > ";
    my $response = <STDIN>;
    if(! length $response) { print "\nAborting. Please enter a valid task number.\n"; exit(1); }
    chomp($response);

    if(looks_like_number($response) && $response >= 1 && $response <= $task_size)
    {
        # ensure leading zeros are stripped in case they enter "01" etc. by forcing integer conversion
        $response += 0;
        my $completed_time = strftime "%Y-%m-%d %H:%M:%S", localtime;
        print "Marking task $response as COMPLETE.\n";
        @$pending[($response - 1)] =~ s/^\[.\]/\[\*\]/;
        my $completed_task = @${pending}[($response - 1)];
        $completed_task =~ s/^\[.\]/\[\*\]/;
        $completed_task = $completed_task . " [Completed: " . $completed_time . "]";
        push @$completed, $completed_task;
        delete @$pending[($response - 1)];
    }
    else
    {
        print "Aborting. Invalid task selected.\n";
        exit(0);
    }


    # overwrite the task file, completed tasks first; new tasks at bottom
    open (my $file_handle, '>', $ScribbleWibble::task_file) or die "$!: $ScribbleWibble::task_file";
    for my $task (@$completed)
    {
        if(length $task)
        {
            print $file_handle $task . "\n";
        }
    }
    print $file_handle "\n";
    for my $task (@$pending)
    {
        if(length $task)
        {
            print $file_handle $task . "\n";
        }
    }
    close($file_handle);

    print_hl_heavy;
    print_hl;
    print "Completed tasks:\n";
    print_hl;
    output_tasks_pretty($completed, "green");
    print_hl;
    print "Remaining pending tasks:";
    print_hl; print "\n";
    output_tasks_pretty($pending, "red");

}

sub output_tasks_pretty
{
    my ($tasks, $task_colour) = @_;

    my $task_total = scalar @{$tasks};
    my $zeropad;
    if($task_total < 10 )       { $zeropad = 1; }
    elsif($task_total < 100)    { $zeropad = 2; }
    elsif($task_total < 1000)   { $zeropad = 3; }
    elsif($task_total < 10000)  { $zeropad = 4; }
    elsif($task_total < 100000) { $zeropad = 5; }
    else { print "ERROR. Too many tasks. Migrate into new file.\n"; die(0); }

    if($task_total eq 0)
    {
        print "[ EMPTY ]\n";
    }
    my $x = 0;
    for my $task (@{$tasks})
    {
        if(defined $task)
        {
            my @clean_entry = split(/>/, $task);
            shift @clean_entry;
            # Get rid of leading/trailing whitespace
            s{^\s+|\s+$}{}g foreach @clean_entry;
            my $padded_num = sprintf("%0${zeropad}d", ++$x);
            print colored ("[ $padded_num ] " . "@clean_entry", "$task_colour"), "\n";
        }
    }

}

sub display_tasks
{

    my ($task_action, $task_string) = @_;
    if($add_task eq 1) { add_new_task($task_string); }
    my $display_all = 0;
    if($ls_task eq 1)
    {
        print "Displaying only live tasks.\n";

    }
    else
    {
        print "Displaying all tasks (including complete).\n";
        $display_all = 1;
    }

    my @tasks = read_task_file;
    my @completed;
    my @pending;
    my $task_colour = "green";

    for my $line (@tasks)
    {
        #print "Complete: $line\n";
        #print "Inomplete: $line\n";
        #print "Line is: $line";
        if ($line =~ /\Q[*] > (/) { push @completed, $line; }
        if ($line =~ /\Q[ ] > (/) { push @pending,   $line; }
    }

    my $task_total = scalar @completed;
    my $zeropad;
    if($task_total < 10 )       { $zeropad = 1; }
    elsif($task_total < 100)    { $zeropad = 2; }
    elsif($task_total < 1000)   { $zeropad = 3; }
    elsif($task_total < 10000)  { $zeropad = 4; }
    elsif($task_total < 100000) { $zeropad = 5; }
    else { print "ERROR. Too many tasks. Migrate into new file.\n"; die(0); }

    if($display_all)
    {
        print_hl;
        print "Completed tasks:";
        print_hl; print "\n";
        output_tasks_pretty(\@completed, $task_colour);
    }

    print_hl;
    print "Pending tasks:";
    print_hl; print "\n";
    $task_colour = "red";
    output_tasks_pretty(\@pending, $task_colour);
    if($task_action eq 1) { complete_task(\@pending, \@completed); }
    #if($add_task)  { add_new_task(\@pending, \@completed);  }
    my $new_total = scalar @pending;
    if($task_action eq 2) { print "\nNew task added as task [ " . $new_total . " ].\n"; }
}

sub dispatch_task
{
    my ($task_string) = @_;
    if($ls_task || $all_task) { display_tasks(0, $task_string); }
    elsif ($add_task)         { display_tasks(2, $task_string);  }
    elsif ($mkc_task)         { display_tasks(1, $task_string);}
    elsif ($mig_task)         { migrate_to_new_task_file; }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $enabled_modes = 0;

    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;

    if($opt->{ls})
    {
        #print "Listing pending tasks.\n\n";
        $ls_task = 1;
        ++$enabled_modes;
    }

    if($opt->{add})
    {
        $add_task = 1;
        ++$enabled_modes;
    }

    if($opt->{all})
    {
        $all_task = 1;
        ++$enabled_modes;
    }

    if($opt->{mark})
    {
        $mkc_task = 1;
        ++$enabled_modes;
    }

    if($opt->{migrate})
    {
        $mig_task = 1;
        ++$enabled_modes;
    }

    if($enabled_modes > 1 || $verbose)
    {
        print colored ("Using task file: $ScribbleWibble::task_file", 'bright_green'), "\n";
        if($enabled_modes gt 1)
        {
            print "Please choose only one option out of [ --add | --ls | --all | --migrate ]\n";
            print "❌ Error: Multiple conflicting options.\n";
            exit(0);
        }

        if($ls_task eq 1)   { print colored ("--ls     : ENABLED", 'bright_green'), "\n"; } else { print colored ("--ls     : DISABLED", 'bright_red'), "\n"; }
        if($add_task eq 1)  { print colored ("--add    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--add    : DISABLED", 'bright_red'), "\n"; }
        if($all_task eq 1)  { print colored ("--all    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--all    : DISABLED", 'bright_red'), "\n"; }
        if($mig_task eq 1)  { print colored ("--migrate: ENABLED", 'bright_green'), "\n"; } else { print colored ("--migrate: DISABLED", 'bright_red'), "\n"; }
    }

    if($enabled_modes == 0) { print "No action specified, defaulting to --ls.\n\n"; $ls_task = 1; }

    my $task_string = "@{$args}";
    dispatch_task($task_string);
}

1;
