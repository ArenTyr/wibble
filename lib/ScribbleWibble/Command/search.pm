# ABSTRACT: Search for an existing note
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)

package ScribbleWibble::Command::search;
use ScribbleWibble -command;
use ScribbleWibble::common_funcs qw ( parameter_check make_tmp_db_backup update_record_metadata ) ;
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy padded_number );
use ScribbleWibble::rg_search qw ( rg_search_by_field rg_multiple_field_search
                                   rg_get_field_from_id_match rg_update_record_field
                                   rg_delete_entry rg_update_entire_record );

use Data::Dumper;
use Exporter 'import';
use File::Path;
use JSON::XS;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use Term::ReadLine;  # Do not "use Term::ReadLine::Gnu;"
use strict;
use warnings;

use constant INPUT_ATTEMPTS => 3;

our $config_override = 0;
our @EXPORT_OK = qw ( check_verbose check_config_override set_config_file
                      extern_search_by_field
                      extern_multiple_field_search
                      extern_update_data_dir_metadata
                      return_all_notes );

my @data_dirs;

my $add_data = 0;
my $add_link = 0;
my $archive_search = 0;
my $add_assoc = 0;
my $associate = 0;
my $create_aa = 0;
my $display_full_record = 0;
my $dump_both = 0;
my $dump_contents = 0;
my $dump_json = 0;
my $edit_linked = 0;
my $edit_metadata = 0;
my $everything = 0;
my $export_result = 0;
my $exact_match = 0;
my $expert_search = 0;
my $ls_dd = 0;
my $ls_linked = 0;
my $move_to_archive = 0;
my $note_to_associate = "";
my $note_linked_ids = "";
my $only_single = 0;
my $op_mode = undef;
my $open_aa = 0;
my $open_dd = 0;
my $quick_edit = 1;
my $related_notes = 0;
my $restore_archive = 0;
my $search_field = "";
my $search_string = "";
my $show_dd_path = 0;
my $use_bare = 0;
my $use_id_only = 0;
my $use_gui = 0;
my $use_menu_search = 0;
my $verbose = 0;
my $view_md = 0;

sub description
{
    return "Use this command to search the note database,\n"
    . "open and edit notes, and update note metadata.\n"
    . "Use it to perform a variety of note operations.\n\n"
    . "                       [Note Operations/Display Options]\n";
}

sub opt_spec
{
    return (
        ["add-data|@"      => "Add data/attachment to note (create DataDir) "],
        ["add-link|%"      => "Add link to an external associated file (in special DataDir file)"],
        ["advanced|Z"      => "Do an advanced search providing explicit search logic (expert)"],
        ["allfields|X"     => "Show expanded result list"],
        ["archived|K"      => "Search in archived notes instead"],
        ["associate|^"     => "Associate with related note: (parent/sibling/child) relationship"],
        ["bare|B"          => "Return bare path. For use with scripts/dmenu/rofi etc"],
        ["create-aa"       => "Create/open an automatic note-associated archive and open with archive manager"],
        ["dump-both|3"     => "Dump (cat) both metadata and contents of matches to STDOUT."],
        ["dump-contents|2" => "Dump (cat) contents of matches to STDOUT."],
        ["dump-json|J"     => "Dump note metadata + content to STDOUT in JSON format."],
        ["edit-md"         => "Edit metadata for note"],
        ["edit-linked"     => "Edit externally linked file/directory list for note"],
        ["everything"      => "Simply return ALL notes in entire database"],
        ["exact|E"         => "Exact match (no substring)"],
        ["from-archive"    => "Restore selected note from the archive"],
        ["full|F"          => "Full text search (implies substring)"],
        ["guiedit|G"       => "Open file with GUI editor instead of regular editor"],
        ["idonly"          => "Display note ID only"],
        ["ls-dd"           => "List note data/attachment directory/directories] (DataDirs)"],
        ["ls-linked"       => "List external linked files"],
        ["menu|I"          => "Interactive menu input/complex search"],
        ["only-single|1"   => "Exit if no singular match (no interactivity)"],
        ["open-aa"         => "Open/create an automatic note-associated archive and open with archive manager"],
        ["open-dd|O"       => "Open note data/attachment directory/directories] (DataDirs)"],
        ["show-dd-dir"     => "Dump path to note data/attachment directory"],
        ["related-notes|R" => "List related notes"],
        ["to-archive"      => "Move selected note to the archive"],
        ["view-md|M"       => "View/display note metadata only         \n\n  [Search] \n"],
        ["date|?"          => "Search by note date"],
        ["id|9"            => "Search by note ID"],
        ["title|T"         => "Search by note title"],
        ["description|D"   => "Search by note description"],
        ["project|P"       => "Search by note project"],
        ["tags|A"          => "Search by note tags"],
        ["filetype|Y"      => "Search by note type"],
        ["class|C"         => "Search by note class"],
        ["keypairs|S"      => "Search by keypairs"],
        ["custom"          => "Search by 'custom' field"],
        ["linkids"         => "Search by 'LinkedIds' field"],
    );
}

sub debug_break
{
    print Dumper(@_);
    exit 0;
}


sub create_auto_archive
{
    my ($note_id, $note_dd) = @_;
    #my $search_cmd_archived = "$ScribbleWibble::archived_db_location";
    #my $search_cmd_main = "$ScribbleWibble::db_location";
    #my $search_cmd = "recsel -t ScribbleWibble_Note -e 'NoteId = \"$note_id\"' ";
    #my $search_exec_main = $search_cmd . $search_cmd_main;
    #my $search_exec_archived = $search_cmd . $search_cmd_archived;

    my $notepath_prefix = substr $note_id, 0, 2;
    my $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";
    my $setup_path = mkpath("$output_path");
    if($setup_path) { print colored("❌ Error creating data directory at $output_path, aborting.", 'bright_red'), "\n"; }
    my $archive_folder = "files_${note_id}";
    my $archive_name = "${note_id}.7z";
    my $archive_cmd = "cd $output_path && mkdir $archive_folder && " .
                      "7z a $archive_name $archive_folder && " .
                      "rmdir $archive_folder && " .
                      "$ScribbleWibble::archive_manager $output_path/$archive_name";
    #print "DEBUG: $archive_cmd\n";
    my $cmd_result = 0;
    if (! -f "$output_path/$archive_name") { $cmd_result = system($archive_cmd); }
    else
    { $cmd_result = system("$ScribbleWibble::archive_manager $output_path/$archive_name"); }
    if($note_dd eq "___NULL___") { update_data_dir_metadata($note_id, $note_dd, $output_path); }
    if($cmd_result eq 0) { print colored("✓ Archive successfully created/opened.", 'bright_green'), "\n"; }
    else                 { print colored("❌ Problem creating/opening archive.", 'bright_red'), "\n";      }
    exit(0);
}

sub get_related_notes
{
    my ($note_id, $filetype) = @_;
    my $data_paths;
    my @related_notes = ();

    if($op_mode eq "RIPGREP")
    {
        $data_paths = rg_get_field_from_id_match($note_id, $archive_search, "LinkedIds");
    }
    elsif($op_mode eq "RECUTILS")
    {
    my $rec_sel_cmd = "";
    my $rec_sel_prx = "";
    my $rec_sel_pofx = "";
    my $rec_sel_op = "";

        if(! $archive_search) { $rec_sel_pofx = "$ScribbleWibble::db_location"; }
        else { $rec_sel_pofx = "$ScribbleWibble::archived_db_location"; }

        $rec_sel_prx  = "recsel -t ScribbleWibble_Note ";
        $rec_sel_prx .= "-e \"NoteId = '$note_id'\" ";
        $rec_sel_cmd = "-P LinkedIds ";

        $rec_sel_op  = $rec_sel_prx . $rec_sel_cmd . $rec_sel_pofx;
        print "A executing search: $rec_sel_op\n";
        $data_paths = `$rec_sel_op`;
    }

    @related_notes = split / /, $data_paths;
    return @related_notes;
}

sub get_linked_files
{
    my ($note_id, $dir) = @_;
    my $output_path = "";
    my $notepath_prefix = "";
    my $linked_files_fn = "/.linked_files.wib";
    my $linked_files = "";
    my @file_results = ();

    if((substr $dir, 0, 1) eq "'")
    {
        $output_path = substr $dir, 1, -1;
        $linked_files = $output_path . $linked_files_fn;
    }
    else
    {
        $notepath_prefix = substr $note_id, 0, 2;
        $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";
        $linked_files = $output_path . $linked_files_fn;
    }

    if (-e $linked_files)
    {
        open (my $file_handle, '<', $linked_files) or die "$!: $linked_files";
        chomp(my @file = <$file_handle>);
        close($file_handle);

        for my $line (@file)
        {
            $line =~ s/~/$ENV{HOME}/g;
            push @file_results, $line;
        }
    }

    return @file_results;
}

sub get_data_dirs
{
    my ($note_id, $filetype) = @_;

    my $data_paths = "";

    if($op_mode eq "RIPGREP")
    {
        $data_paths = rg_get_field_from_id_match($note_id, $archive_search, "DataDirs");
    }
    elsif($op_mode eq "RECUTILS")
    {
        my $rec_sel_cmd = "";
        my $rec_sel_prx = "";
        my $rec_sel_pofx = "";
        my $rec_sel_op = "";

        if(! $archive_search) { $rec_sel_pofx = "$ScribbleWibble::db_location"; }
        else { $rec_sel_pofx = "$ScribbleWibble::archived_db_location"; }

        $rec_sel_prx  = "recsel -t ScribbleWibble_Note ";
        $rec_sel_prx .= "-e \"NoteId = '$note_id'\" ";
        $rec_sel_cmd = "-P DataDirs ";

        $rec_sel_op  = $rec_sel_prx . $rec_sel_cmd . $rec_sel_pofx;
        $data_paths = `$rec_sel_op`;
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                    'bright_red'), "\n";
        exit(1);
    }
    @data_dirs = split / /, $data_paths;
}

sub data_dir_operation
{
    my ($data_dirs, $note_id, $action) = @_;
    my $op = "";
    my $output_path = "";
    my $notepath_prefix = "";

    if($action eq "fm" ) { $op = "$ScribbleWibble::file_manager"; }
    if($action eq "list" ) { $op = "ls -Alh --color"; }

    for my $entry(@data_dirs)
    {
        chomp $entry;
        if((substr $entry, 0, 1) eq "'") { $output_path = substr $entry, 1, -1; }
        else
        {
            $notepath_prefix = substr $note_id, 0, 2;
            $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";
        }

        $output_path =~ s/~/$ENV{HOME}/g;
        print "Data directory: $output_path\n";
        if($action eq "ls") { print "---\n"; }
        if(-e $output_path) { system("$op '$output_path' &"); }
        else
        {
            print colored ("Data directory '$output_path' does not (yet) exist.\n", 'bright_red');
            print colored ("Add some data (--add-data) to the note to automatically create it.\n", 'bright_green');
        }
        if($action eq "ls") { print "===\n"; }
    }

}

sub open_data_dir
{
    my ($note_id, $filetype) = @_;
    my $output_path = "";
    my $notepath_prefix = "";
    get_data_dirs($note_id, $filetype);
    data_dir_operation(\@data_dirs, $note_id, "fm");
}

sub ls_data_dir
{
    my ($note_id, $filetype) = @_;
    my $output_path = "";
    get_data_dirs($note_id, $filetype);
    data_dir_operation(\@data_dirs, $note_id, "list");
}

sub ls_linked_files
{
    my ($note_id, $filetype) = @_;
    get_data_dirs($note_id, $filetype);
    my @file_listing = ();

    for my $entry (@data_dirs)
    {
        chomp $entry;
        push @file_listing, get_linked_files($note_id, $entry);
    }

    my $x = 0;
    my $total_files = scalar @file_listing;
    if($total_files == 0)
    {
        print colored("Note '$note_id' has no linked files. Add a linked file to the note with --add-link first.", 'bright_red'), "\n";
        exit(0);
    }
    my $padded_x;
    print "-------------\n";
    for my $file (@file_listing)
    {

        $padded_x = padded_number($total_files, $x);
        print colored("[$padded_x]", 'bright_yellow'); print " $file\n";
        ++$x;
    }
    #my $list_size = @file_listing;
    print "-------------\n";
    print "Enter '*' to open all files\n";
    print "\nSelect file > ";
    my $open_file = <STDIN>;
    chomp $open_file;
    if(looks_like_number($open_file) && $open_file >= 0 && $open_file < $total_files)
    {
        system("xdg-open $file_listing[$open_file]");
    }
    elsif($open_file eq '*')
    {
        #my $file_list = join(" ", @file_listing);
        #system("xdg-open $file_list");
        for my $file (@file_listing)
        {
            system("xdg-open $file");
        }
    }
}

sub extern_update_data_dir_metadata
{
    my ($note_id, $data_dir, $output_path, $extern_op_mode) = @_;
    $op_mode = $extern_op_mode;
    return update_data_dir_metadata($note_id, $data_dir, $output_path);
}

sub update_data_dir_metadata
{
    my ($note_id, $data_dir, $output_path) = @_;
    my $update_result = 0;
    if($op_mode eq "RIPGREP")
    {
        my $datadirs = rg_get_field_from_id_match($note_id, $archive_search, "DataDirs");

        if($datadirs eq "___NULL___")
        {
            $update_result = rg_update_record_field($note_id, $archive_search, "DataDirs", $note_id);
        }
        else
        {
            $update_result = rg_update_record_field($note_id, $archive_search, "DataDirs", "$data_dir");
        }
    }
    elsif($op_mode eq "RECUTILS")
    {
        my $rec_set_prx  = "recset -t ScribbleWibble_Note ";
        $rec_set_prx .= "-e \"NoteId = '$note_id'\" ";
        my $rec_set_pofx = "";

        if($archive_search) { $rec_set_pofx = "$ScribbleWibble::archived_db_location --verbose"; }
        else                { $rec_set_pofx = "$ScribbleWibble::db_location --verbose"; }

        my $rec_set_cmd = "";
        if($data_dir eq "___NULL___") { $rec_set_cmd = "-f DataDirs -s '$note_id' "; }
        else                          { $rec_set_cmd = "-f DataDirs -s \"$data_dir\" "; }

        my $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        my $rec_set_update = system("$rec_set_op");

        if($rec_set_update == 0) { $update_result = 1; }
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                      'bright_red'), "\n";
        exit(1);
    }

    if ($update_result)
    {
        print colored("✓ Metadata successfully updated.\n", 'bright_green', 'bold');
    }
    else
    {
        print colored("❌ Potential issue updating metadata. Please check directory $output_path manually.\n", 'bright_red', 'bold');
    }
}

sub add_linked_file_to_note
{
    my ($note_id, $filetype, $data_dir) = @_;
    my $term = new Term::ReadLine 'Filepath';
    my $input_file = $term->readline("Specify file or directory to link: ");
    $input_file =~ s/^\s+|\s+$//g ;
    $input_file =~ s/~/$ENV{HOME}/g;
    if(! -e "$input_file") { print "❌ Unknown/unreadable file/directory '$input_file'. Aborting.\n"; exit 1; }
    my $notepath_prefix = substr $note_id, 0, 2;
    my $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";
    mkpath("$output_path");
    my $linked_files_fn = "$output_path/.linked_files.wib";
    unless (-e $linked_files_fn)
    {
        open(my $file_handle, '>', $linked_files_fn) or die "$!: Could not create file\n";
        close $file_handle;
    }
    open (my $file_handle, '+<', $linked_files_fn) or die "$!: $linked_files_fn";

    my @file = <$file_handle>;
    chomp(@file);
    my @matches = grep(/\Q$input_file/, @file);
    if(scalar @matches gt 0)
    {
        print "File already linked.\n";
    }
    else
    {
        print "$input_file\n";
        print $file_handle "$input_file\n";
        print "✓ Linked file added to note metadata file.\n";
    }
    close($file_handle);

    # Add the NoteId DataDir setting if not already there, since linked files
    # are in effect some "data" associated with the note
    update_data_dir_metadata($note_id, $data_dir, $output_path);
}

sub add_note_association
{
    my ($note_id, $linkids) = @_;
    display_note_from_recsel_result($note_id);
    my $update_links = "";

    print "Specify relationship that original note [SOURCE] has to this note:\n";
    print "(p) = is [p]arent of\n";
    print "(s) = is [s]ibling of\n";
    print "(c) = is [c]hild of\n";
    print "Any other key to abort.\n";
    print "> ";
    my $answer = <STDIN>;
    chomp($answer);

    print "linkids: '$linkids'\n"; #exit(0);
    if($linkids eq "___NULL___")         { $linkids = "";         }
    if($note_linked_ids eq "___NULL___") { $note_linked_ids = ""; }


    if($op_mode eq "RIPGREP")
    {
        # Question asks relationship from POV of the [SOURCE] note
        if($answer eq "p" || $answer eq "c")
        {
            # target child/destination note
            if($answer eq "p") { $update_links = "$linkids" . " C¬$note_to_associate"; }
            else               { $update_links = "$linkids" . " P¬$note_to_associate"; }

            rg_update_record_field($note_id, $archive_search, "LinkedIds", $update_links);

            if($answer eq "p") { print colored("✓ Child note metadata updated.", 'bright_green'), "\n";  }
            else               { print colored("✓ Parent note metadata updated.", 'bright_green'), "\n"; }

            # target parent/source note
            if($answer eq "p") { $update_links = "$note_linked_ids" . " P¬$note_id"; }
            else               { $update_links = "$note_linked_ids" . " C¬$note_id"; }

            rg_update_record_field($note_to_associate, $archive_search, "LinkedIds", $update_links);

            if($answer eq "p") { print colored("✓ Parent note metadata updated.", 'bright_green'), "\n"; }
            else               { print colored("✓ Child note metadata updated.", 'bright_green'), "\n";  }

        }
        elsif($answer eq "s")
        {
            $update_links = "$linkids" . " S¬$note_to_associate";
            rg_update_record_field($note_id, $archive_search, "LinkedIds", $update_links);
            $update_links = "$note_linked_ids" . " S¬$note_id";
            rg_update_record_field($note_to_associate, $archive_search, "LinkedIds", $update_links);
            print colored("✓ Sibling note metadata updated.", 'bright_green'), "\n";
        }

        print colored("✓ Association complete.", 'bright_green'), "\n";
    }
    elsif($op_mode eq "RECUTILS")
    {
        my $rec_set_prx  = "recset -t ScribbleWibble_Note ";
        my $rec_set_pofx = "";
        my $rec_set_cmd = "";
        my $rec_set_op = "";
        my $rec_set_update = "";
        if($archive_search) { $rec_set_pofx = "$ScribbleWibble::archived_db_location --verbose"; }
        else                { $rec_set_pofx = "$ScribbleWibble::db_location --verbose"; }

        # Question asks relationship from POV of the [SOURCE] note
        if($answer eq "p" || $answer eq "c")
        {
            # if "p", this note = child, else it is the parent
            $rec_set_prx .= "-e \"NoteId = '$note_id'\" ";
            if($answer eq "p") { $update_links = "$linkids " . "C¬$note_to_associate"; }
            else               { $update_links = "$linkids " . "P¬$note_to_associate"; }

            $rec_set_cmd = "-f LinkedIds -s '$update_links' ";
            $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
            print "DEBUG: $rec_set_op\n";
            $rec_set_update = system("$rec_set_op");

            if ($rec_set_update eq 0)
            {
                if($answer eq "p") { print "✓ Child note metadata updated.\n"; }
                else               { print "✓ Parent note metadata updated.\n"; }
            }
            else
            {
                print "❌ Potential issue updating metadata. Aborting association.\n";
                exit 1;
            }

            # reset string
            $rec_set_prx  = "recset -t ScribbleWibble_Note ";
            # source note = parent
            $rec_set_prx .= "-e \"NoteId = '$note_to_associate'\" ";
            if($answer eq "p") { $update_links = "$note_linked_ids " . "P¬$note_id"; }
            else               { $update_links = "$note_linked_ids " . "C¬$note_id"; }

            $rec_set_cmd = "-f LinkedIds -s '$update_links' ";
            $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
            $rec_set_update = system ("$rec_set_op");

            if ($rec_set_update eq 0)
            {
                if($answer eq "p") { print "✓ Parent note metadata updated.\n"; }
                else               { print "✓ Child note metadata updated.\n";  }
                print "✓ Association complete.\n";
            }
            else
            {
                print "❌ Potential issue updating metadata for parent. Check values.\n";
                exit 1;
            }
        }
        elsif($answer eq "s")
        {
            # both siblings...
            $rec_set_prx  = "recset -t ScribbleWibble_Note ";
            $rec_set_prx .= "-e \"NoteId = '$note_id'\" ";
            $update_links = "$linkids " . "S¬$note_to_associate";
            $rec_set_cmd = "-f LinkedIds -s '$update_links' ";
            $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
            $rec_set_update = system("$rec_set_op");

            if ($rec_set_update eq 1)
            {
                print "✓ Sibling note metadata updated.\n";
            }
            else
            {
                print "❌ Potential issue updating metadata. Aborting association.\n";
                exit 1;
            }

            $rec_set_prx  = "recset -t ScribbleWibble_Note ";
            $rec_set_prx .= "-e \"NoteId = '$note_to_associate'\" ";
            $update_links = "$note_linked_ids " . "S¬$note_id";
            $rec_set_cmd = "-f LinkedIds -s '$update_links' ";
            $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
            $rec_set_update = system("$rec_set_op");

            if ($rec_set_update eq 0)
            {
                print "✓ Sibling note metadata updated.\n";
                print "✓ Association complete.\n";
            }
            else
            {
                print "❌ Potential issue updating metadata. Aborting association.\n";
                exit 1;
            }
        }
        else
        {
            print "Aborted.\n";
            exit 0;
        }
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                      'bright_red'), "\n";
        exit(1);
    }

    exit 0;
}

sub add_data_to_note
{
    my ($note_id, $filetype, $editdirs) = @_;
    my $term = new Term::ReadLine 'Filepath';
    my $input_file = $term->readline("Specify file or directory to attach: ");
    $input_file =~ s/^\s+|\s+$//g ;
    $input_file =~ s/~/$ENV{HOME}/g;

    if(! -e "$input_file") { print "❌ Unknown/unreadable file/directory $input_file. Aborting.\n"; exit 1; }

    my $file_operation = $term->readline("Specify (m) to [m]ove or (c) to [c]opy $input_file: ");
    $file_operation =~ s/^\s+|\s+$//g ;

    if(! $file_operation eq "m" && ! $file_operation eq "c")
    { print "❌ Invalid option. Expecting 'm' or 'c'.\n"; exit 1; }

    my $notepath_prefix = substr $note_id, 0, 2;
    my $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";

    #FIXME: Replace with File::Copy::Recursive commands
    if($file_operation eq "c")
    {
        print colored("COPYING '$input_file' to $output_path.\nProceed? (y/n)> ", 'bright_green');
        my $proceed = <STDIN>;
        chomp $proceed;
        if($proceed eq "y")
        {
            mkpath("$output_path");
            my $copy_result = system("cp -anv $input_file $output_path");
            if($copy_result eq 0) { print "✓ File attachment successfully copied.\n"; }
            else { print colored("❌ Issue adding attachment. Copy operation failed.\n", 'bright_red'); }

        }
    }

    if($file_operation eq "m")
    {
        print colored("MOVING '$input_file' to $output_path.\nProceed? (y/n)> ", 'bright_red', 'bold');
        my $proceed = <STDIN>;
        chomp $proceed;
        if($proceed eq "y")
        {
            mkpath("$output_path");
            my $mv_result = system("mv -nv $input_file $output_path");
            if($mv_result eq 0) { print "✓ File attachment successfully moved.\n"; }
            else { print colored("❌ Issue adding attachment. Copy operation failed.\n", 'bright_red'); }

        }
    }

    update_data_dir_metadata($note_id, $editdirs, $output_path);
}

sub edit_linked_spec_file
{
    my ($note_id) = @_;
    my $notepath_prefix = substr $note_id, 0, 2;
    my $output_path = "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id";

    if(! -f "$output_path/.linked_files.wib")
    {
        print colored("Note '$note_id' has no linked files. Add a linked file with --add-link first.", 'bright_red'), "\n";
        exit 0;
    }
    if($use_gui)  { system("$ScribbleWibble::gui_editor $output_path/.linked_files.wib &"); }
    else          { system("$ScribbleWibble::editor $output_path/.linked_files.wib"); }
    exit 0;
}

sub display_results
{
    my ($results) = @_;
    my @dates      = ( $results =~ /^Date:\s*(.*)$/gm );
    my @note_title = ( $results =~ /^Title:\s*(.*)$/gm );
    my @note_id    = ( $results =~ /^NoteId:\s*(.*)$/gm );
    my @note_type  = ( $results =~ /^Filetype:\s*(.*)$/gm );
    my @note_desc  = ( $results =~ /^Description:\s*(.*)$/gm );
    my @note_proj  = ( $results =~ /^Project:\s*(.*)$/gm );
    my @note_tags  = ( $results =~ /^Tags:\s*(.*)$/gm );
    my @note_class = ( $results =~ /^Class:\s*(.*)$/gm );
    my @note_dirs  = ( $results =~ /^DataDirs:\s*(.*)$/gm );
    my @key_pairs  = ( $results =~ /^KeyPairs:\s*(.*)$/gm );
    my @note_cust  = ( $results =~ /^Custom:\s*(.*)$/gm );
    my @note_links = ( $results =~ /^LinkedIds:\s*(.*)$/gm );
    #my %note_hash;
    #@note_hash{@note_title} = @note_ids;

    my $x = 0;
    my $y = @note_id;

    if($dump_json)
    {
        # Use function with fully qualifed name to avoid namespace clash/dependency cycle
        print encode_json (ScribbleWibble::lib_wrapper::serialize_wibble_records($archive_search, $results, 0));
        exit 0;
    }

    # Exit condition: no matches
    if($y eq 0)
    {
        print "No matches. Exiting.\n";
        exit 0;
    }

    # Exit condition: more than one match, only-single logic specified
    if($y gt 1 && $only_single eq 1)
    {
        if($verbose) { print "❌ Multiple matches but 'only-single' predicate specified. Exiting.\n"; }
        exit 1;
    }

    # At least one match, so default to first match
    my $editfile  = $note_id[0];
    my $edittype  = $note_type[0];
    my $editdate  = $dates[0];
    my $edittitle = $note_title[0];
    my $editdesc  = $note_desc[0];
    my $editproj  = $note_proj[0];
    my $edittags  = $note_tags[0];
    my $editclass = $note_class[0];
    my $editdirs  = $note_dirs[0];
    my $editpairs = $key_pairs[0];
    my $editcust  = $note_cust[0];
    my $linkids   = $note_links[0];

    chomp $edittype;

    # singular record matches...
    if   ($y eq 1 && $quick_edit)     { open_with_editor($editfile, $edittype, $editproj); }
    elsif($y eq 1 && $add_assoc)
    {
        add_note_association($editfile, $linkids);
    }
    elsif($y eq 1 && $add_data)       { add_data_to_note($editfile, $edittype, $editdirs); }
    elsif($y eq 1 && $add_link)
    {   add_linked_file_to_note($editfile, $edittype, $editdirs);                          }
    elsif($y eq 1 && $associate)      { associate_note($editfile, $linkids);               }
    elsif($y eq 1 && $create_aa)      { create_auto_archive($editfile, $editdirs);         }
    elsif($y eq 1 && $edit_linked)    { edit_linked_spec_file($editfile);                  }
    elsif($y eq 1 && $edit_metadata)
    {
        edit_note_metadata($editfile, $edittype, $editdate, $edittitle,
            $editdesc, $editproj, $edittags, $editclass,
            $editdirs, $editpairs, $editcust, $linkids);
    }
    elsif($y eq 1 && $ls_dd)          { ls_data_dir($editfile, $edittype);                 }
    elsif($y eq 1 && $ls_linked)      { ls_linked_files($editfile, $edittype);             }
    elsif(($y eq 1 && $move_to_archive) || ($y eq 1 && $restore_archive))
    {
        move_to_archive($editfile, $edittype, $editdate, $edittitle,
            $editdesc, $editproj, $edittags, $editclass,
            $editdirs, $editpairs, $editcust, $linkids);
    }
    elsif($y eq 1 && $open_dd)        { open_data_dir($editfile, $edittype);               }
    elsif($y eq 1 && $related_notes)  { ls_related_notes($editfile, $edittype);            }
    # if here, matches > 1
    else
    {
        # display list, prompt for user input
        if ($add_data)       { print "Select "; print colored("note", 'bright_yellow'); print " to add data to:\n";              }
        elsif($associate)    { print "Select "; print colored("note", 'bright_yellow'); print " to associate:\n";                }
        elsif($create_aa)    { print "Select "; print colored("note", 'bright_yellow'); print " to add automatic archive to:\n"; }
        elsif($dump_contents || $dump_both) { }
        elsif($open_dd)      { print "Select "; print colored("note", 'bright_yellow'); print " to open data directory:\n";      }
        elsif($use_bare)     { print "Select "; print colored("note", 'bright_yellow'); print " to display path:\n";             }
        elsif($use_id_only)  { print "Select "; print colored("note", 'bright_yellow'); print " to display ID:\n";             }
        else
        {
            if (! $view_md && ! $move_to_archive) { print "Select "; print colored("note", 'bright_yellow'); print " to open/edit:\n";            }
            if (! $view_md &&   $move_to_archive) { print "Select "; print colored("note", 'bright_yellow'); print " to move to archive:\n";      }
            if (! $view_md &&   $restore_archive) { print "Select "; print colored("note", 'bright_yellow'); print " to restore from archive:\n"; }
        }

        if(! $display_full_record)
        {
            my $data_ind = "";
            my $total_matches = scalar @note_title;
            my $padded_x;
            for my $line(@note_title)
            {
                $padded_x = padded_number($total_matches, $x + 1);
                if($note_dirs[$x] eq "___NULL___") { $data_ind = ""; }
                else { $data_ind = " [<DATA>]"; }
                if($note_tags[$x] eq "___NULL___")
                { print colored ("[$padded_x]", 'bright_yellow', 'bold'); print " $line "; print colored("$data_ind\n", 'bright_green');                                                             }
                else
                { print colored ("[$padded_x]", 'bright_yellow', 'bold'); print " $line [$note_tags[$x]] "; print colored("$data_ind\n", 'bright_green'); }
                $x++;
            }
        }
        else
        {
            my $entry_counter = "";
            my $total_matches = scalar @dates;
            my $padded_x;
            for my $line(@dates)
            {

                my $path = "";
                my $notepath_prefix = "";
                my $file = "";
                my $ft = "";
                $padded_x = padded_number($total_matches, $x + 1);
                #$entry_counter = "[$x] ";
                $entry_counter = "[$padded_x] ";
                if($view_md) { $entry_counter = ""; }
                if($dump_both) { $entry_counter = ""; }

                if(! $dump_contents)
                {
                    print_hl_heavy;
                    print colored("$entry_counter", 'bright_yellow');  print "Date       : $line\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Title      : $note_title[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "NoteId     : $note_id[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Description: $note_desc[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Filetype   : $note_type[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Project    : $note_proj[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Tags       : $note_tags[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Class      : $note_class[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "DataDirs   : $note_dirs[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "KeyPairs   : $key_pairs[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "Custom     : $note_cust[$x]\n";
                    print colored("$entry_counter", 'bright_yellow');  print "LinkedIds  : $note_links[$x]\n";
                    print_hl_heavy;

                }

                if($dump_contents || $dump_both)
                {
                    if(! $archive_search) { $path = "$ScribbleWibble::wibble_store/notes/$note_type[$x]/$note_proj[$x]"; }
                    else         { $path = "$ScribbleWibble::wibble_store/archived_notes/$note_type[$x]/$note_proj[$x]"; }

                    $file = $note_id[$x];
                    $notepath_prefix = substr $file, 0, 2;
                    $ft = $note_type[$x];

                    print_hl;
                    print "File $path/$notepath_prefix/$file.$ft";
                    print_hl;
                    print "\n";
                    system("cat $path/$notepath_prefix/$file.$ft");
                }

                $x++;
            }
            if($dump_both eq 1 || $dump_contents eq 1) { exit 0; }
            if($view_md) { exit 0; }
        }
        print "> ";
        my $response = <STDIN>;
        if(looks_like_number($response) && $response >= 1 && $response <= $y)
        {
            # We add 1 in presentation so count starts from 1 rather than 0
            $response = $response - 1;

            $editfile  = $note_id[$response];
            $edittype  = $note_type[$response];

            my $editdate  = $dates[$response];
            my $edittitle = $note_title[$response];
            my $editdesc  = $note_desc[$response];
            my $editproj  = $note_proj[$response];
            my $edittags  = $note_tags[$response];
            my $editclass = $note_class[$response];
            my $editdirs  = $note_dirs[$response];
            my $editpairs = $key_pairs[$response];
            my $editcust  = $note_cust[$response];
            my $linkids   = $note_links[$response];

            chomp $edittype;

            # switching logic
            if($add_assoc)
            {
                add_note_association($editfile, $linkids);
            }
            elsif($add_data)          { add_data_to_note($editfile, $edittype, $editdirs);    }
            elsif($add_link)
            {   add_linked_file_to_note($editfile, $edittype, $editdirs);                     }
            elsif($associate)         { associate_note($editfile, $linkids);                  }
            elsif($create_aa)         { create_auto_archive($editfile, $editdirs);            }
            elsif($edit_linked)       { edit_linked_spec_file($editfile);                     }
            elsif($edit_metadata)
            {
                edit_note_metadata($editfile, $edittype, $editdate, $edittitle,
                                   $editdesc, $editproj, $edittags, $editclass,
                                   $editdirs, $editpairs, $editcust, $linkids);
            }
            elsif($ls_dd)             { ls_data_dir($editfile, $edittype);                    }
            elsif($ls_linked)         { ls_linked_files($editfile, $edittype);                }
            elsif($move_to_archive || $restore_archive)
            {
                move_to_archive($editfile, $edittype, $editdate, $edittitle,
                    $editdesc, $editproj, $edittags, $editclass,
                    $editdirs, $editpairs, $editcust, $linkids);
            }
            elsif($open_dd)           { open_data_dir($editfile, $edittype);                  }
            elsif($related_notes)     { ls_related_notes($editfile, $edittype);               }
            # otherwise we're opening/editing the note or viewing metadata - default action
            else                      { open_with_editor($editfile, $edittype, $editproj);    }
        }
    }
}

sub search_note_basic
{
    my $results = "";
    if($verbose) { print "Full text search: $search_string\n"; }
    if($op_mode eq "RIPGREP")
    {
        if(! $dump_json)
        {
            print colored("No search field specified - so trying ALL descriptive fields:\n"
                      . "1. 'Title'\n2. 'Description'\n3. 'Project'\n4. 'Class'\n5. 'Custom'\n"
                          . "Note result may appear more than once.\n", 'bright_green'), "\n";
        }
        $results  = rg_search_by_field("Title", 0, $search_string, $archive_search, $verbose, "", 0);
        $results .= rg_search_by_field("Description", 0, $search_string, $archive_search, $verbose, "", 0);
        $results .= rg_search_by_field("Project", 0, $search_string, $archive_search, $verbose, "", 0);
        $results .= rg_search_by_field("Class", 0, $search_string, $archive_search, $verbose, "", 0);
        $results .= rg_search_by_field("Custom", 0, $search_string, $archive_search, $verbose, "", 0);

        display_results($results);
    }
    elsif($op_mode eq "RECUTILS")
    {
        if($archive_search) { $results = `recsel -t ScribbleWibble_Note -q "$search_string" $ScribbleWibble::archived_db_location`; }
        else                { $results = `recsel -t ScribbleWibble_Note -q "$search_string" $ScribbleWibble::db_location`;          }
        display_results($results);
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                      'bright_red'), "\n";
        exit(1);
    }

}

sub edit_note_metadata
{
    my $rename_file = 0;
    my ($note_id, $filetype, $editdate, $edittitle, $editdesc, $editproj,
        $edittags, $editclass, $editdirs, $editpairs, $editcust, $linkids) = @_;
    my $term;
    $term = Term::ReadLine->new('Edit metadata', \*STDIN, \*STDOUT);
    print colored("\n================== [ EDIT METADATA ] ======================", 'bright_yellow', 'bold');
    print colored("\nTitle      :", 'bright_yellow', 'bold'); print " $edittitle";
    print colored("\nFiletype   :", 'bright_yellow', 'bold'); print " $filetype";
    print colored("\nDescription:", 'bright_yellow', 'bold'); print " $editdesc";
    print colored("\nProject    :", 'bright_yellow', 'bold'); print " $editproj";
    print colored("\nTags       :", 'bright_yellow', 'bold'); print " $edittags";
    print colored("\nClass      :", 'bright_yellow', 'bold'); print " $editclass";
    print colored("\nDataDirs   :", 'bright_yellow', 'bold'); print " $editdirs";
    print colored("\nKeyPairs   :", 'bright_yellow', 'bold'); print " $editpairs";
    print colored("\nCustom     :", 'bright_yellow', 'bold'); print " $editcust";
    print colored("\nLinkedIds  :", 'bright_yellow', 'bold'); print " $linkids";
    print colored("\n===========================================================\n\n", 'bright_yellow', 'bold');

    # NULL field placeholder should be invisible to user input
    if($editdesc  eq "___NULL___") { $editdesc  = ""; }
    if($editproj  eq "___NULL___") { $editproj  = ""; }
    if($edittags  eq "___NULL___") { $edittags  = ""; }
    if($editclass eq "___NULL___") { $editclass = ""; }
    if($editdirs  eq "___NULL___") { $editdirs  = ""; }
    if($editpairs eq "___NULL___") { $editpairs = ""; }
    if($editcust  eq "___NULL___") { $editcust  = ""; }
    if($linkids   eq "___NULL___") { $linkids   = ""; }

    my $attempts = 0;
    my $prompt = "Title      : " ;
    my $prefilled = "$edittitle" ;
    do
    {
        $edittitle = $term->readline($prompt,$prefilled);
        ++$attempts;
    }
    while (! length $edittitle && $attempts < INPUT_ATTEMPTS);

    if (! length $edittitle)
    { print "\n❌ Title is required. Maximum attempts reached. Aborting.\n"; exit 1; };

    $attempts = 0;
    my $current_type = $filetype;
    my $current_proj = $editproj;
    $prompt = "Filetype: " ;
    $prefilled = "$filetype" ;
    do
    {
        $filetype = $term->readline($prompt,$prefilled) ;
        ++$attempts;
    }
    while (! length $filetype && $attempts < INPUT_ATTEMPTS);

    if (! length $filetype)
    { print "\n❌ Filetype is required. Maximum attempts reached. Aborting.\n"; exit 1; };

    $prompt = "Description: " ;
    $prefilled = "$editdesc" ;
    $editdesc = $term->readline($prompt,$prefilled) ;
    $prompt = "Project    : " ;
    $prefilled = "$editproj" ;
    $editproj = $term->readline($prompt,$prefilled) ;
    $prompt = "Tags       : " ;
    $prefilled = "$edittags" ;
    $edittags = $term->readline($prompt,$prefilled) ;
    $prompt = "Class      : " ;
    $prefilled = "$editclass" ;
    $editclass = $term->readline($prompt,$prefilled) ;
    $prompt = "DataDirs   : " ;
    $prefilled = "$editdirs" ;
    $editdirs = $term->readline($prompt,$prefilled) ;
    $prompt = "KeyPairs   : " ;
    $prefilled = "$editpairs" ;
    $editpairs = $term->readline($prompt,$prefilled) ;
    $prompt = "Custom     : " ;
    $prefilled = "$editcust" ;
    $editcust = $term->readline($prompt,$prefilled) ;
    $prompt = "LinkedIds  : " ;
    $prefilled = "$linkids" ;
    $linkids = $term->readline($prompt,$prefilled) ;

    # remove leading/trailing extraneous whitespace/tabs etc
    $filetype  =~ s/^\s+|\s+$//g ;
    $editdesc  =~ s/^\s+|\s+$//g ;
    $editproj  =~ s/^\s+|\s+$//g ; $editproj =~ tr/ /_/;
    $edittags  =~ s/^\s+|\s+$//g ;
    $editclass =~ s/^\s+|\s+$//g ;
    $editdirs  =~ s/^\s+|\s+$//g ;
    $editpairs =~ s/^\s+|\s+$//g ;
    $editcust  =~ s/^\s+|\s+$//g ;
    $linkids   =~ s/^\s+|\s+$//g ;

    # Put NULL field placeholder back as needed
    if(! length $editdesc)  { $editdesc  = "___NULL___"; }
    if(! length $editproj)  { $editproj  = "General";    }
    if(! length $edittags)  { $edittags  = "___NULL___"; }
    if(! length $editclass) { $editclass = "___NULL___"; }
    if(! length $editdirs)  { $editdirs  = "___NULL___"; }
    if(! length $editpairs) { $editpairs = "___NULL___"; }
    if(! length $editcust)  { $editcust  = "___NULL___"; }
    if(! length $linkids)   { $linkids   = "___NULL___"; }


    print colored("\n====================== [ UPDATE ] =========================", 'bright_yellow', 'bold');
    print colored("\nTitle      :", 'bright_yellow', 'bold'); print " $edittitle";
    print colored("\nFiletype   :", 'bright_yellow', 'bold'); print " $filetype";
    print colored("\nDescription:", 'bright_yellow', 'bold'); print " $editdesc";
    print colored("\nProject    :", 'bright_yellow', 'bold'); print " $editproj";
    print colored("\nTags       :", 'bright_yellow', 'bold'); print " $edittags";
    print colored("\nClass      :", 'bright_yellow', 'bold'); print " $editclass";
    print colored("\nDataDirs   :", 'bright_yellow', 'bold'); print " $editdirs";
    print colored("\nKeyPairs   :", 'bright_yellow', 'bold'); print " $editpairs";
    print colored("\nCustom     :", 'bright_yellow', 'bold'); print " $editcust";
    print colored("\nLinkedIds  :", 'bright_yellow', 'bold'); print " $linkids";
    print colored("\n===========================================================\n\n", 'bright_yellow', 'bold');

    # Display warnings in case either the filetype or project has changed
    # since this means the original file will be renamed/moved
    if($current_type ne $filetype)
    {
        print "WARNING. Filetype has changed from '$current_type' to '$filetype'.\n";
        print "This will result in renaming the file with the new file extension.\n";
    }
    if($current_proj ne $editproj)
    {
        print "WARNING. Project has changed from '$current_proj' to '$editproj'.\n";
        print "This will result in moving the file under the new project sub-directory.\n";
    }
    print colored("Apply changes?", 'bright_yellow', 'bold'); print " (y/n) > ";

    my $confirm = <STDIN>;
    chomp $confirm;

    if($confirm eq "y")
    {
        # extracted to common_funcs.pm - $from_file arg is "" here
        update_record_metadata($note_id,        $filetype,     $editdate,
                               $edittitle,      $editdesc,     $editproj,
                               $edittags,       $editclass,    $editdirs,
                               $editpairs,      $editcust,     $linkids,
                               $archive_search, $verbose,      "",
                               $current_type,   $current_proj, $op_mode);
    }
}

sub move_to_archive
{
    my $do_restore = 0;
    # if restore_archive flag present, function becomes "restore_from_archive"
    if($restore_archive == 1) { $do_restore = 1; }

    if(! $do_restore)
    {
        if($verbose) { print colored("Note archive operation in effect.\n", 'bright_green'); }
    }
    else
    {
        if($verbose) { print colored("Note restore operation in effect.\n", 'bright_green'); }

    }

    my $output_path = "";
    my ($note_id, $filetype, $editdate, $edittitle, $editdesc, $editproj,
        $edittags, $editclass, $editdirs, $editpairs, $editcust, $linkids) = @_;

    if(! length $note_id || ! length $filetype || ! length $editdate || ! length $edittitle)
    {
        print "Invalid note metadata, empty NoteId and/or Filetype and/or Date and/or Title.\n";
        print "Please manually fix note with ID '$note_id'\n";
        exit 1;
    }

    my $notepath_prefix = substr $note_id, 0, 2;
    my $note_file = "";
    my $note_dest = "";
    if(! $do_restore)
    {
        $note_file = "$ScribbleWibble::wibble_store/notes/$filetype/$editproj/$notepath_prefix/$note_id.$filetype";
        $note_dest = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$editproj/$notepath_prefix/";
    }
    else
    {
        $note_file = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$editproj/$notepath_prefix/$note_id.$filetype";
        $note_dest = "$ScribbleWibble::wibble_store/notes/$filetype/$editproj/$notepath_prefix/";
    }
    mkpath("$note_dest");

    my $note_copy = system("cp -v $note_file $note_dest");

    if($note_copy eq 1)
    {
        print "❌ Error archiving note. Aborting.";
        exit 1;
    }

    $edittitle =~ s/"/\\"/g;
    $filetype  =~ s/"/\\"/g;
    $editdesc  =~ s/"/\\"/g;
    $editproj  =~ s/"/\\"/g;
    $edittags  =~ s/"/\\"/g;
    $editclass =~ s/"/\\"/g;
    $editdirs  =~ s/"/\\"/g;
    $editpairs =~ s/"/\\"/g;
    $editcust  =~ s/"/\\"/g;
    $linkids   =~ s/"/\\"/g;

    if($op_mode eq "RIPGREP")
    {
        my $database_file;
        if(! $do_restore )
        {
            $database_file = "$ScribbleWibble::archived_db_location";
            make_tmp_db_backup(1);
        }
        else
        {
            $database_file = "$ScribbleWibble::db_location";
            make_tmp_db_backup(0);
        }

        open (my $file_handle, '>>', "$database_file") or die "$!: $database_file";
        my $entry_datetime = strftime "%a, %d %b %Y %T %z", localtime;
        my $record_string  = "\n";
           $record_string .= "Date: " . $editdate . "\n";
           $record_string .= "Title: " . $edittitle . "\n";
           $record_string .= "NoteId: " . $note_id . "\n";
           $record_string .= "Description: " . $editdesc . "\n";
           $record_string .= "Filetype: " . $filetype . "\n";
           $record_string .= "Project: " . $editproj . "\n";
           $record_string .= "Tags: " . $edittags . "\n";
           $record_string .= "Class: " . $editclass . "\n";
           $record_string .= "DataDirs: " . $editdirs . "\n";
           $record_string .= "KeyPairs: " . $editpairs . "\n";
           $record_string .= "Custom: " . $editcust . "\n";
           $record_string .= "LinkedIds: " . $linkids . "\n";

        print $file_handle $record_string;
        close($file_handle);

        if(! $do_restore) { print "✓ Note metadata added to archive database.\n"; }
        else              { print "✓ Note metadata added to main database.\n"; }

        if(! $do_restore ) { rg_delete_entry($note_id, 0, $verbose); }
        else               { rg_delete_entry($note_id, 1, $verbose);}

        if(! $do_restore) { print "✓ Note metadata successfully removed from main database.\n"; }
        else              { print "✓ Note metadata successfully removed from archive database.\n"; }

        my $delete_original = system("rm $note_file");

        if($delete_original eq 0)
        {
            if(! $do_restore) { print "✓ Original note removed. Now present in archive.\n"; }
            else              { print "✓ Original note removed. Now present in main database.\n"; }
        }
        else { print "❌ Error removing original note stored at $note_file. Manual fix required.\n"; }

        my $delete_empty_dir = "";
        if(! $do_restore) { $delete_empty_dir = system("find $ScribbleWibble::wibble_store/notes/ -type d -empty -delete"); }
        else     { $delete_empty_dir = system("find $ScribbleWibble::wibble_store/archived_notes/ -type d -empty -delete"); }

        if($delete_empty_dir eq 0) { print "✓ Original note directory removed.\n"; }
        else { print "❌ Error removing original note directory. Run 'wibble tool --clean' to fix.\n"; }
    }
    elsif($op_mode eq "RECUTILS")
    {
        my $rec_ins_cmd  = "recins -t ScribbleWibble_Note -f Date -v \"$editdate\" ";
        $rec_ins_cmd .= "-f Title -v \"$edittitle\" ";
        $rec_ins_cmd .= "-f NoteId -v '$note_id' -f Description -v \"$editdesc\" ";
        $rec_ins_cmd .= "-f Filetype -v \"$filetype\" ";
        $rec_ins_cmd .= "-f Project -v \"$editproj\" -f Tags -v \"$edittags\" ";
        $rec_ins_cmd .= "-f Class -v \"$editclass\" -f DataDirs -v \"$editdirs\" ";
        $rec_ins_cmd .= "-f KeyPairs -v \"$editpairs\" -f Custom -v \"$editcust\" ";
        $rec_ins_cmd .= "-f LinkedIds -v \"$linkids\" ";

        if(! $do_restore ) { $rec_ins_cmd .= "$ScribbleWibble::archived_db_location --verbose"; }
        else               { $rec_ins_cmd .= "$ScribbleWibble::db_location --verbose"; }

        my $recutils_insert = system("$rec_ins_cmd");
        if($recutils_insert eq 0)
        {
            if(! $do_restore) { print "✓ Note metadata added to archive database.\n"; }
            else              { print "✓ Note metadata added to main database.\n"; }
            my $recutils_delete = "";
            if(! $do_restore)
            {
                $recutils_delete = system("recdel -t ScribbleWibble_Note -e \"NoteId = '$note_id'\" $ScribbleWibble::db_location --verbose");
            }
            else
            {
                $recutils_delete = system("recdel -t ScribbleWibble_Note -e \"NoteId = '$note_id'\" $ScribbleWibble::archived_db_location --verbose");

            }

            if($recutils_delete eq 0)
            {
                if(! $do_restore) { print "✓ Note metadata successfully removed from main database.\n"; }
                else              { print "✓ Note metadata successfully removed from archive database.\n"; }

                my $delete_original = system("rm $note_file");

                if($delete_original eq 0)
                {
                    if(! $do_restore) { print "✓ Original note removed. Now present in archive.\n"; }
                    else              { print "✓ Original note removed. Now present in main database.\n"; }
                }
                else { print "❌ Error removing original note stored at $note_file. Manual fix required.\n"; }

                my $delete_empty_dir = "";
                if(! $do_restore) { $delete_empty_dir = system("find $ScribbleWibble::wibble_store/notes/ -type d -empty -delete"); }
                else     { $delete_empty_dir = system("find $ScribbleWibble::wibble_store/archived_notes/ -type d -empty -delete"); }

                if($delete_empty_dir eq 0) { print "✓ Original note directory removed.\n"; }
                else { print "❌ Error removing original note directory. Run 'wibble tool --clean' to fix.\n"; }
            }
            else
            { print "❌ Error removing note metadata for NoteId $note_id. Manual fix required.\n"; }
        }
        else { print "❌ Error inserting new note metadata for NoteId $note_id. Aborting.\n"; }
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                      'bright_red'), "\n";
        exit(1);
    }
}

sub open_with_editor
{
    my $output_path = "";
    my ($note_id, $filetype, $project) = @_;
    my $notepath_prefix = substr $note_id, 0, 2;
    if ($archive_search) { $output_path = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$project/$notepath_prefix"; }
    else { $output_path = "$ScribbleWibble::wibble_store/notes/$filetype/$project/$notepath_prefix"; }
    my @created = mkpath("$output_path");
    if($verbose)
    {
        print "Matched note with ID $note_id.\n";
        print "Editor command: $ScribbleWibble::editor\n";
        print "Note/file path: $output_path/$note_id.$filetype\n";
    }

    if($use_bare)        { print "$output_path/$note_id.$filetype";                                   }
    elsif($use_id_only)  { print "$note_id";                                                          }
    elsif($show_dd_path) { print "$ScribbleWibble::wibble_store/note_data/$notepath_prefix/$note_id"; }
    elsif($use_gui)      { system("$ScribbleWibble::gui_editor $output_path/$note_id.$filetype &");   }
    else { system("$ScribbleWibble::editor $output_path/$note_id.$filetype");                         }
}

sub search_by_note_field
{
    my $results = "";

    if($op_mode eq "RIPGREP")
    {
        # ================  Begin RIPGREP search ================
        $results = rg_search_by_field($search_field, $exact_match, $search_string, $archive_search, $verbose, "", 0);
        # ================   End RIPGREP search  ================
    }
    elsif($op_mode eq "RECUTILS")
    {

        # ================ Begin RECUTILS search ================
        my $search_cmd = "";
        my $search_cmd_archived = "$ScribbleWibble::archived_db_location";
        my $search_cmd_main = " $ScribbleWibble::db_location";
        my $search_exec_main = "";
        my $search_exec_archived = "";

        if($exact_match)
        {
            if($verbose) { print "Searching field \"$search_field\" for exact match \"$search_string\".\n"; }

            # careful escaping required in order to accommodate searches like "I've seen"
            $search_cmd = "recsel -t ScribbleWibble_Note -i -e \"$search_field = \\\"$search_string\\\"\" ";
            $search_exec_main = $search_cmd . $search_cmd_main;
            $search_exec_archived = $search_cmd . $search_cmd_archived;

            if(! $archive_search) { $results = `$search_exec_main`; }
            else { $results = `$search_exec_archived`; }
        }
        elsif($expert_search)
        {
            print "Enter recsel -t ScribbleWibble_Note selection expression: ";
            my $rec_exp = <STDIN>;
            chomp $rec_exp;
            if($archive_search) { $results = `recsel -t ScribbleWibble_Note -i -e "$rec_exp" $ScribbleWibble::archived_db_location`; }
            else { $results = `recsel -t ScribbleWibble_Note -i -e "$rec_exp" $ScribbleWibble::db_location`; }
        }
        else
        {
            $search_cmd = "recsel -t ScribbleWibble_Note -i -e \"$search_field ~ \\\"$search_string\\\"\" ";
            $search_exec_main = $search_cmd . $search_cmd_main;
            $search_exec_archived = $search_cmd . $search_cmd_archived;
            if($verbose) { print "Searching field \"$search_field\" for \"$search_string\".\n"; }
            if(! $archive_search) { $results = `$search_exec_main`; }
            else { $results = `$search_exec_archived`; }
        }
        # ================  End RECUTILS search  ================
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                      'bright_red'), "\n";
        exit(1);
    }

    #print colored("DEBUG: EXPORT RESULT: $export_result", 'bright_red', 'bold'), "\n";
    if($export_result) { $export_result = 0; return $results; }
    else               { display_results($results); }
}

sub pretty_print_note
{
    my ($entry_counter, $date, $note_title, $note_id,
        $note_type, $note_desc, $note_proj,
        $note_tags, $note_class, $note_dirs,
        $key_pairs, $note_cust, $note_linked ) = @_;

    if(! length $date)        { $date        = "___FIELD_MISSING___"; }
    if(! length $note_title)  { $note_title  = "___FIELD_MISSING___"; }
    if(! length $note_id)     { $note_id     = "___FIELD_MISSING___"; }
    if(! length $note_type)   { $note_type   = "___FIELD_MISSING___"; }
    if(! length $note_desc)   { $note_desc   = "___FIELD_MISSING___"; }
    if(! length $note_proj)   { $note_proj   = "___FIELD_MISSING___"; }
    if(! length $note_tags)   { $note_tags   = "___FIELD_MISSING___"; }
    if(! length $note_class)  { $note_class  = "___FIELD_MISSING___"; }
    if(! length $note_dirs)   { $note_dirs   = "___FIELD_MISSING___"; }
    if(! length $key_pairs)   { $key_pairs   = "___FIELD_MISSING___"; }
    if(! length $note_cust)   { $note_cust   = "___FIELD_MISSING___"; }
    if(! length $note_linked) { $note_linked = "___FIELD_MISSING___"; }

    print "===============================================\n";
    print $entry_counter . "Date       : $date\n";
    print $entry_counter . "Title      : $note_title\n";
    print $entry_counter . "NoteId     : $note_id\n";
    print $entry_counter . "Description: $note_desc\n";
    print $entry_counter . "Filetype   : $note_type\n";
    print $entry_counter . "Project    : $note_proj\n";
    print $entry_counter . "Tags       : $note_tags\n";
    print $entry_counter . "Class      : $note_class\n";
    print $entry_counter . "DataDirs   : $note_dirs\n";
    print $entry_counter . "KeyPairs   : $key_pairs\n";
    print $entry_counter . "Custom     : $note_cust\n";
    print $entry_counter . "LinkedIds  : $note_linked\n";
    print "===============================================\n";
}

sub display_note_from_recsel_result
{
    my ($note_id) = @_;
    my $note_data;
    if($op_mode eq "RIPGREP")
    {
        $note_data = rg_search_by_field("NoteId", 1, $note_id, $archive_search, $verbose, "", "");
    }
    elsif($op_mode eq "RECUTILS")
    {
        my $search_cmd_archived = "$ScribbleWibble::archived_db_location";
        my $search_cmd_main = "$ScribbleWibble::db_location";
        my $search_cmd = "recsel -t ScribbleWibble_Note -e 'NoteId = \"$note_id\"' ";
        my $search_exec_main = $search_cmd . $search_cmd_main;
        my $search_exec_archived = $search_cmd . $search_cmd_archived;
        $note_data = `$search_exec_main`;
    }
    my @date       = ();
    my @note_title = ();
    my @note_l_id  = ();
    my @note_type  = ();
    my @note_desc  = ();
    my @note_proj  = ();
    my @note_tags  = ();
    my @note_class = ();
    my @note_dirs  = ();
    my @key_pairs  = ();
    my @note_cust  = ();
    my @note_links = ();

    @date       = ( $note_data =~ /^Date:\s*(.*)$/gm );
    @note_title = ( $note_data =~ /^Title:\s*(.*)$/gm );
    @note_l_id  = ( $note_data =~ /^NoteId:\s*(.*)$/gm );
    @note_type  = ( $note_data =~ /^Filetype:\s*(.*)$/gm );
    @note_desc  = ( $note_data =~ /^Description:\s*(.*)$/gm );
    @note_proj  = ( $note_data =~ /^Project:\s*(.*)$/gm );
    @note_tags  = ( $note_data =~ /^Tags:\s*(.*)$/gm );
    @note_class = ( $note_data =~ /^Class:\s*(.*)$/gm );
    @note_dirs  = ( $note_data =~ /^DataDirs:\s*(.*)$/gm );
    @key_pairs  = ( $note_data =~ /^KeyPairs:\s*(.*)$/gm );
    @note_cust  = ( $note_data =~ /^Custom:\s*(.*)$/gm );
    @note_links = ( $note_data =~ /^LinkedIds:\s*(.*)$/gm );

    pretty_print_note("", $date[0],      $note_title[0], $note_l_id[0],
                          $note_type[0], $note_desc[0],  $note_proj[0],
                          $note_tags[0], $note_class[0], $note_dirs[0],
                          $key_pairs[0], $note_cust[0],  $note_links[0]);

    # print colored("=== [END] display_note_from_recsel_result ===", 'bright_red'), "\n";
}

sub ls_related_notes
{
    my ($note_id, $filetype) = @_;
    my $output_path = "";
    my @related_notes = get_related_notes($note_id, $filetype);
    my @child_notes = ();
    my @parent_notes = ();
    my @sibling_notes = ();

    for my $rl_note (@related_notes)
    {
        my $rl_note_hier = substr $rl_note, 0, 3;
        my $rl_note_id   = substr $rl_note, 3;

        if($rl_note_hier eq "C¬") { push @child_notes, $rl_note_id; }
        if($rl_note_hier eq "P¬") { push @parent_notes, $rl_note_id; }
        if($rl_note_hier eq "S¬") { push @sibling_notes, $rl_note_id; }
    }

    $search_field = "NoteId";
    if(scalar @parent_notes gt 0)
    {
        print "\nParent of:\n\n";
        for my $rl_note (@parent_notes)
        {
            chomp($rl_note);
            print "* $rl_note\n";
        }

        print("\n---\n");

        for my $rl_note (@parent_notes)
        {
            chomp($rl_note);
            display_note_from_recsel_result($rl_note);
        }
    }
    if(scalar @sibling_notes gt 0)
    {
        print "\nSibling of:\n\n";
        for my $rl_note (@sibling_notes)
        {
            chomp($rl_note);
            print "* $rl_note\n";
        }

        print("\n---\n");

        for my $rl_note (@sibling_notes)
        {
            chomp($rl_note);
            display_note_from_recsel_result($rl_note);
        }

    }
    if(scalar @child_notes gt 0)
    {
        print "\nChild of:\n\n";
        for my $rl_note (@child_notes)
        {
            chomp($rl_note);
            print "* $rl_note\n";
        }

        print("\n---\n");

        for my $rl_note (@child_notes)
        {
            #print "* $rl_note\n";
            chomp($rl_note);
            display_note_from_recsel_result($rl_note);
        }
    }
}

sub perform_search
{
    my ($search) = @_;

    # determine whether to do a fulltext/substring search
    # or search a particular field, or an "expert" search
    # by inputing a recutils search expression (SEXP)
    if ($search->{full}) { search_note_basic; }
    else
    {
        if ($search->{exact})       { $exact_match = 1;              }
        if ($search->{id})          { $search_field = "NoteId";      }
        if ($search->{date})        { $search_field = "Date";        }
        if ($search->{title})       { $search_field = "Title";       }
        if ($search->{description}) { $search_field = "Description"; }
        if ($search->{project})     { $search_field = "Project";     }
        if ($search->{tags})        { $search_field = "Tags";        }
        if ($search->{filetype})    { $search_field = "Filetype";    }
        if ($search->{class})       { $search_field = "Class";       }
        if ($search->{keypairs})    { $search_field = "KeyPairs";    }
        if ($search->{custom})      { $search_field = "Custom";      }
        if ($search->{linkids})     { $search_field = "LinkedIds";   }

        if($search_field eq "" && $expert_search == 0)
        {
            if($verbose)
            { print "WARNING - No search field specified. Defaulting to substring/basic (--full).\n\n"; }
            search_note_basic;
        }
        else
        {
            search_by_note_field;
        }
    }
}

sub search_menu
{

    my $results = "";

    my $date_search; my $title_search; my $id_search;   my $desc_search;
    my $ft_search;   my $proj_search;  my $tags_search; my $class_search;
    my $data_search; my $keys_search;  my $cust_search; my $link_ids;

    if(! $export_result)
    {
        print "Input search terms for each field. Leave blank\n";
        print "(just press <ENTER>) to skip field.\n\n";
        print "---\n";
        print "Date       : ";
        $date_search = <STDIN>;
        chomp $date_search;
        print "Title      : ";
        $title_search = <STDIN>;
        chomp $title_search;
        print "NoteId     : ";
        $id_search = <STDIN>;
        chomp $id_search;
        print "Description: ";
        $desc_search = <STDIN>;
        chomp $desc_search;
        print "Filetype   : ";
        $ft_search = <STDIN>;
        chomp $ft_search;
        print "Project    : ";
        $proj_search = <STDIN>;
        chomp $proj_search; $proj_search =~ tr/ /_/;
        print "Tags       : ";
        $tags_search = <STDIN>;
        chomp $tags_search;
        print "Class      : ";
        $class_search = <STDIN>;
        chomp $class_search;
        print "DataDirs   : ";
        $data_search = <STDIN>;
        chomp $data_search;
        print "KeyPairs   : ";
        $keys_search = <STDIN>;
        chomp $keys_search;
        print "Custom     : ";
        $cust_search = <STDIN>;
        chomp $cust_search;
        print "LinkedIds  : ";
        $link_ids = <STDIN>;
        chomp $link_ids;
        print "---\n\n";
    }
    else
    {
        # FIXME: pass in archived flag
     ( $date_search, $title_search, $id_search,   $desc_search,
       $ft_search,   $proj_search,  $tags_search, $class_search,
       $data_search, $keys_search,  $cust_search, $link_ids      ) = @_;
    }

    if($op_mode eq "RIPGREP")
    {
        # ================  Begin RIPGREP search ================
        $results = rg_multiple_field_search( $date_search, $title_search, $id_search,   $desc_search,
                                             $ft_search,   $proj_search,  $tags_search, $class_search,
                                             $data_search, $keys_search,  $cust_search, $link_ids, $archive_search,
                                             $verbose );
        # ================   End RIPGREP search  ================
    }
    elsif($op_mode eq "RECUTILS")
    {
        # ================ Begin RECUTILS search ================
        my $menu_search = "";
        if (length $date_search) { $menu_search .= "(Date ~ '$date_search')"; }
        if (length $title_search)
        {
            if(length $menu_search) { $menu_search .= " && (Title ~ '$title_search')"; }
            else { $menu_search .= "(Title ~ '$title_search')"; }
        }
        if (length $id_search)
        {
            if(length $menu_search) { $menu_search .= " && (NoteId ~ '$id_search')"; }
            else { $menu_search .= "(NoteId ~ '$id_search')"; }
        }
        if (length $desc_search)
        {
            if(length $menu_search) { $menu_search .= " && (Description ~ '$desc_search')"; }
            else { $menu_search .= "(Description ~ '$desc_search')"; }
        }
        if (length $ft_search)
        {
            if(length $menu_search) { $menu_search .= " && (Filetype ~ '$ft_search')"; }
            else { $menu_search .= "(Filetype ~ '$ft_search')"; }
        }
        if (length $proj_search)
        {
            if(length $menu_search) { $menu_search .= " && (Project ~ '$proj_search')"; }
            else { $menu_search .= "(Project ~ '$proj_search')"; }
        }
        if (length $class_search)
        {
            if(length $menu_search) { $menu_search .= " && (Class ~ '$class_search')"; }
            else { $menu_search .= "(Class ~ '$class_search')"; }
        }
        if (length $data_search)
        {
            if(length $menu_search) { $menu_search .= " && (DataDirs ~ '$data_search')"; }
            else { $menu_search .= "(DataDirs ~ '$data_search')"; }
        }
        if (length $keys_search)
        {
            if(length $menu_search) { $menu_search .= " && (KeyPairs ~ '$keys_search')"; }
            else { $menu_search .= "(KeyPairs ~ '$keys_search')"; }
        }
        if (length $cust_search)
        {
            if(length $menu_search) { $menu_search .= " && (Custom ~ '$cust_search')"; }
            else { $menu_search .= "(Custom ~ '$cust_search')"; }
        }
        if (length $link_ids)
        {
            if(length $menu_search) { $menu_search .= " && (LinkedIds ~ '$link_ids')"; }
            else { $menu_search .= "(LinkedIds ~ '$link_ids')"; }
        }

        if(! length $menu_search)
        {
            print "Warning: No search expression specified. ";
            if (! $export_result ) { print "Exiting.\n"; exit 0; }
            $export_result = 0;
            return "NULL_SEARCH";
        }

        print "Searching with expression:\n$menu_search\n\n";
        if(! $archive_search) { $results = `recsel -t ScribbleWibble_Note -i -e "$menu_search" $ScribbleWibble::db_location`; }
        else { $results = `recsel -t ScribbleWibble_Note -i -e "$menu_search" $ScribbleWibble::archived_db_location`; }
        # ================  End RECUTILS search  ================
    }
    else
    {
        print "Unknown mode '$op_mode'. Aborting.\n"; exit(1);
    }

    #print colored("DEBUG: EXPORT RESULT: $export_result", 'bright_red', 'bold'), "\n";
    if($export_result) { $export_result = 0; return $results; }
    else               { display_results($results); }
}

sub return_all_notes
{
    my ($export_result_extern) = @_;
    $export_result = $export_result_extern;
    my $results = "";
    # Skip searching entirely, just use cat/tail bypassing field definitions
    #my $match_all = "( NoteId ~ '[A-Za-z0-9]*' )";
    #if(! $archive_search) { $results = `recsel -t ScribbleWibble_Note -i -e "$match_all" $ScribbleWibble::db_location`; }
    #else { $results = `recsel -t ScribbleWibble_Note -i -e "$match_all" $ScribbleWibble::archived_db_location`; }
    if(! $archive_search) { $results = `tail -n +10 $ScribbleWibble::db_location`; }
    else                  { $results = `tail -n +10 $ScribbleWibble::archived_db_location`; }
    if($export_result) { $export_result = 0; return $results; }
    else               { display_results($results); }
}

sub associate_note
{

    my ($note_id, $linkids) = @_;
    display_note_from_recsel_result($note_id);
    print("[SOURCE] Associate note? (y/n)");
    my $response = <STDIN>;
    chomp($response);
    if($response eq "y")
    {
        $note_to_associate = $note_id;
        $note_linked_ids = $linkids;
        $add_assoc = 1; $associate = 0;
        print("Select note to associate/link to:\n");
        search_menu;
    }
}

sub set_flags
{
    my ($search) = @_;
    my $enabled_modes = 0;
    # operation flags
    if($search->{archived} || $search->{from_archive})
    {
        $archive_search = 1;
        if($verbose)
        {
            print colored("\nPerforming ", 'bright_green');
            print colored("archive ", 'bright_blue');
            print colored("search with database $ScribbleWibble::archived_db_location.\n\n", 'bright_green');
        }
    }
    else
    {
        if($verbose)
        {
            print colored("\nPerforming standard search with database $ScribbleWibble::db_location.\n\n", 'bright_green');
        }
    }

    # search result flags
    if($search->{advanced})      { $expert_search = 1;                                                               }
    if($search->{allfields})     { $display_full_record = 1;                                                         }
    if($search->{bare})          { $use_bare = 1;                                                                    }
    if($search->{idonly})        { $use_id_only = 1;                                                                 }
    if($search->{guiedit})       { $use_gui = 1;                                                                     }
    if($search->{only_single})   { $only_single = 1;                                                                 }

    # search mode operations
    if($search->{add_data})      { $add_data = 1; ++$enabled_modes;                                                  }
    if($search->{add_link})      { $add_link = 1; ++$enabled_modes;                                                  }
    if($search->{associate})     { $associate = 1; ++$enabled_modes;                                                 }
    if($search->{create_aa} || $search->{open_aa}) { $create_aa = 1; ++$enabled_modes;                               }
    if($search->{dump_both})     { $dump_both = 1; $display_full_record = 1; ++$enabled_modes;                       }
    if($search->{dump_contents}) { $dump_contents = 1; $display_full_record = 1; ++$enabled_modes;                   }
    if($search->{dump_json})     { $dump_json = 1; ++$enabled_modes;                                                 }
    if($search->{edit_linked})   { $edit_linked = 1; ++$enabled_modes;                                               }
    if($search->{edit_md})       { $edit_metadata = 1; ++$enabled_modes;                                             }
    if($search->{everything})    { $everything = 1; ++$enabled_modes;                                                }
    if($search->{from_archive})  { $restore_archive = 1; ++$enabled_modes;                                           }
    if($search->{ls_dd})         { $ls_dd = 1; ++$enabled_modes;                                                     }
    if($search->{ls_linked})     { $ls_linked = 1; ++$enabled_modes;                                                 }
    if($search->{menu})          { $use_menu_search = 1;                                                             }
    if($search->{open_dd})       { $open_dd = 1; ++$enabled_modes;                                                   }
    if($search->{show_dd_dir})   { $show_dd_path = 1; ++$enabled_modes;                                              }
    if($search->{related_notes}) { $related_notes = 1; $display_full_record = 1; ++$enabled_modes;                   }
    if($search->{to_archive})    { $move_to_archive = 1; ++$enabled_modes;                                           }
    if($search->{view_md})       { $view_md = 1; $display_full_record = 1; ++$enabled_modes;                         }

    # block quick edit for certain actions
    if($add_data == 1)           { $quick_edit = 0;                                                                  }
    if($add_link == 1)           { $quick_edit = 0;                                                                  }
    if($associate == 1)          { $quick_edit = 0;                                                                  }
    if($create_aa == 1)          { $quick_edit = 0;                                                                  }
    if($dump_both == 1)          { $quick_edit = 0;                                                                  }
    if($dump_contents == 1)      { $quick_edit = 0;                                                                  }
    if($dump_json == 1)          { $quick_edit = 0;                                                                  }
    if($edit_linked == 1)        { $quick_edit = 0;                                                                  }
    if($edit_metadata == 1)      { $quick_edit = 0;                                                                  }
    if($everything == 1)         { $quick_edit = 0;                                                                  }
    if($ls_dd == 1)              { $quick_edit = 0;                                                                  }
    if($ls_linked == 1)          { $quick_edit = 0;                                                                  }
    if($move_to_archive == 1)    { $quick_edit = 0;                                                                  }
    if($open_aa == 1)            { $quick_edit = 0;                                                                  }
    if($open_dd == 1)            { $quick_edit = 0;                                                                  }
    if($related_notes == 1)      { $quick_edit = 0;                                                                  }
    if($restore_archive == 1)    { $quick_edit = 0;                                                                  }
    if($view_md == 1)            { $quick_edit = 0;                                                                  }

    if($enabled_modes > 1 || $verbose)
    {
        if($enabled_modes > 1)
        {
            print "NOTE: Redundant search filters are silently ignored.\n";
            print "(e.g. using --title with --menu: --title will be ignored.)\n\n";
            print "❌ Error: Multiple conflicting options.\n";
            print "Please enable only one mode from the following list per invocation.\n\n";
        }

        # Quick edit active?
        if($quick_edit) { print colored("QUICK EDIT ENABLED\n", 'bright_green'), "\n"; }
        else            { print colored("QUICK EDIT DISABLED\n", 'bright_red'), "\n"; }

        # Mode flags
        if($add_data == 1)        { print colored ("--add-data     : ENABLED", 'bright_green'), "\n"; } else { print colored ("--add-data     : DISABLED", 'bright_red'), "\n"; }
        if($add_link == 1)        { print colored ("--add-link     : ENABLED", 'bright_green'), "\n"; } else { print colored ("--add-link     : DISABLED", 'bright_red'), "\n"; }
        if($associate == 1)       { print colored ("--associate    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--associate    : DISABLED", 'bright_red'), "\n"; }
        if($create_aa == 1)       { print colored ("--create-aa    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--create-aa    : DISABLED", 'bright_red'), "\n"; }
        if($dump_both == 1)       { print colored ("--dump-both    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--dump-both    : DISABLED", 'bright_red'), "\n"; }
        if($dump_contents == 1)   { print colored ("--dump-contents: ENABLED", 'bright_green'), "\n"; } else { print colored ("--dump-contents: DISABLED", 'bright_red'), "\n"; }
        if($dump_json == 1)       { print colored ("--dump-json    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--dump-json: DISABLED", 'bright_red'), "\n";     }
        if($edit_metadata == 1)   { print colored ("--edit-md      : ENABLED", 'bright_green'), "\n"; } else { print colored ("--edit-md      : DISABLED", 'bright_red'), "\n"; }
        if($edit_linked == 1)     { print colored ("--edit-linked  : ENABLED", 'bright_green'), "\n"; } else { print colored ("--edit-linked  : DISABLED", 'bright_red'), "\n"; }
        if($everything == 1)      { print colored ("--everything   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--everything   : DISABLED", 'bright_red'), "\n"; }
        if($restore_archive == 1) { print colored ("--from-archive : ENABLED", 'bright_green'), "\n"; } else { print colored ("--from-archive : DISABLED", 'bright_red'), "\n"; }
        if($ls_dd == 1)           { print colored ("--ls-dd        : ENABLED", 'bright_green'), "\n"; } else { print colored ("--ls-dd        : DISABLED", 'bright_red'), "\n"; }
        if($ls_linked == 1)       { print colored ("--ls-linked    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--ls-linked    : DISABLED", 'bright_red'), "\n"; }
        if($use_menu_search == 1) { print colored ("--menu         : ENABLED", 'bright_green'), "\n"; } else { print colored ("--menu         : DISABLED", 'bright_red'), "\n"; }
        if($open_dd == 1)         { print colored ("--open-dd      : ENABLED", 'bright_green'), "\n"; } else { print colored ("--open-dd      : DISABLED", 'bright_red'), "\n"; }
        if($show_dd_path == 1)    { print colored ("--show-dd-dir  : ENABLED", 'bright_green'), "\n"; } else { print colored ("--show-dd-dir  : DISABLED", 'bright_red'), "\n"; }
        if($related_notes == 1)   { print colored ("--related-notes: ENABLED", 'bright_green'), "\n"; } else { print colored ("--related-notes: DISABLED", 'bright_red'), "\n"; }
        if($move_to_archive == 1) { print colored ("--to-archive   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--to-archive   : DISABLED", 'bright_red'), "\n"; }
        if($view_md == 1)         { print colored ("--view-md      : ENABLED", 'bright_green'), "\n"; } else { print colored ("--view-md      : DISABLED", 'bright_red'), "\n"; }


        print "\n";
        if($enabled_modes gt 1) { exit 1; }
    }
}


# ========== BEGIN EXPORTED FUNCTIONS  ===========
sub check_verbose
{
    my ($self) = @_;
    $verbose = $self->app->global_options->{verbose};
    if (length $verbose) { $verbose = 1; }
    else { $verbose = 0 }
    return $verbose;
}

sub set_config_file
{
    my ($config) = @_;
    $config =~ s/~/$ENV{HOME}/g;
    $ScribbleWibble::config_file = $config;
    if($verbose) { print colored ("Configuration file: $ScribbleWibble::config_file\n", 'bright_red', 'bold'); }
}

sub check_config_override
{
    my ($self) = @_;
    if($self->app->global_options->{config})
    {
        $config_override = 1;
        if($verbose) { print colored("CONFIGURATION OVERRIDE ENABLED.\n", 'bright_red', 'bold'); }
        return 1;
    }
    else
    {
        return 0;
    }
}

sub extern_search_by_field
{
    my ($ark_search, $field, $term, $passed_op_mode) = @_;
    parameter_check(\@_, 4, "extern_search_by_field");

    $export_result = 1;
    $archive_search = $ark_search;
    $search_field = $field;
    $search_string = $term;
    $op_mode = $passed_op_mode;
    #search_by_note_field;
    # Important, flag prevents usual exit when running as normal CLI
    $export_result = 1;
    return search_by_note_field;
}

sub extern_multiple_field_search
{
    my ($ark_search,
        $date_search, $title_search, $id_search,   $desc_search,
        $ft_search,   $proj_search,  $tags_search, $class_search,
        $data_search, $keys_search,  $cust_search, $link_ids, $passed_op_mode  ) = @_;
    parameter_check(\@_, 14, "extern_multiple_field_search");

    $op_mode = $passed_op_mode;
    # Important, flag prevents usual exit when running as normal CLI
    $export_result = 1;
    $archive_search = $ark_search;

    return search_menu($date_search, $title_search, $id_search,   $desc_search,
                $ft_search,   $proj_search,  $tags_search, $class_search,
                $data_search, $keys_search,  $cust_search, $link_ids);
}
# ================================================

sub set_operation_mode
{
    if($ScribbleWibble::op_mode ne "ripgrep" && $ScribbleWibble::op_mode ne "recutils")
    {
        print colored("❌ Error. Operation mode must be specified. "
                      . "Set op_mode flag to either 'recutils' or 'ripgrep'.", 'bright_red', 'bold'), "\n";
        exit(1);
    }

    if($ScribbleWibble::op_mode eq "ripgrep")
    {
        $op_mode = "RIPGREP";
    }
    else
    {
        $op_mode = "RECUTILS";
    }

    if($verbose)
    {
        print colored("Using operation mode: $op_mode", 'bright_green', 'bold'), "\n";
    }
    #print colored("DEBUG: op mode: $op_mode", 'bright_red', 'bold'),  "\n";
}


sub execute
{
    check_verbose(@_);
    check_config_override(@_);
    my ($self, $opt, $args) = @_;
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }
    $search_string = "@{$args}";

    if ( ! length $search_string && ! $opt->{advanced} && ! $opt->{menu} && ! $opt->{everything} )
    {
        print "❌ Error. No seach term specified. Aborting.\n";
        exit 0;
    }

    $self->app->read_config_file;
    set_operation_mode;

    set_flags($opt);

    if($opt->{menu})          { search_menu; }
    elsif($opt->{everything}) { return_all_notes(0); }
    else                      { perform_search($opt); }
}

1;
