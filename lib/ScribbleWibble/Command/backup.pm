# ABSTRACT: Backup notes/metadata and optionally all note_data
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::backup;
use ScribbleWibble -command;
#use base 'App::Cmd::Command';
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy );
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Path;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;
use Term::ReadLine;  # Do not "use Term::ReadLine::Gnu;"
use strict;
use warnings;

my $backup = 0;
my $full_backup = 0;
my $ls_backup = 0;
my $prune = 0;
my $verbose;

sub opt_spec
{
    return (
        ["backup|B"       => "Perform backup of all notes/metadata (EXCLUDES note data/attachments)"],
        ["full-backup|F"  => "Perform backup of EVERYTHING, including all note data/attachments"],
        ["log|L"          => "Display backup log"],
        ["prune|P"        => "Prune old backups"],
           );
}

sub description
{
    return "Use this command to backup the note data\n"
          . "and optionally prune old archives.\n";
}

sub do_backup
{
    my($do_full) = @_;
    print colored("\nPerforming backup.", 'bright_green'), "\n\n";

    my $date_suffix = strftime "%Y-%m-%d_%H_%M_%S", localtime;
    my $wib_backup = "";

    if($do_full) { $wib_backup = "wibble-FULL-backup-${date_suffix}.txz"; }
    else         { $wib_backup = "wibble-backup-${date_suffix}.txz"; }

    my $backup_dir = "$ScribbleWibble::backup_location/wibble_backup";
    if(! -d $backup_dir) { mkpath($backup_dir) or die "Unable to create backup directory $backup_dir!\n"; }
    my $cp_cmd = "echo '-> Copying archived notes'; cp -a $ScribbleWibble::wibble_store/archived_notes $backup_dir && "
               . "echo '-> Copying bookmarks'     ; cp -a $ScribbleWibble::wibble_store/bookmarks $backup_dir && "
               . "echo '-> Copying journal  '     ; cp -a $ScribbleWibble::wibble_store/journal $backup_dir && "
               . "echo '-> Copying notes    '     ; cp -a $ScribbleWibble::wibble_store/notes $backup_dir && "
               . "echo '-> Copying quicknotes'    ; cp -a $ScribbleWibble::wibble_store/quicknotes $backup_dir && "
               . "echo '-> Copying tasks'         ; cp -a $ScribbleWibble::wibble_store/tasks $backup_dir && "
               . "echo '-> Copying templates'     ; cp -a $ScribbleWibble::wibble_store/templates $backup_dir && "
               . "echo '-> Copying main wibble DB'; cp -a $ScribbleWibble::db_location $backup_dir && "
               . "echo '-> Copying archived DB'   ; cp -a $ScribbleWibble::archived_db_location $backup_dir && ";

    if($do_full)
    {
        $cp_cmd .= "echo '-> Copying ALL NOTE DATA'; cp -a $ScribbleWibble::wibble_store/note_data $backup_dir && "
                 . " cd $backup_dir/.. && ";
    }
    else
    {
        $cp_cmd .= "echo '-> SKIPPING note_data'    ; cd $backup_dir/.. && ";
    }

    my $log_msg = "";
    if($do_full) { $log_msg = "FULL backup including data made at: $date_suffix"; }
    else         { $log_msg = "note/metadata backup made at: $date_suffix"; }
    $cp_cmd .=  "echo '-> Creating XZ compressed tar archive' && echo '' && tar -cJvf $wib_backup wibble_backup/ && "
              . "echo '* Wibble $log_msg' >> $ScribbleWibble::backup_location/wibble-backup.log && "
              . "echo '' && echo '-> Archive creation into $wib_backup complete.' && sleep 0.5 && find $backup_dir -delete";

    my $cp_files = system("$cp_cmd");
    #print "DEBUG: $cp_cmd\n";
    if($cp_files == 0)
    {
        if(! $do_full)
        {

            print colored("\nBackup of note data into:\n\n${ScribbleWibble::backup_location}\n\nsuccessful. "
                        . "Note that note_data/ was NOT backed up.", 'bright_green'), "\n";
            print colored("Please use '--full-backup' if you wish to create an archived backup "
                        . "INCLUDING all external attached data.", 'bright_green'), "\n";
        }
        else
        {
            print colored("\nFULL Backup of note data into:\n\n${ScribbleWibble::backup_location}\n\nsuccessful. "
                          . "This includes all attachments and all data contained under note_data/.", 'bright_green'), "\n";
        }
    }
}

sub show_backup_log
{
    print colored("Backup history into $ScribbleWibble::backup_location:", 'bright_green'), "\n\n";
    system("cat $ScribbleWibble::backup_location/wibble-backup.log");
    exit 0;
}

sub do_full_backup
{
    print colored("\nFULL backup initiatiated, including note_data.", 'bright_yellow'), "\n\n";
    do_backup(1);
}

sub dispatch_backup
{
    if ($backup)         { do_backup(0);    }
    elsif ($full_backup) { do_full_backup;  }
    elsif ($ls_backup)   { show_backup_log; }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $enabled_modes = 0;

    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;

    if($opt->{backup})
    {
        $backup = 1;
        ++$enabled_modes;
    }

    if($opt->{log})
    {
        $ls_backup = 1;
        ++$enabled_modes;
    }

    if($opt->{full_backup})
    {
        $full_backup = 1;
        ++$enabled_modes;
    }

    if($enabled_modes > 1 || $verbose)
    {
        print colored ("Using backup location: $ScribbleWibble::backup_location", 'bright_green'), "\n";
        if($enabled_modes gt 1)
        {
            print "Please choose only one option out of [ --backup | --full_backup ]\n";
            print "❌ Error: Multiple conflicting options.\n";
            exit(0);
        }

        if($backup == 1)      { print colored ("--backup     : ENABLED", 'bright_green'), "\n"; } else { print colored ("--backup     : DISABLED", 'bright_red'), "\n"; }
        if($full_backup == 1) { print colored ("--full-backup: ENABLED", 'bright_green'), "\n"; } else { print colored ("--full-backup: DISABLED", 'bright_red'), "\n"; }
        if($ls_backup == 1)   { print colored ("--log        : ENABLED", 'bright_green'), "\n"; } else { print colored ("--log        : DISABLED", 'bright_red'), "\n"; }
    }

    if($enabled_modes == 0) { print "No action specified, defaulting to --log.\n\n"; $ls_backup = 1; }

    dispatch_backup();
}

1;
