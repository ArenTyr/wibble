# ABSTRACT: Add / work with quicknote entry / entries
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)

package ScribbleWibble::Command::quicknote;
use ScribbleWibble -command;
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use ScribbleWibble::ui_elems qw ( print_hl print_hl_heavy padded_number );

use strict;
use warnings;

use Data::Dumper;
use Exporter 'import';
use File::Basename;
use File::Path;
use POSIX qw(strftime);
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;

our @EXPORT_OK = qw ( get_special_wrapper_start get_special_wrapper_end inbox_section_header
                      dump_or_edit_inbox add_inbox_section view_inbox_section check_inbox_init
                      extern_qn_set_state );

use constant END_HEADER   => "[<-<-<-<-<-------------------------------------------------<-<-<-<-<]\n";
use constant HEADER_WIDTH => 67;
use constant PADDING => 6;
use constant START_STUB_L => "[>->->->->------------ [ ";
use constant START_STUB_R => " ] ------------>->->->->]\n";

my $add_inbox = 0;
my $alt_file = 0;
my $custom_file;
my $del_inbox = 0;
my $dump_inbox = 0;
my $edit_inbox = 0;
my $ls_files = 0;
my $ls_inbox = 0;
my $view_inbox = 0;
my $use_gui = 0;
my $verbose;

my $qn_note_file = "";
my $qn_note_dir = "";
my $qn_type = "quicknote";
my $qn_type_uc = "Quicknote";

sub opt_spec
{
    return (
        ["add|A"     => "Add/append to default quicknote file"],
        ["delete|X"  => "Delete quicknote entry"],
        ["dir|R"     => "List available/initialised quicknote files"],
        ["dump|D"    => "Dump entire quicknote file"],
        ["edit|E"    => "Edit quicknote file"],
        ["file|F"    => "Use alternate quicknote file instead of default file"],
        ["guiedit|G" => "Add/edit to quicknote file with GUI editor instead of regular editor"],
        ["ls|L"      => "List all quicknote entries"],
        ["view|W"    => "View quicknote entry"],
    );
}

sub description
{
    return "Rapid-fire quicknotes/INBOX files.\n";
}

# Effectively acts as "constructor" to re-use functionality for journal-mode
sub extern_qn_set_state
{
    my ($extern_dir,      $extern_file,        $extern_type,    $extern_type_uc,
        $extern_alt_file, $extern_custom_file, $extern_use_gui, $extern_ls_only,
        $extern_verbose,  $extern_edit,        $extern_dump,    $extern_view) = @_;

    $qn_note_dir  = $extern_dir;
    $qn_note_file = $extern_file;
    $qn_type      = $extern_type;
    $qn_type_uc   = $extern_type_uc;
    $alt_file     = $extern_alt_file;
    $custom_file  = $extern_custom_file;
    $use_gui      = $extern_use_gui;
    $ls_inbox     = $extern_ls_only;
    $verbose      = $extern_verbose;
    $edit_inbox   = $extern_edit;
    $dump_inbox   = $extern_dump;
    $view_inbox   = $extern_view;
}

sub ls_qn_files
{
    print "Available/initialised quicknote files:\n\n";
    system("ls -1 $qn_note_dir");
    exit(0);
}

sub get_special_wrapper_start
{
    my ($wrapper) = @_;
    if($wrapper eq "org")  { return "\n#+begin_example\n"; }
    if($wrapper eq "md")   { return "\n```\n"; }
    if($wrapper eq "adoc") { return "\n....\n"; }
    else { return "\n"; }
}

sub get_special_wrapper_end
{
    my ($wrapper) = @_;
    if($wrapper eq "org")  { return "#+end_example\n\n"; }
    if($wrapper eq "md")   { return "```\n\n"; }
    if($wrapper eq "adoc") { return "....\n\n"; }
    else { return "\n"; }
}

sub inbox_section_header
{
    my ($desc, $wrapper) = @_;
    my $usable_width = HEADER_WIDTH - PADDING;
    if((length $desc) > $usable_width)
    { $desc = substr $desc, 0, ($usable_width + 2); }
    my $padding = (HEADER_WIDTH - 4) - (length $desc);
    $desc .= " " x $padding;
    my $current_datetime = strftime "%Y-%m-%d %H:%M:%S", localtime;
    my $header = get_special_wrapper_start($wrapper);
    $header   .= START_STUB_L . $current_datetime . START_STUB_R;
    $header   .= "[= " . $desc . " =]\n";
    $header   .= END_HEADER;
    $header   .= get_special_wrapper_end($wrapper) . "\n";
    return $header;
}

sub dump_or_edit_inbox
{
    if($dump_inbox)
    {
        #print_hl; print "\n";
        #print colored("INBOX contents:", 'bright_green') . "\n";
        print_hl_heavy;
        if ($alt_file) { system("cat $qn_note_dir/$custom_file"); }
        else           { system("cat $qn_note_file\n"); }
        print_hl_heavy;
    }
    elsif($edit_inbox)
    {
        if($use_gui)
        {
            if ($alt_file) { system("$ScribbleWibble::gui_editor $qn_note_dir/$custom_file"); }
            else           { system("$ScribbleWibble::gui_editor $qn_note_file");               }
        }
        else
        {
            if ($alt_file) { system("$ScribbleWibble::editor $qn_note_dir/$custom_file");     }
            else           { system("$ScribbleWibble::editor $qn_note_file");                   }
        }
    }
}

sub add_inbox_section
{
    print "Enter description for section:\n> ";
    my $desc = <STDIN>;
    chomp($desc);
    $desc =~ s/^\s+|\s+$//g;
    if(! length $desc) { $desc = "NO DESCRIPTION"; }

    my @exts = qw(.org .md .adoc);
    my $name; my $dir; my $ext;
    if ($alt_file) { ($name, $dir, $ext) = fileparse("$qn_note_dir/$custom_file", @exts); }
    else           { ($name, $dir, $ext) = fileparse("$qn_note_file", @exts); }

    my $special_wrapper = "none";

    if($ext eq ".org")  { $special_wrapper = "org";  }
    if($ext eq ".md")   { $special_wrapper = "md";   }
    if($ext eq ".adoc") { $special_wrapper = "adoc"; }

    my $file_handle;
    if($alt_file) { open ($file_handle, '>>', "$qn_note_dir/$custom_file")
                    or die "$qn_note_dir/$custom_file"; }
    else          { open ($file_handle, '>>', "$qn_note_file") or die "$!: $qn_note_file"; }
    print $file_handle inbox_section_header($desc, $special_wrapper);
    close($file_handle);

    $edit_inbox = 1; $dump_inbox = 0;
    dump_or_edit_inbox;
}

sub view_inbox_section
{
    my $file_handle;
    if($alt_file) { open ($file_handle, '<', "$qn_note_dir/$custom_file",)
                    or die "$!: $qn_note_dir/$custom_file"; }
    else          { open ($file_handle, '<', "$qn_note_file") or die "$!: $qn_note_file"; }
    my @inbox_file = <$file_handle>;
    close($file_handle);

    my %entries;
    my @inbox_entries;
    my $header_index = 0;
    my $header_entry;
    my $header_date;
    my @header_subtr;
    my @header_date_bg;
    my @header_date_ed;
    my @last_entry;
    my %last_inbox_entry;
    my $content_ready = 0;
    my $desc_line;
    my $entry_count = 0;
    my $pad_count = 0;

    # To prettify padding, quickly scan through to see how many entries there are
    for (my $line_num = 0; $line_num < @inbox_file; ++$line_num)
    {
        if ($inbox_file[$line_num] =~ /^\Q[>->->->->------------ [/)
        { ++$pad_count; }
    }

    for (my $line_num = 0; $line_num < @inbox_file; ++$line_num)
    {
        if ($inbox_file[$line_num] =~ /^\Q[>->->->->------------ [/)
        {
            ++$header_index;

            # 1. Extract out the header date to create a pretty header entry
            @header_date_bg = split /\[/, $inbox_file[$line_num];
            @header_date_ed = split /\]/, "$header_date_bg[2]";
            $header_date = $header_date_ed[0];
            my $padded_index = padded_number($pad_count, $header_index);
            #$header_entry = "[$header_index] $header_date";
            $header_entry = "[$padded_index] $header_date";

            # Skip onto next line (description)
            ++$line_num;
        }
        # Greedily slurp up to next header
        else { next; }


        # Array to store entry content/lines
        # Hash to store key-values to index this INBOX entry
        my @entry_lines = ();
        my %inbox_entry;

        CONTENT: while($line_num < @inbox_file && (defined $inbox_file[$line_num]))
        {

            # 2. Extract out the description line
            if($inbox_file[$line_num] =~ /^\Q[= /)
            {
                $desc_line = ($inbox_file[$line_num] =~ /^\[=(.*)=\]$/)[0];
                $desc_line  =~ s/^\s+|\s+$//g ;

                # Move onto content
                ++$line_num; ++$line_num;

                # Skip markup wrappers (if they exist)
                if($inbox_file[$line_num] =~ /^\Q#+end_example/) { ++$line_num; }
                if($inbox_file[$line_num] =~ /^\Q```/)           { ++$line_num; }
                if($inbox_file[$line_num] =~ /^\Q..../)          { ++$line_num; }
            }

            push(@entry_lines, $inbox_file[$line_num]);
            ++$line_num;

            # Indicate that we have new content to add, having parsed a content block
            $content_ready = 1;

            # Break condition - exit if the NEXT line is a new day section
            if(defined $inbox_file[$line_num + 1])
            {
                last CONTENT if ($inbox_file[$line_num + 1] =~ /^\Q[>->->->->------------ [/);
            }
        }

        if($content_ready)
        {
            # Add the new entry to our hash
            $inbox_entry{"date"} = $header_entry;
            $inbox_entry{"desc"} = $desc_line;
            $inbox_entry{"content"} = \@entry_lines;
            # Push the hash onto our array of entries
            push(@inbox_entries, \%inbox_entry);
            ++$entry_count;

            # reset to indicate no entry to add (yet)
            $content_ready = 0;
        }
    }

    if($view_inbox)
    {
        # Display results to the user
        print "\nPlease select $qn_type entry to view:\n\n";
    }
    elsif($del_inbox)
    {
        print "\nPlease select $qn_type entry to DELETE:\n\n";
    }

    if((scalar @inbox_entries) == 0 )
    { print colored("Empty $qn_type file. Add [--add] an entry to begin.", 'bright_red'), "\n"; }
    else
    {
        for my $entry (@inbox_entries)
        {
            print colored ("$entry->{date}", 'bright_yellow');
            print "[ $entry->{desc} ]\n";
        }
    }

    if($ls_inbox) { exit(0); }

    if($view_inbox)
    {
        print "\n> ";
        my $response = <STDIN>;
        chomp($response);
        if(looks_like_number($response) && $response >= 1 && $response <= @inbox_entries)
        {
            print_hl_heavy;
            my $content_array = $inbox_entries[($response - 1)]->{content};
            print "@$content_array" .  "\n";
            print_hl_heavy;
        }
    }
    elsif($del_inbox)
    {
        print "\n> ";
        my $response = <STDIN>;
        chomp($response);

        if(looks_like_number($response) && $response >= 1 && $response <= @inbox_entries)
        {
            print "Really DELETE " . $inbox_entries[$response - 1]->{date} . "?\nEnter 'delete' to confirm.\n> ";
            my $confirm = <STDIN>;
            chomp($confirm);
            if($confirm ne "delete") { print "Aborting.\n"; exit(0); }

            delete $inbox_entries[($response - 1)];

            my @exts = qw(.org .md .adoc);
            my $name; my $dir; my $ext;
            if($alt_file) { ($name, $dir, $ext) = fileparse("$qn_note_dir/$custom_file", @exts); }
            else          { ($name, $dir, $ext) = fileparse("$qn_note_file", @exts); }

            my $special_wrapper = "none";

            if($ext eq ".org")  { $special_wrapper = "org";  }
            if($ext eq ".md")   { $special_wrapper = "md";   }
            if($ext eq ".adoc") { $special_wrapper = "adoc"; }

            my $file_handle;
            if($alt_file) { open ($file_handle, '>',  "$qn_note_dir/$custom_file")
                            or die "$!:  $qn_note_dir/$custom_file"; }
            else          { open ($file_handle, '>', $qn_note_file) or die "$!: $qn_note_file"; }

            # Rebuild a new replacement quicknote file
            foreach my $entry (@inbox_entries)
            {
                if(! defined $entry) { next; }

                # Opening wrapper for markup formats
                if($special_wrapper eq "org") { print $file_handle "#+begin_example\n"; }
                elsif($special_wrapper eq "md")  { print $file_handle "```\n"; }
                elsif($special_wrapper eq "adoc")  { print $file_handle "....\n"; }

                # Extract date & desc
                my @date_elem = split / /, $entry->{date};
                shift @date_elem; shift @date_elem;
                my $restored_date = "@{date_elem}";
                my $padding = (HEADER_WIDTH - 4) - (length $entry->{desc});
                print $file_handle START_STUB_L . $restored_date . START_STUB_R;
                print $file_handle "[= " . $entry->{desc} . " " x $padding . " =]\n";
                print $file_handle END_HEADER;

                # Closing wrapper
                if($special_wrapper eq "org") { print $file_handle "#+end_example\n"; }
                elsif($special_wrapper eq "md")  { print $file_handle "```\n"; }
                elsif($special_wrapper eq "adoc")  { print $file_handle "....\n"; }

                # Write out the entry content
                my $content_array = $entry->{content};
                print $file_handle "@$content_array";
            }

            close($file_handle);
            print colored("Entry [" . $response . "] deleted.", 'bright_red'), "\n";
        }

    }
}

sub dispatch_inbox
{
    if    ($add_inbox)  { add_inbox_section;  }
    elsif ($del_inbox)  { view_inbox_section; }
    elsif ($ls_files)   { ls_qn_files;        }
    elsif ($ls_inbox)   { view_inbox_section; }
    elsif ($view_inbox) { view_inbox_section; }
    elsif ($edit_inbox) { dump_or_edit_inbox; }
    elsif ($dump_inbox) { dump_or_edit_inbox; }
    else
    {
        print "Unknown option specified. Please select one of '--add', '--edit', '--delete', '--dir', '--dump', "
              . "'--ls', or '--view'.\n";
    }
}

sub check_inbox_init
{

    unless(-d "$qn_note_dir")
    {
        print colored("No $qn_type directory present, initialising at $qn_note_dir.\n", 'bright_red') . "\n";
        mkpath("$qn_note_dir");
    }

    unless(-f "$qn_note_file")
    {
        print colored("No default $qn_type file present, initialising at $qn_note_file.\n", 'bright_red') . "\n";
        system("touch $qn_note_file");
    }

    if($alt_file)
    {
        unless(-f  "$qn_note_dir/$custom_file")
        {
            print colored("Custom $qn_type file note NOT present, initialising at $qn_note_dir/$custom_file", 'bright_red') . "\n";
            system("touch $qn_note_dir/$custom_file");
        }
    }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $enabled_modes = 0;
    my $inbox_arg = "";

    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;

    $qn_note_dir  = $ScribbleWibble::qn_dir;
    $qn_note_file = $ScribbleWibble::qn_file;


    if($opt->guiedit)   { $use_gui = 1;  }
    if($opt->file)
    {
        $alt_file = 1;
        if(scalar @$args == 0)
        {
            print "No custom $qn_type filename supplied. Exiting.\n";
            exit(1);
        }
        else
        {
            $custom_file = "@$args";
            $custom_file =~ tr/ /_/;
            chomp($custom_file);
        }

    }

    # mode options
    if($opt->add)       { $add_inbox = 1;  ++$enabled_modes; }
    if($opt->delete)    { $del_inbox = 1;  ++$enabled_modes; }
    if($opt->dir)       { $ls_files = 1;   ++$enabled_modes; }
    if($opt->dump)      { $dump_inbox = 1; ++$enabled_modes; }
    if($opt->edit)      { $edit_inbox = 1; ++$enabled_modes; }
    if($opt->ls)        { $ls_inbox = 1;   ++$enabled_modes; }
    if($opt->view)      { $view_inbox = 1; ++$enabled_modes; }

    check_inbox_init;

    if($enabled_modes gt 1 || $verbose)
    {

        if(! $alt_file) { print colored ("Using $qn_type file: $qn_note_file", 'bright_green'), "\n"; }
        else            { print colored ("Using CUSTOM $qn_type file: $qn_note_dir/$custom_file", 'bright_red'), "\n"; }

        if($enabled_modes gt 1)
        {
            print "Please choose only one option out of [ --add | --ls | --view | --dump ]\n";
            print "❌ Error: Multiple conflicting options.\n";
            exit(0);
        }

        if($add_inbox eq 1)  { print colored ("--add    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--add    : DISABLED", 'bright_red'), "\n"; }
        if($del_inbox eq 1)  { print colored ("--delete : ENABLED", 'bright_green'), "\n"; } else { print colored ("--delete : DISABLED", 'bright_red'), "\n"; }
        if($dump_inbox eq 1) { print colored ("--dump   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--dump   : DISABLED", 'bright_red'), "\n"; }
        if($edit_inbox eq 1) { print colored ("--edit   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--edit   : DISABLED", 'bright_red'), "\n"; }
        if($ls_files eq 1)   { print colored ("--dir    : ENABLED", 'bright_green'), "\n"; } else { print colored ("--dir    : DISABLED", 'bright_red'), "\n"; }
        if($ls_inbox eq 1)   { print colored ("--ls     : ENABLED", 'bright_green'), "\n"; } else { print colored ("--ls     : DISABLED", 'bright_red'), "\n"; }
        if($view_inbox eq 1) { print colored ("--view   : ENABLED", 'bright_green'), "\n"; } else { print colored ("--view   : DISABLED", 'bright_red'), "\n"; }
    }

    dispatch_inbox;
}

1;
