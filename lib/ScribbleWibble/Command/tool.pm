# ABSTRACT: Helper utilities for working with notes
# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::Command::tool;
use ScribbleWibble -command;
#use base 'App::Cmd::Command';
use ScribbleWibble::common_funcs qw ( set_operation_mode );
use ScribbleWibble::Command::search qw ( check_verbose check_config_override set_config_file );
use Term::ANSIColor;

use Exporter 'import';
use JSON::XS;
use Data::Dumper;

use warnings;
use strict;

our @EXPORT_OK = qw ( render_stats );

my $op_mode;
my $verbose;

sub opt_spec
{
    return (
        ["rg-title|T"      => "Use ripgrep to display all note titles"],
        ["rg-tags|A"       => "Use ripgrep to display note tags + count"],
        ["rg-type|F"       => "Use ripgrep to display note filetypes + count"],
        ["archived|K"      => "Operate on archived notes instead"],
        ["build-aliases|S" => "Output a set of shell aliases to STDOUT"],
        ["stats|Z"         => "Show statistics"],
        ["clean|L"         => "Prune any empty directories from storage directories"],
    );
}

sub description
{
    return "\nVarious helper functions/utilities to enhance Wibble\n"
         . "\n"
         . "Usage: wibble tool [--rg-title|T] [--rg-tags|A] [--rg-type|F]\n"
         . "                   [--archived|K] [--build-aliases|S]\n"
         . "                   [--stats|Z]    [--clean|L]\n"
         . "\n\n"
         . " - Display sorted listing of all note titles, tags, or filetypes\n"
         . "\n"
         . " - Display note statistics\n"
         . "\n"
         . " - Build bash shell aliases\n"
         . "\n"
         . " - Check for/purge any empty directories in note tree\n";
}

sub build_aliases
{
    print colored("This will help you build a convenient set of shell aliases, that you can "
                 ."save/copy into a file and then source/import into your bash environment.",
                  'bright_green'), "\n";
    
    print "Enter a convenient/SHORT prefix for command (e.g. 'note', 'wib', 'qn'). This will form the base "
        ."prefix for all of the commands. For example, if you enter 'note' (without quotes!), you will get "
        . "'note-search', 'note-tool', etc.\n";
    my $term = new Term::ReadLine 'Path Getter';
    my $prefix = $term->readline("Enter prefix (default = \"wib\") > ", "wib");
    chomp($prefix);
    $prefix =~ s/ /_/g;
    print "\n\n";
    print "OK. Here is your set of alias commands.\nSave the contents below the line to a file and then source "
        . "it somewhere into your bashrc/bash environment.\n";
    print "You can then rapidly bring up the commands by typing your prefix and mashing <TAB> to generate "
        . "completion candidates\n";

    print "\n\n------------------------------------------------\n\n";
    print "#!/bin/bash\n";
    print "\n";
    print "# Wibble helper functions\n";
    print "function _wib_id_dd_to_clipboard()\n";
    print "{\n";
    print "    wibble search --show-dd-dir -1 --id \$1\n";
    print "    echo \"\"\n";
    print "    xclip -r -selection clipboard <<< \$(wibble search --show-dd-dir -1 --id \$1)\n";
    print "}\n\n";
    print "function _wib_id_note_path_to_clipboard()\n";
    print "{\n";
    print "    wibble search --bare -1 --id \$1\n";
    print "    echo \"\"\n";
    print "    xclip -r -selection clipboard <<< \$(wibble search --bare -1 --id \$1)\n";
    print "}\n\n";
    print "function _wib_dd_to_clipboard()\n";
    print "{\n";
    print "    wibble search --show-dd-dir -T \$1\n";
    print "    echo \"\"\n";
    print "    xclip -r -selection clipboard <<< \$(wibble search --show-dd-dir -T \$1)\n";
    print "}\n\n";
    print "function _wib_note_path_to_clipboard()\n";
    print "{\n";
    print "    wibble search --bare -T \$1\n";
    print "    echo \"\"\n";
    print "    xclip -r -selection clipboard <<< \$(wibble search --bare -T \$1)\n";
    print "}\n\n";
    print "# Wibble aliases\n";
    print "\n";
    print "# General \n";
    print "alias $prefix-new='wibble create'\n";
    print "\n# Searching functionality: searcy by date, id, title, etc...\n";
    print "alias $prefix-search='wibble search'\n";
    print "alias $prefix-search-date='wibble search -?'\n";
    print "alias $prefix-search-id='wibble search -9'\n";
    print "alias $prefix-search-desc='wibble search -D'\n";
    print "alias $prefix-search-proj='wibble search -P'\n";
    print "alias $prefix-search-tags='wibble search -A'\n";
    print "alias $prefix-search-title='wibble search -T'\n";
    print "alias $prefix-search-filetype='wibble search -Y'\n";
    print "alias $prefix-search-class='wibble search -C'\n";
    print "alias $prefix-search-keypairs='wibble search -S'\n";
    print "alias $prefix-search-custom='wibble search --custom'\n";
    print "alias $prefix-search-linkids='wibble search --linkids'\n";
    print "alias $prefix-search-menu='wibble search --menu'\n";
    print "\n# Default search field is 'title'\n";
    print "# Edit lines below if you want to default to another field\n";
    print "# Or use --menu instead if you want to trigger full menu search each time\n\n";
    print "# View metadata and related note information\n";
    print "alias $prefix-view-metadata='wibble search -MT'\n";
    print "alias $prefix-view-data-dir='wibble search --ls-dd -T'\n";
    print "alias $prefix-view-data-show-path='wibble search --show-dd-dir -T'\n";
    print "alias $prefix-view-linked-files='wibble search --ls-linked -T'\n";
    print "alias $prefix-view-related='wibble search -RT'\n";
    print "\n# Edit metadata and related note information\n";
    print "alias $prefix-edit-metadata='wibble search --edit-md -T'\n";
    print "alias $prefix-edit-linked='wibble search --edit-linked -T'\n";
    print "alias $prefix-edit-related='wibble search --associate -T'\n";
    print "\n# Add data/linked files operations\n";
    print "alias $prefix-add-data='wibble search -\@T'\n";
    print "alias $prefix-add-linked-file='wibble search -%T'\n";
    print "\n# Other data directory operations\n";
    print "alias $prefix-open-dd='wibble search -OT'\n";
    print "alias $prefix-open-aa='wibble search --open-aa -T'\n";
    print "\n# Archival operations\n";
    print "alias $prefix-mv-archive='wibble search --to-archive -T'\n";
    print "alias $prefix-mv-unarchive='wibble search --from-archive -T'\n";
    print "\n# Dump notes to stdout\n";
    print "alias $prefix-cat-content='wibble search -2T'\n";
    print "alias $prefix-cat-both='wibble search -3T'\n";
    print "\n\n# Task management\n";
    print "alias $prefix-task-add='wibble task --add'\n";
    print "alias $prefix-task-done='wibble task --mark'\n";
    print "alias $prefix-task-view='wibble task --ls'\n";
    print "\n\n# Journal\n";
    print "alias $prefix-journal-add='wibble journal --add'\n";
    print "alias $prefix-journal-files='wibble journal --dir'\n";
    print "alias $prefix-journal-dump='wibble journal --dump'\n";
    print "alias $prefix-journal-ls='wibble journal --ls'\n";
    print "alias $prefix-journal-view='wibble journal --view'\n";
    print "\n\n# Quicknotes\n";
    print "alias $prefix-qn-add='wibble quicknote --add'\n";
    print "alias $prefix-qn-delete='wibble quicknote --delete'\n";
    print "alias $prefix-qn-files='wibble quickote --dir'\n";
    print "alias $prefix-qn-dump='wibble quicknote --dump'\n";
    print "alias $prefix-qn-ls='wibble quicknote --ls'\n";
    print "alias $prefix-qn-view='wibble quicknote --view'\n";
    print "\n\n# Copying helpers\n";
    print "alias $prefix-copy-data-dir-path='_wib_id_dd_to_clipboard'\n";
    print "alias $prefix-copy-note-path='_wib_id_note_path_to_clipboard'\n";
    print "alias $prefix-copy-title-data-dir-path='_wib_dd_to_clipboard'\n";
    print "alias $prefix-copy-title-note-path='_wib_note_path_to_clipboard'\n";
    print "\n\n# All help\n";
    print "alias $prefix-help='wibble help create; echo \"\"; echo \"===============\"; echo \"\"; "
        . "wibble help delete; echo \"\"; echo \"===============\"; echo \"\"; "
        . "wibble help search; echo \"\"; echo \"===============\"; echo \"\"; wibble help task; "
        . "echo \"\"; echo \"===============\"; echo \"\"; wibble help journal; "
        . "echo \"\"; echo \"===============\"; echo \"\"; wibble help quicknote; "
        . "echo \"\"; echo \"===============\"; echo \"\"; wibble help tool'\n";
    
}

sub render_tag_statistics
{
    my($to_json, $db) = @_;

    my $rg_tags_cmd          = "rg --color never -Ne \"^Tags:(.*)\$\" $db | rg -v \"___NULL___\"";
    $rg_tags_cmd            .= " | cut -d ':' -f 2- | cut -c 2- | tr ' ' '\\n' | sort | uniq -c | sort -nr";

    if($to_json)
    {
        my $raw_counts = `$rg_tags_cmd`;
        my @tag_results = split /\n/, $raw_counts;
        my @tag_counts;
        my @tag_labels;
        
        foreach(@tag_results)
        {
            my $clean_line = $_;
            $clean_line =~ s/^\s+|\s+$//g;
            my $num_count = (split / /, $clean_line)[0] + 0;
            my $tag_label = (split / /, $clean_line)[1];

            push @tag_counts, $num_count;
            push @tag_labels, $tag_label;
        }

        my $tag_data =
        {
            'tag_counts' => \@tag_counts,
            'tag_labels' => \@tag_labels
        };

        return $tag_data;
    }
    else
    {
        system($rg_tags_cmd);
    }
}

sub render_stats
{
    my ($to_json, $db, $extern_op_mode) = @_;

    # edge case, really - trying to get stats but they haven't yet created any notes...
    if(! -e "$ScribbleWibble::wibble_store/notes")
    {
        if($to_json)
        {
            return 'wibble_data' =>
            {
                'storage_stats' =>
                {
                    'main_note_storage'  => 'NONE',
                    'ark_note_storage'  => 'NONE',
                    'note_data_storage' => 'NONE',
                    'base_directory'    => 'NONE',
                    'main_db'           => 'NONE',
                    'ark_db'            => 'NONE'
                },
                'file_count_labels' =>
                [
                    'Note Count',
                    'Archived Note Count',
                    'Data Files',
                    'Journal Files',
                    'Quicknote Files',
                    'Template Files',
                    'Schema Files'
                ],
                'file_count_nums' =>
                [
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0'
                ],
                'tag_data' =>
                {
                    'tag_counts' => [ '0' ],
                    'tag_labels' => [ 'No notes in database.' ]
                }
            };
        }
        else
        {
            print colored("No notes detected. Please create at least one note with 'wibble create' first.\n", 'bright_red', 'bold');
            exit 1;
        }
    }

    $op_mode = $extern_op_mode;
    my $stats_store_main_cmd = "du -Lhs $ScribbleWibble::wibble_store/notes | cut -f 1";
    my $stats_store_ak_cmd   = "du -Lhs $ScribbleWibble::wibble_store/archived_notes | cut -f 1";
    my $stats_store_data_cmd = "du -Lhs $ScribbleWibble::wibble_store/note_data | cut -f 1";
    my $stats_db_cmd         = "du -h $ScribbleWibble::db_location | cut -f 1";
    my $stats_db_ak_cmd      = "du -h $ScribbleWibble::archived_db_location | cut -f 1";
    my $note_count;
    my $note_ak_count;
    if($op_mode eq "RIPGREP")
    {
        $note_count          = "rg \"^NoteId\" $ScribbleWibble::db_location | wc -l";
        $note_ak_count       = "rg \"^NoteId\" $ScribbleWibble::archived_db_location | wc -l";
    }
    elsif($op_mode eq "RECUTILS")
    {
        $note_count          = "recinf $ScribbleWibble::db_location | cut -f 1 -d ' '";
        $note_ak_count       = "recinf $ScribbleWibble::archived_db_location | cut -f 1 -d ' '";
    }
    else
    {
        print "Unknown mode '$op_mode'. Aborting.\n";
        exit 1;
    }
    my $data_file_count      = "find $ScribbleWibble::wibble_store/note_data -type f,l -printf x | wc -c";
    my $journal_file_count   = "find $ScribbleWibble::wibble_store/journal -type f,l -printf x | wc -c";
    my $quicknote_file_count = "find $ScribbleWibble::wibble_store/quicknotes -type f,l -printf x | wc -c";
    my $template_file_count  = "find $ScribbleWibble::wibble_store/templates -type f,l -printf x | wc -c";
    my $schema_file_count    = "find $ScribbleWibble::wibble_store/schemas -type f,l -printf x | wc -c";

    my $ds_stats_store_main_cmd = `$stats_store_main_cmd`;
    my $ds_stats_store_ak_cmd   = `$stats_store_ak_cmd`;
    my $ds_stats_store_data_cmd = `$stats_store_data_cmd`;
    my $ds_stats_db_cmd         = `$stats_db_cmd`;
    my $ds_stats_db_ak_cmd      = `$stats_db_ak_cmd`;
    my $ds_note_count           = `$note_count`;
    my $ds_note_ak_count        = `$note_ak_count`;
    my $ds_data_file_count      = `$data_file_count`;
    my $ds_journal_file_count   = `$journal_file_count`;
    my $ds_quicknote_file_count = `$quicknote_file_count`;
    my $ds_template_file_count  = `$template_file_count`;
    my $ds_schema_file_count    = `$schema_file_count`;

    chomp($ds_stats_store_main_cmd);
    chomp($ds_stats_store_ak_cmd);
    chomp($ds_stats_store_data_cmd);
    chomp($ds_stats_db_cmd);
    chomp($ds_stats_db_ak_cmd);
    chomp($ds_note_count);
    chomp($ds_note_ak_count);
    chomp($ds_data_file_count);
    chomp($ds_journal_file_count);
    chomp($ds_quicknote_file_count);
    chomp($ds_template_file_count);
    chomp($ds_schema_file_count);

    
    if($to_json)
    {
        my $tag_results; 
        if($op_mode eq "RECUTILS")
        {
            $tag_results =
            {
                'tag_counts' => [ '1' ],
                'tag_labels' => [ 'Error. Ripgrep mode required for tag stats.' ]
            };
        }
        else 
        {
            $tag_results = render_tag_statistics(1, $db);
        }
        
        my $stats_payload = 
        {
            'wibble_data' =>
            { 
                'storage_stats' =>
                {
                    'main_note_storage' => $ds_stats_store_main_cmd,
                    'ark_note_storage'  => $ds_stats_store_ak_cmd,
                    'note_data_storage' => $ds_stats_store_data_cmd,
                    'base_directory'    => $ScribbleWibble::wibble_store,
                    'main_db'           => $ScribbleWibble::db_location,
                    'ark_db'            => $ScribbleWibble::archived_db_location
                },
                'file_count_labels' =>
                [
                    'Note Count',
                    'Archived Note Count',
                    'Data Files',
                    'Journal Files',
                    'Quicknote Files',
                    'Template Files',
                    'Schema Files'
                ],
                'file_count_nums' =>
                [
                    $ds_note_count,
                    $ds_note_ak_count,
                    $ds_data_file_count,
                    $ds_journal_file_count,
                    $ds_quicknote_file_count,
                    $ds_template_file_count,
                    $ds_schema_file_count
                ],
                    'tag_data' => $tag_results
            }
        };

        return $stats_payload;
    }
    else
    {
        print "\n\n Statistics\n";
        print "========================================================\n";
        print colored(" Main note storage                           ", 'bright_yellow', 'bold');
        print "$ds_stats_store_main_cmd\n";
        print colored(" Archived note storage                       ", 'bright_yellow', 'bold');
        print "$ds_stats_store_ak_cmd\n";
        print colored(" Note data storage                           ", 'bright_yellow', 'bold');
        print "$ds_stats_store_data_cmd\n";
        print "--------------------------------------------------------\n";
        print colored(" Main note database size                     ", 'bright_yellow', 'bold');
        print "$ds_stats_db_cmd\n";
        print colored(" Archived note database size                 ", 'bright_yellow', 'bold');
        print "$ds_stats_db_ak_cmd\n";
        print "--------------------------------------------------------\n";
        print colored(" Number of notes                             ", 'bright_yellow', 'bold');
        print "$ds_note_count\n";
        print colored(" Number of archived notes                    ", 'bright_yellow', 'bold');
        print "$ds_note_ak_count\n";
        print colored(" Number of data files                        ", 'bright_yellow', 'bold');
        print "$ds_data_file_count\n";
        print colored(" Number of journal files                     ", 'bright_yellow', 'bold');
        print "$ds_journal_file_count\n";
        print colored(" Number of quicknote files                   ", 'bright_yellow', 'bold');
        print "$ds_quicknote_file_count\n";
        print colored(" Number of template definitions              ", 'bright_yellow', 'bold');
        print "$ds_template_file_count\n";
        print colored(" Number of schema definitions                ", 'bright_yellow', 'bold');
        print "$ds_schema_file_count\n";
        print "--------------------------------------------------------\n";
        print colored(" Note base directory     ", 'bright_yellow', 'bold');
        print " $ScribbleWibble::wibble_store\n";
        print colored(" Note main database      ", 'bright_yellow', 'bold');
        print " $ScribbleWibble::db_location\n";
        print colored(" Note archived database  ", 'bright_yellow', 'bold');
        print " $ScribbleWibble::archived_db_location\n";
        print "========================================================\n";
    }
}

sub execute
{
    $verbose = check_verbose(@_);
    my ($self, $opt, $args) = @_;
    my $db = "";

    my $config_override = check_config_override(@_);
    if($config_override)
    {
        my $conf = shift @{$args};
        set_config_file($conf);
    }

    $self->app->read_config_file;
    $op_mode = set_operation_mode($verbose);

    if(! $opt->{archived}) { $db = "$ScribbleWibble::db_location"; }
    else{ $db = "$ScribbleWibble::archived_db_location"; }

#    render_stats(1, $db, $op_mode);
 #   exit 0;

    if($opt->{rg_title})
    {
        my $rg_title_cmd         = "rg --color never -Ne \"^Title:(.*)\$\" $db | rg -v \"___NULL___\"";
        $rg_title_cmd           .= " | cut -d ':' -f 2- | cut -c 2- | sort";
        system($rg_title_cmd);
    }
    elsif($opt->{rg_tags})
    {
        render_tag_statistics(0, $db);
    }
    elsif($opt->{rg_type})
    {
        my $rg_type_cmd          = "rg --color never -Ne \"^Filetype:(.*)\$\" $db | rg -v \"___NULL___\"";
        $rg_type_cmd            .= " | cut -d ':' -f 2- | cut -c 2- | tr ' ' '\n' | sort | uniq -c | sort -nr";
        system($rg_type_cmd);
    }
    elsif($opt->{stats})
    {
        render_stats(0, $db, $op_mode);
    }
    elsif($opt->{clean})
    {
        print "Empty directories:\n";
        print "----------------------------\n";
        system("find $ScribbleWibble::wibble_store -type d -empty -print");
        print "----------------------------\n";
        print "Clean up storage under $ScribbleWibble::wibble_store? (y/n)\n> ";
        my $response = <STDIN>;
        chomp $response;
        if($response eq "y")
        {
            system("find $ScribbleWibble::wibble_store -type d -empty -delete");
            print "Cleaned.\n";
        }
    }
    elsif($opt->{build_aliases})
    {
        build_aliases;
    }
    else
    {
        print description;
    }
}

1;
