# TODO Implement POD documentation section
# Author : Aren Tyr
# License: GPL v3 (Do what thou wilt...)
package ScribbleWibble::common_funcs;

#use ScribbleWibble::rg_search qw ( rg_update_entire_record );

use Data::Dumper;
use Exporter 'import';
use File::Copy;
use File::Path;
use Term::ANSIColor;

use strict;
use warnings;

our @EXPORT_OK = qw ( open_file_ro open_file_handle_append open_file_handle_CLOBBER
                      parameter_check set_operation_mode make_tmp_db_backup update_record_metadata );

sub open_file_ro
{
    my ($filename) = @_;
    $filename =~ s/~/$ENV{HOME}/g;
    open (my $file_handle, '<', "$filename") or die "$!: $filename";
    my @file = <$file_handle>;
    close($file_handle);
    return \@file;
}

sub open_file_rw
{
    my ($filename) = @_;
    $filename =~ s/~/$ENV{HOME}/g;
    open (my $file_handle, '+<', "$filename") or die "$!: $filename";
    my @file = <$file_handle>;
    close($file_handle);
    return \@file;
}

sub open_file_handle_append
{
    my ($filename) = @_;
    $filename =~ s/~/$ENV{HOME}/g;
    open (my $file_handle, '>>', "$filename") or die "$!: $filename";
    return $file_handle;
}

sub open_file_handle_CLOBBER
{
    my ($filename) = @_;
    $filename =~ s/~/$ENV{HOME}/g;
    open (my $file_handle, '>', "$filename") or die "$!: $filename";
    return $file_handle;
}

sub parameter_check
{
    my ($args, $expected_args, $func_ref) = @_;

    my $chk_args = scalar(@{$args});

    if($chk_args != $expected_args)
    {
        print colored("\nCRITICAL ERROR\n==============\n\nInvalid parameter list for ( $func_ref ).\n"
                    . "Expected: $expected_args. Got: $chk_args.\n\nAborting.", 'bright_red', 'bold'), "\n";
        exit(1);
    }
}

sub set_operation_mode
{
    my ($verbose) = @_;
    my $op_mode = undef;

    if($ScribbleWibble::op_mode ne "ripgrep" && $ScribbleWibble::op_mode ne "recutils")
    {
        print colored("ERROR. Operation mode must be specified. "
                      . "Set op_mode flag to either 'recutils' or 'ripgrep'.", 'bright_red', 'bold'), "\n";
        exit(1);
    }

    if($ScribbleWibble::op_mode eq "ripgrep")
    {
        $op_mode = "RIPGREP";
    }
    else
    {
        $op_mode = "RECUTILS";
    }

    if($verbose)
    {
        print colored("Using operation mode: $op_mode", 'bright_green', 'bold'), "\n";
    }
    return $op_mode;
}

sub make_tmp_db_backup
{
    my ($ark) = @_;
    parameter_check(\@_, 1, "make_tmp_db_backup");
    my $database_file;
    if   (!$ark) { $database_file = $ScribbleWibble::db_location; }
    else         { $database_file = $ScribbleWibble::archived_db_location; }

    # Make backup copy of database, just in case
    my $backup_copy = 0;
    mkpath("$ScribbleWibble::tmp_dir");
    if (!$ark) { $backup_copy = copy("$database_file", "$ScribbleWibble::tmp_dir" ); }
    else       { $backup_copy = copy("$database_file", "$ScribbleWibble::tmp_dir"); }

    if (!$backup_copy)
    {
        print colored("❌ Unable to make a backup copy of '$database_file' into $ScribbleWibble::tmp_dir.\n"
                    . "Check that 'tmp_dir' points to a writeable directory, and that both 'db_location'\n"
                    . "and 'db_archived' point to valid files. Aborting.", 'bright_red', 'bold'), "\n";
        exit(1);
    }
    else
    {
        print colored ("✓ Temporary backup copy of '$database_file' successfully created "
                     . "under '$ScribbleWibble::tmp_dir'.", 'bright_green'), "\n";
    }

    return $database_file;
}

sub update_record_metadata
{
    my ($note_id,        $filetype,     $editdate,
        $edittitle,      $editdesc,     $editproj,
        $edittags,       $editclass,    $editdirs,
        $editpairs,      $editcust,     $linkids,
        $archive_search, $verbose,      $from_file,
        $current_type,   $current_proj, $op_mode) = @_;
    parameter_check(\@_, 18, "update_record_metadata");

    my $success;

    if($op_mode eq "RIPGREP")
    {
        my @replacement_record;
        push @replacement_record, "Date: "        . $editdate . "\n";
        push @replacement_record, "Title: "       . $edittitle . "\n";
        push @replacement_record, "NoteId: "      . $note_id . "\n";
        push @replacement_record, "Description: " . $editdesc . "\n";
        push @replacement_record, "Filetype: "    . $filetype . "\n";
        push @replacement_record, "Project: "     . $editproj . "\n";
        push @replacement_record, "Tags: "        . $edittags . "\n";
        push @replacement_record, "Class: "       . $editclass . "\n";
        push @replacement_record, "DataDirs: "    . $editdirs . "\n";
        push @replacement_record, "KeyPairs: "    . $editpairs . "\n";
        push @replacement_record, "Custom: "      . $editcust . "\n";
        push @replacement_record, "LinkedIds: "   . $linkids . "\n";

        # default to fail, if update successful, will be 1
        $success = 0;
        $success = ScribbleWibble::rg_search::rg_update_entire_record($note_id, $archive_search, \@replacement_record);
    }
    elsif($op_mode eq "RECUTILS")
    {
        # Note: UNIX exit code 0 = success, Perl success = 1;
        # Set $success to 1 and only change it if a recutils command returns 1 (UNIX error)
        $success = 1;

        my $rec_set_cmd = "";
        my $rec_set_op = "";
        my $rec_set_pofx = "";
        my $rec_set_prx = "";
        my $recutils_update = 0;

        if(! $archive_search) { $rec_set_pofx = "$ScribbleWibble::db_location --verbose"; }
        else { $rec_set_pofx = "$ScribbleWibble::archived_db_location --verbose"; }

        $rec_set_prx  = "recset -t ScribbleWibble_Note ";
        $rec_set_prx .= "-e \"NoteId = '$note_id'\" ";

        # 1. Update the title
        $rec_set_cmd = "-f Title -s \"$edittitle\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 2. Update the filetype
        $rec_set_cmd = "-f Filetype -s \"$filetype\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 3. Update the description
        $rec_set_cmd = "-f Description -s \"$editdesc\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 4. Update the project
        $rec_set_cmd = "-f Project -s \"$editproj\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 5. Update the tags
        $rec_set_cmd = "-f Tags -s \"$edittags\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 6. Update the class
        $rec_set_cmd = "-f Class -s \"$editclass\"  ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 7. Update the datadirs
        $rec_set_cmd = "-f DataDirs -s \"$editdirs\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 8. Update the keypairs
        $rec_set_cmd = "-f KeyPairs -s \"$editpairs\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 9. Update the custom field
        $rec_set_cmd = "-f Custom -s \"$editcust\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }

        # 10. Update the LinkedIds
        $rec_set_cmd = "-f LinkedIds -s \"$linkids\" ";
        $rec_set_op  = $rec_set_prx . $rec_set_cmd . $rec_set_pofx;
        $recutils_update = system("$rec_set_op");
        if($recutils_update eq 1) { $success = 0; }

        if($verbose) { print "$rec_set_op\n"; }
    }
    else
    {
        print colored("❌ Error. Unknown mode selected. Must be either 'RECUTILS' or 'RIPGREP'. Aborting",
                    'bright_red'), "\n";
        exit(1);
    }

    if($success)
    {
        print colored("✓ Updated note metadata for $note_id.\n", 'bright_green');

        my $notepath_prefix = substr $note_id, 0, 2;
        # if the filetype (extension) has changed, we need to rename/move the file
        if(($current_type ne $filetype) || ($current_proj ne $editproj))
        {
            my $note_file = "";
            my $note_dest = "";
            my $newpath = "";

            if(! $archive_search)
            {
                $note_file = "$ScribbleWibble::wibble_store/notes/$current_type/$current_proj/$notepath_prefix/$note_id.$current_type";
                $note_dest = "$ScribbleWibble::wibble_store/notes/$filetype/$editproj/$notepath_prefix/$note_id.$filetype";
                $newpath   = "$ScribbleWibble::wibble_store/notes/$filetype/$editproj/$notepath_prefix/";
            }
            else
            {
                $note_file = "$ScribbleWibble::wibble_store/archived_notes/$current_type/$current_proj/$notepath_prefix/$note_id.$current_type";
                $note_dest = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$editproj/$notepath_prefix/$note_id.$filetype";
                $newpath   = "$ScribbleWibble::wibble_store/archived_notes/$filetype/$editproj/$notepath_prefix/";
            }

            mkpath("$newpath");
            my $mv_cmd = system("mv -v $note_file $note_dest"); # returns 0 on success
            if(! $mv_cmd)
            {
                print "✓ Successfully renamed note.\n";
                my $delete_empty_dir = "";
                if(! $archive_search) { $delete_empty_dir = system("find $ScribbleWibble::wibble_store/notes/ -type d -empty -delete"); }
                else         { $delete_empty_dir = system("find $ScribbleWibble::wibble_store/archived_notes/ -type d -empty -delete"); }

                if($delete_empty_dir eq 0) { print "✓ Original note directory removed.\n"; }
                else { print "❌ Error removing original note directory. Run 'wibble tool --clean' to fix.\n"; }

            }
            else { print "❌ Problem renaming note from $note_file to $note_dest. Manually fix.\n"; }
        }

        # prohibit batch mode operation on archived notes - that's why they're archived...
        if(defined $from_file && length $from_file && ! $archive_search)
        {
            my $output_path = "$ScribbleWibble::wibble_store/notes/$filetype/$editproj/$notepath_prefix";
            $from_file =~ s/~/$ENV{HOME}/g;
            if(! -f $from_file)
            { print colored("Batch mode: Invalid file '$from_file' specified, original note contents left unchanged.\n", 'bright_red', 'bold'); exit 1; }
            my $gen_from_file = copy($from_file, "$output_path/$note_id.$filetype");
            if($gen_from_file)
            {
                print colored("✓ Batch mode: Note contents successfully overwritten from file $from_file.\n", 'bright_green');
            }
            else
            {
                print colored("❌ Batch mode: Error creating new note from file $from_file. Check that '$from_file' exists and is readable.\n", 'bright_red', 'bold');
            }

        }

        exit 0;
    }
    else
    {
        print "❌ Error creating note metadata. Aborting.\n";
        exit 1;
    }

}


1;
